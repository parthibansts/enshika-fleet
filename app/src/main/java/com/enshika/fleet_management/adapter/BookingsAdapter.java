package com.enshika.fleet_management.adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.fragment.BookingFragment;
import com.enshika.fleet_management.model.BookingsModel;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;

import java.util.ArrayList;

/**
 * Created by Vino on 29-03-2018.
 */

public class BookingsAdapter extends RecyclerView.Adapter<BookingsAdapter.MyViewHolder> {
    ArrayList<BookingsModel> bookingsModels;
    private BookingFragment bookingFragment;
    private Activity activity;

    public BookingsAdapter(Activity activity, BookingFragment bookingFragment, ArrayList<BookingsModel> bookingsModels) {
        this.bookingsModels = bookingsModels;
        this.activity = activity;
        this.bookingFragment = bookingFragment;
    }

    @Override
    public BookingsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.list_bookings, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BookingsAdapter.MyViewHolder holder, final int position) {
        BookingsModel bookingsPojo = bookingsModels.get(position);
        holder.txtTripId.setText(bookingsPojo.getTourId());
        holder.txtDateTime.setText(Support.getDateFromLong(bookingsPojo.getBookingTime(), Vars.DATE_FORMAT));
        holder.txtDriver.setText(bookingsPojo.getDriverName());
        holder.txtRider.setText(bookingsPojo.getPassengerName());
        holder.txtInvoiceAmount.setText(bookingsPojo.getTotal() + " " + Support.getCurrencySymbol(activity));
        holder.txtStatus.setText(bookingsPojo.getStatus());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bookingFragment.onListClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookingsModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView txtTripId, txtDateTime, txtDriver, txtRider, txtInvoiceAmount, txtCarType, txtDistance, txtStatus;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtTripId = itemView.findViewById(R.id.txtTripId);
            txtDateTime = itemView.findViewById(R.id.txtDateTime);
            txtDriver = itemView.findViewById(R.id.txtDriver);
            txtRider = itemView.findViewById(R.id.txtRider);
            txtInvoiceAmount = itemView.findViewById(R.id.txtInvoiceAmount);
            txtCarType = itemView.findViewById(R.id.txtCarType);
            txtDistance = itemView.findViewById(R.id.txtDistance);
            txtStatus = itemView.findViewById(R.id.txtStatus);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }
}
