package com.enshika.fleet_management.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.RegionPojo;
import java.util.ArrayList;

/**
 * Created by Dharmalingam Sekar on 07-09-2017.
 */

public class MultiSelectionAdapter extends RecyclerView.Adapter<MultiSelectionAdapter.MyViewHolder> {

    private ArrayList<RegionPojo> listValue;
    Activity activity;
    Context mContext;
    int type;
    boolean isPickup;
    ArrayList<String> selectedItems;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_Value;
        CheckBox chk;
        RelativeLayout relative_Option;

        public MyViewHolder(View view) {
            super(view);
            txt_Value = (TextView) view.findViewById(R.id.txt_Value);
            chk = (CheckBox) view.findViewById(R.id.chk);
            relative_Option = (RelativeLayout) view.findViewById(R.id.relative_Option);
        }
    }

    public MultiSelectionAdapter(Activity activity, ArrayList<RegionPojo> listValue, ArrayList<String> selectedItems) {
        this.listValue = listValue;
        this.activity = activity;
        this.type = type;
        this.selectedItems = selectedItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_multiselection, parent, false);
        mContext = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if (listValue != null) {
            final RegionPojo val = listValue.get(position);
            final String ValueName = val.getName();

            holder.chk.setClickable(false);
            holder.txt_Value.setText(ValueName);

            if(!selectedItems.isEmpty() && selectedItems.contains(val.getId())) {
                holder.chk.setChecked(true);
            } else
                holder.chk.setChecked(false);

            holder.relative_Option.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(selectedItems.contains(val.getId())) {
                        holder.chk.setChecked(false);
                        selectedItems.remove(val.getId());
                    } else {
                        selectedItems.add(val.getId());
                        holder.chk.setChecked(true);
                    }
//                    ((NewDriverActivity)activity).onWeightSelection(val.getid(), ValueName, type);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return listValue.size();
    }

    public void resetSelection() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

}









