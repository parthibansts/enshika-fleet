package com.enshika.fleet_management.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.fragment.DriversFragment;
import com.enshika.fleet_management.fragment.FleetFeeFragment;
import com.enshika.fleet_management.model.DriversPojo;
import com.enshika.fleet_management.utils.Support;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.enshika.fleet_management.utils.Support.stringToFloat;

/**
 * Created by Vino on 29-03-2018.
 */

public class DriversAdapter extends RecyclerView.Adapter<DriversAdapter.MyViewHolder> {
    DriversFragment driversFragment;
    FleetFeeFragment fleetFeeFragment;
    private ArrayList<DriversPojo> driversPojos;
    private Activity activity;

    public DriversAdapter(Activity activity, DriversFragment driversFragment, ArrayList<DriversPojo> driversPojos) {
        this.driversPojos = driversPojos;
        this.driversFragment = driversFragment;
        this.activity = activity;
    }

    public DriversAdapter(Activity activity, FleetFeeFragment fleetFeeFragment, ArrayList<DriversPojo> driversPojos) {
        this.driversPojos = driversPojos;
        this.fleetFeeFragment = fleetFeeFragment;
        this.activity = activity;
    }

    @Override
    public DriversAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.list_drivers, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DriversAdapter.MyViewHolder holder, final int position) {
        DriversPojo driversPojo = driversPojos.get(position);
        holder.txt_driver_name.setText(driversPojo.getFirstName());
        holder.rt_driver_rating.setRating(stringToFloat(driversPojo.getRate()));
        if (fleetFeeFragment != null) {
            holder.txtSecurityDeposit.setText(driversPojo.getBalanceAmount() + " " + Support.getCurrencySymbol(activity));
            holder.txtSecurityDeposit.setVisibility(View.VISIBLE);
        } else
            holder.txtSecurityDeposit.setVisibility(View.GONE);
        Support.loadImage(driversPojo.getPhotoUrl(), holder.img_driver, R.drawable.ic_user_placeholder);
        if (driversPojo.getOnLineStatus() != null && driversPojo.getOnLineStatus().equals("true")) {
            holder.img_online.setImageDrawable(activity.getResources().getDrawable(R.drawable.circle_online_green));
        } else
            holder.img_online.setImageDrawable(activity.getResources().getDrawable(R.drawable.circle_gray));
        holder.rlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (driversFragment != null)
                    driversFragment.onClickDriverList(position);
                else if (fleetFeeFragment != null)
                    fleetFeeFragment.onClickDriverList(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return driversPojos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView img_driver;
        ImageView img_online;
        TextView txt_driver_name;
        RatingBar rt_driver_rating;
        RelativeLayout rlContainer;
        TextView txtSecurityDeposit;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtSecurityDeposit = itemView.findViewById(R.id.txtSecurityDeposit);
            img_online = itemView.findViewById(R.id.img_online);
            img_driver = itemView.findViewById(R.id.img_driver);
            txt_driver_name = itemView.findViewById(R.id.txt_driver_name);
            rt_driver_rating = itemView.findViewById(R.id.rt_driver_rating);
            rlContainer = itemView.findViewById(R.id.rlContainer);
        }
    }
}
