package com.enshika.fleet_management.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.activity.DriverDetailsActivity;
import com.enshika.fleet_management.utils.Support;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static com.enshika.fleet_management.utils.Vars.DEVELOPMENT_URL;

/**
 * Created by SUN on 30-03-2017.
 */

public class DriverDocumentsAdapter extends PagerAdapter {

    RelativeLayout relativeContainer;
    ImageView img;
    TextView txtName;
    ArrayList<String> urls;
    private LinkedHashMap<String, String> listImages = new LinkedHashMap<>();
    private Context mContext;
    private LayoutInflater mLayoutInflater;

    public DriverDocumentsAdapter(Context context, LinkedHashMap<String, String> listImages) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listImages = listImages;
        this.urls = new ArrayList<>(listImages.values());
        for (int i = 0; i < urls.size(); i++) {
            urls.set(i, DEVELOPMENT_URL + urls.get(i));
        }
    }

    @Override
    public int getCount() {
        return listImages.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_driver_documents, container, false);

        relativeContainer = itemView.findViewById(R.id.relativeContainer);
        img = itemView.findViewById(R.id.img);
        txtName = itemView.findViewById(R.id.txtName);

        final String documentUrl = (new ArrayList<String>(listImages.values())).get(position);
        final String documentName = (new ArrayList<String>(listImages.keySet())).get(position);

        Support.loadImage(documentUrl, img, R.drawable.ic_placeholder_gallery);
        txtName.setText(documentName);

        relativeContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DriverDetailsActivity) mContext).onClickImage(position, urls);
            }
        });
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((LinearLayout) object);
    }
}