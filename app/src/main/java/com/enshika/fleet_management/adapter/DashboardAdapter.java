package com.enshika.fleet_management.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.fragment.DashboardSummaryFragment;
import com.enshika.fleet_management.model.DashboardSummaryModel;
import com.enshika.fleet_management.utils.Support;

import java.util.ArrayList;

/**
 * Created by Shamla Tech on 14-06-2018.
 */

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewHolder> {
    private ArrayList<DashboardSummaryModel> dashboardPojos;
    private DashboardSummaryFragment dashboardFragment;
    private Activity activity;

    public DashboardAdapter(Activity activity, DashboardSummaryFragment dashboardFragment, ArrayList<DashboardSummaryModel> dashboardPojos) {
        this.dashboardPojos = dashboardPojos;
        this.activity = activity;
        this.dashboardFragment = dashboardFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.list_dashboard, parent, false);
        return new DashboardAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        DashboardSummaryModel dashboardPojo = dashboardPojos.get(position);
        Support.loadImage(dashboardPojo.getImageUrl(), holder.img_dashboard_item);
        holder.txt_title.setText(dashboardPojo.getName());
        holder.txt_value.setText(dashboardPojo.getValue());
    }

    @Override
    public int getItemCount() {
        return dashboardPojos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_dashboard_item;
        TextView txt_title, txt_value;

        public MyViewHolder(View itemView) {
            super(itemView);
            img_dashboard_item = itemView.findViewById(R.id.img_dashboard_item);
            txt_title = itemView.findViewById(R.id.txt_title);
            txt_value = itemView.findViewById(R.id.txt_value);
        }
    }
}
