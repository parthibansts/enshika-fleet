package com.enshika.fleet_management.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.enshika.fleet_management.fragment.CarsFragment;
import com.enshika.fleet_management.fragment.DriversFragment;
import com.enshika.fleet_management.model.CarsPojo;
import com.enshika.fleet_management.model.DriversPojo;

import java.util.ArrayList;

/**
 * Created by hp on 20-09-2017.
 */

public class DriversCarsFragmentAdapter extends FragmentPagerAdapter {

    private Activity activity;

    public DriversCarsFragmentAdapter(Activity activity, FragmentManager fm) {
        super(fm);
        this.activity = activity;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        Bundle bundle = new Bundle();
        if (position == 0) {
            fragment = new DriversFragment();
            fragment.setArguments(bundle);
        } else if (position == 1) {
            fragment = new CarsFragment();
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Drivers";
        } else if (position == 1) {
            title = "Cars";
        }
        return title;
    }

}
