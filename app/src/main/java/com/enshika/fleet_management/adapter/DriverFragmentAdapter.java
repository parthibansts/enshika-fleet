package com.enshika.fleet_management.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.enshika.fleet_management.fragment.DriverDetailFragment;
import com.enshika.fleet_management.fragment.DriverRidesFragment;

/**
 * Created by hp on 20-09-2017.
 */

public class DriverFragmentAdapter extends FragmentPagerAdapter {

    String driverID;
    private Activity activity;

    public DriverFragmentAdapter(Activity activity, FragmentManager fm, String driverID) {
        super(fm);
        this.driverID = driverID;
        this.activity = activity;

    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = new DriverDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putString("driverId", driverID);
            fragment.setArguments(bundle);
        } else if (position == 1) {
            fragment = new DriverRidesFragment();
            Bundle bundle = new Bundle();
            bundle.putString("driverId", driverID);
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Details";
        } else if (position == 1) {
            title = "Bookings";
        }
        return title;
    }

}
