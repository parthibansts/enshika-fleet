package com.enshika.fleet_management.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.fragment.CarsFragment;
import com.enshika.fleet_management.model.CarsPojo;
import com.enshika.fleet_management.utils.Support;

import java.util.ArrayList;

/**
 * Created by Vino on 29-03-2018.
 */

public class CarsAdapter extends RecyclerView.Adapter<CarsAdapter.MyViewHolder> {
    private ArrayList<CarsPojo> carsPojos;
    private CarsFragment carsFragment;
    private Activity activity;

    public CarsAdapter(Activity activity, CarsFragment carsFragment, ArrayList<CarsPojo> carsPojos) {
        this.carsPojos = carsPojos;
        this.activity = activity;
        this.carsFragment = carsFragment;
    }

    @Override
    public CarsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.list_cars, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CarsAdapter.MyViewHolder holder, final int position) {
        CarsPojo carsPojo = carsPojos.get(position);
        holder.txt_car_model.setText(carsPojo.getModelName());
        holder.txt_car_number.setText(carsPojo.getCarPlateNo());
        holder.txt_car_type.setText(carsPojo.getCarType());
        Support.loadImage(carsPojo.getFrontImgUrl(), holder.img_car, R.drawable.ic_place_holder_car);
        holder.relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                carsFragment.onClickCarListItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return carsPojos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_car;
        TextView txt_car_model, txt_car_number, txt_car_type;
        RelativeLayout relative;

        public MyViewHolder(View itemView) {
            super(itemView);
            img_car = itemView.findViewById(R.id.img_car);
            txt_car_model = itemView.findViewById(R.id.txt_car_model);
            txt_car_type = itemView.findViewById(R.id.txt_car_type);
            txt_car_number = itemView.findViewById(R.id.txt_car_number);
            relative = itemView.findViewById(R.id.relative);
        }
    }
}
