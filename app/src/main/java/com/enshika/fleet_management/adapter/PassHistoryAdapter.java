package com.enshika.fleet_management.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.fragment.WalletFragment;
import com.enshika.fleet_management.model.PassHistoryModel;
import com.enshika.fleet_management.utils.Support;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shamla Tech on 14-06-2018.
 */

public class PassHistoryAdapter extends RecyclerView.Adapter<PassHistoryAdapter.MyViewHolder> {
    private ArrayList<PassHistoryModel> passHistoryModels;
    private WalletFragment walletFragment;
    private Activity activity;

    public PassHistoryAdapter(Activity activity, WalletFragment walletFragment, ArrayList<PassHistoryModel> passHistoryModels) {
        this.passHistoryModels = passHistoryModels;
        this.activity = activity;
        this.walletFragment = walletFragment;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.list_pass_history, parent, false);
        return new PassHistoryAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        PassHistoryModel dashboardPojo = passHistoryModels.get(position);
        Support.loadImage(dashboardPojo.getDriverImage(), holder.ciDriver, R.drawable.ic_user_placeholder);
        holder.txtName.setText(dashboardPojo.getDriverName());
        holder.txtDate.setText(dashboardPojo.getDateTime());
        holder.txtPrice.setText(dashboardPojo.getPrice());
    }

    @Override
    public int getItemCount() {
        return passHistoryModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView ciDriver;
        TextView txtName, txtPrice, txtDate;

        public MyViewHolder(View itemView) {
            super(itemView);
            ciDriver = itemView.findViewById(R.id.ciDriver);
            txtName = itemView.findViewById(R.id.txtName);
            txtPrice = itemView.findViewById(R.id.txtPrice);
            txtDate = itemView.findViewById(R.id.txtDate);
        }
    }
}
