package com.enshika.fleet_management.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.fragment.SettingsFragment;
import com.enshika.fleet_management.fragment.WalletFragment;
import com.enshika.fleet_management.model.PaymentOptionPojo;
import com.enshika.fleet_management.utils.Support;

import java.util.ArrayList;

/**
 * Created by Vino on 29-03-2018.
 */

public class PaymentOptionsAdapter extends RecyclerView.Adapter<PaymentOptionsAdapter.MyViewHolder> {
    private ArrayList<PaymentOptionPojo> paymentPojos;
    private WalletFragment walletFragment;
    private SettingsFragment settingsFragment;
    private Activity activity;

    public PaymentOptionsAdapter(Activity activity, WalletFragment walletFragment, ArrayList<PaymentOptionPojo> paymentPojos) {
        this.walletFragment = walletFragment;
        this.activity = activity;
        this.paymentPojos = paymentPojos;
    }

    public PaymentOptionsAdapter(Activity activity, SettingsFragment settingsFragment, ArrayList<PaymentOptionPojo> paymentPojos) {
        this.settingsFragment = settingsFragment;
        this.activity = activity;
        this.paymentPojos = paymentPojos;
    }

    @Override
    public PaymentOptionsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.list_payment_options, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PaymentOptionsAdapter.MyViewHolder holder, final int position) {
        PaymentOptionPojo paymentPojo = paymentPojos.get(position);
        holder.txt_name.setText(paymentPojo.getPaymentType());
        Support.loadImageWithFullUrl(paymentPojo.getImage(), holder.img_icon, R.drawable.ic_mobile_money);
        holder.linearContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (walletFragment != null)
                    walletFragment.onClickPayment(position);
                else if(settingsFragment!=null)
                    settingsFragment.onClickPayment(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return paymentPojos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_icon;
        TextView txt_name;
        LinearLayout linearContainer;

        public MyViewHolder(View itemView) {
            super(itemView);
            linearContainer = itemView.findViewById(R.id.linearContainer);
            img_icon = itemView.findViewById(R.id.imgIcon);
            txt_name = itemView.findViewById(R.id.txtName);
        }
    }
}
