package com.enshika.fleet_management.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.fragment.DriverRidesFragment;
import com.enshika.fleet_management.model.DriverRidePojo;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;

import java.util.ArrayList;

/**
 * Created by Vino on 29-03-2018.
 */

public class DriverRidesAdapter extends RecyclerView.Adapter<DriverRidesAdapter.MyViewHolder> {
    private ArrayList<DriverRidePojo> driverRidePojos;
    private Activity activity;
    private DriverRidesFragment driverRidesFragment;

    public DriverRidesAdapter(Activity activity, DriverRidesFragment driverRidesFragment, ArrayList<DriverRidePojo> driverRidePojos) {
        this.driverRidePojos = driverRidePojos;
        this.driverRidesFragment = driverRidesFragment;
        this.activity = activity;
    }

    @Override
    public DriverRidesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.list_driver_rides, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(DriverRidesAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        DriverRidePojo bookingsPojo = driverRidePojos.get(position);
        holder.txtDate.setText(Support.getDateFromLong(bookingsPojo.getBookingTime(), Vars.DATE_FORMAT));
        holder.txtVehicleTypeNum.setText(bookingsPojo.getCarType() + " - " + bookingsPojo.getCarPlateNo());
        if (bookingsPojo.getDestinationAddress().isEmpty())
            holder.txtDrop.setVisibility(View.GONE);
        else {
            holder.txtDrop.setVisibility(View.VISIBLE);
            holder.txtDrop.setText(bookingsPojo.getDestinationAddress());
        }
        holder.txtPickup.setText(bookingsPojo.getSourceAddress());
        holder.txtPrice.setText(bookingsPojo.getTotal() + " " + Support.getCurrencySymbol(activity));
        holder.rlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                driverRidesFragment.onClickDriverRideList(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return driverRidePojos.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtDate, txtVehicleTypeNum, txtPrice, txtPickup, txtDrop;
        RelativeLayout rlContainer;

        MyViewHolder(View itemView) {
            super(itemView);
            rlContainer = itemView.findViewById(R.id.rlContainer);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtVehicleTypeNum = itemView.findViewById(R.id.txtVehicleTypeNum);
            txtPrice = itemView.findViewById(R.id.txtPrice);
            txtPickup = itemView.findViewById(R.id.txtPickup);
            txtDrop = itemView.findViewById(R.id.txtDrop);
        }
    }
}
