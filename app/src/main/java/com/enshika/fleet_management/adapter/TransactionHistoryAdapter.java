package com.enshika.fleet_management.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.fragment.WalletFragment;
import com.enshika.fleet_management.model.TransactionHistoryModel;
import com.enshika.fleet_management.utils.Support;

import java.util.ArrayList;

/**
 * Created by Shamla Tech on 14-06-2018.
 */

public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.MyViewHolder> {
    private ArrayList<TransactionHistoryModel> transactionHistoryModels;
    private WalletFragment walletFragment;
    private Activity activity;

    public TransactionHistoryAdapter(Activity activity, WalletFragment walletFragment, ArrayList<TransactionHistoryModel> transactionHistoryModels) {
        this.transactionHistoryModels = transactionHistoryModels;
        this.activity = activity;
        this.walletFragment = walletFragment;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.list_transaction_history, parent, false);
        return new TransactionHistoryAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        TransactionHistoryModel transactionHistoryModel = transactionHistoryModels.get(position);
        holder.txtPaymentType.setText(transactionHistoryModel.getType());
        holder.txtDate.setText(transactionHistoryModel.getDate());
        holder.txtPrice.setText(transactionHistoryModel.getPrice() + " " + Support.getCurrencySymbol(activity));
    }

    @Override
    public int getItemCount() {
        return transactionHistoryModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtPaymentType, txtPrice, txtDate;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtPaymentType = itemView.findViewById(R.id.txtPaymentType);
            txtPrice = itemView.findViewById(R.id.txtPrice);
            txtDate = itemView.findViewById(R.id.txtDate);
        }
    }
}
