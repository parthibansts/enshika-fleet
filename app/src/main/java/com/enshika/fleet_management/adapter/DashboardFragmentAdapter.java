package com.enshika.fleet_management.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.enshika.fleet_management.fragment.DashboardCommissionFragment;
import com.enshika.fleet_management.fragment.DashboardDriversFragment;
import com.enshika.fleet_management.fragment.DashboardSummaryFragment;
import com.enshika.fleet_management.fragment.DashboardRevenueFragment;

/**
 * Created by hp on 20-09-2017.
 */

public class DashboardFragmentAdapter extends FragmentPagerAdapter {

    private Activity activity;

    public DashboardFragmentAdapter(Activity activity, FragmentManager fm) {
        super(fm);
        this.activity = activity;

    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        Bundle bundle = new Bundle();
        if (position == 0) {
            fragment = new DashboardSummaryFragment();
            fragment.setArguments(bundle);
        } else if (position == 1) {
            fragment = new DashboardRevenueFragment();
            fragment.setArguments(bundle);
        }/* else if (position == 2) {
            fragment = new DashboardCommissionFragment();
            fragment.setArguments(bundle);
        }*/else if (position == 2) {
            fragment = new DashboardDriversFragment();
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Summary";
        } else if (position == 1) {
            title = "Revenue";
        }/*else if (position == 2) {
            title = "Commission";
        }*/else if (position == 2) {
            title = "Drivers";
        }
        return title;
    }

}
