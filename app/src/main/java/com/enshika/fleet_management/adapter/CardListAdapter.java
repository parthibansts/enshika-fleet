package com.enshika.fleet_management.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.activity.CardListActivity;
import com.enshika.fleet_management.model.CardModel;
import com.enshika.fleet_management.utils.Vars;
import com.vinaygaba.creditcardview.CardNumberFormat;
import com.vinaygaba.creditcardview.CardType;
import com.vinaygaba.creditcardview.CreditCardView;

import java.util.ArrayList;

/**
 * Created by Vino on 29-03-2018.
 */

public class CardListAdapter extends RecyclerView.Adapter<CardListAdapter.MyViewHolder> {
    private ArrayList<CardModel> cardPojos;
    private Activity activity;
    private boolean isWalletTransfer;

    public CardListAdapter(Activity activity, ArrayList<CardModel> cardPojos,boolean isWalletTransfer) {
        this.activity = activity;
        this.cardPojos = cardPojos;
        this.isWalletTransfer = isWalletTransfer;
    }

    @NonNull
    @Override
    public CardListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.list_card, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CardListAdapter.MyViewHolder holder, final int position) {
        CardModel cardPojo = cardPojos.get(position);
        holder.cardView.setCardName(cardPojo.getDetails().getHolder_name());
        holder.cardView.setCardNumber(cardPojo.getDetails().getWallet_number());

        String exp_date = cardPojo.getDetails().getExpiry_date();
        String month = exp_date.substring(0, exp_date.length() / 2);
        String year = exp_date.substring(exp_date.length() / 2);

        holder.cardView.setExpiryDate(month + "/" + year);
        holder.cardView.setCardNumberFormat(CardNumberFormat.MASKED_ALL_BUT_LAST_FOUR);
        switch (cardPojo.getDetails().getWallet_name()) {
            case Vars.MASTER_CARD:
                holder.cardView.setType(CardType.MASTERCARD);
                break;
            case Vars.VISA_CARD:
                holder.cardView.setType(CardType.VISA);
                break;
            default:
                holder.cardView.setType(CardType.AUTO);
                break;
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isWalletTransfer) {
                    ((CardListActivity)activity).onClickCard(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cardPojos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CreditCardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }
}
