package com.enshika.fleet_management.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.activity.AddMobileMoneyActivity;
import com.enshika.fleet_management.model.MobileMoneyOperators;
import com.enshika.fleet_management.utils.Support;

import java.util.ArrayList;

/**
 * Created by Shamla Tech on 14-06-2018.
 */

public class ServiceProviderAdapter extends RecyclerView.Adapter<ServiceProviderAdapter.MyViewHolder> {
    private ArrayList<MobileMoneyOperators> serviceProviders;
    private Activity activity;
    private String selectedProviderId;

    public ServiceProviderAdapter(Activity activity, ArrayList<MobileMoneyOperators> serviceProviders, String selectedProviderId) {
        this.serviceProviders = serviceProviders;
        this.selectedProviderId = selectedProviderId;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.list_service_providers, parent, false);
        return new ServiceProviderAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        MobileMoneyOperators model = serviceProviders.get(position);
        holder.linearContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AddMobileMoneyActivity) activity).onProviderListClick(position);
            }
        });
        holder.txtProvider.setText(model.getOperator());
        Support.loadImageWithFullUrl(model.getOperatorImage(), holder.imgIcon, R.drawable.ic_placeholder_gallery);
        if (selectedProviderId != null &&
                !selectedProviderId.isEmpty() && selectedProviderId.equals(model.getOperatorId() + "")) {
            holder.linearContainer.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));
            holder.txtProvider.setTextColor(activity.getResources().getColor(R.color.white));
        } else {
            holder.linearContainer.setBackgroundColor(activity.getResources().getColor(R.color.white));
            holder.txtProvider.setTextColor(activity.getResources().getColor(R.color.blackLight));
        }
    }

    @Override
    public int getItemCount() {
        return serviceProviders.size();
    }

    public void updateSelectedProvider(String selectedProviderId) {
        this.selectedProviderId = selectedProviderId;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearContainer;
        TextView txtProvider;
        ImageView imgIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            linearContainer = itemView.findViewById(R.id.linearContainer);
            txtProvider = itemView.findViewById(R.id.txtProvider);
            imgIcon = itemView.findViewById(R.id.imgIcon);
        }
    }
}
