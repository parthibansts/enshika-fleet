package com.enshika.fleet_management.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.activity.MobileMoneyActivity;
import com.enshika.fleet_management.model.MobileMoneyOperators;
import com.enshika.fleet_management.utils.Support;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shamla Tech on 14-06-2018.
 */

public class MobileMoneyAdapter extends RecyclerView.Adapter<MobileMoneyAdapter.MyViewHolder> {
    private ArrayList<MobileMoneyOperators> mobileMoneyModels;
    private Activity activity;
    private String selectedMobileNumber;

    public MobileMoneyAdapter(Activity activity, ArrayList<MobileMoneyOperators> mobileMoneyModels, String selectedId) {
        this.mobileMoneyModels = mobileMoneyModels;
        this.selectedMobileNumber = selectedId;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.list_mobile_money, parent, false);
        return new MobileMoneyAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        MobileMoneyOperators model = mobileMoneyModels.get(position);
        holder.rlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MobileMoneyActivity) activity).onMobileMoneyClick(position);
            }
        });
        holder.txtMobile.setText(model.getMobileNumber()+"");
        holder.txtName.setText(model.getOperator());
        Support.loadImageWithFullUrl(model.getOperatorImage(), holder.imgOperator, R.drawable.ic_placeholder_gallery);
        if (selectedMobileNumber != null &&
                !selectedMobileNumber.isEmpty() && selectedMobileNumber.equals(model.getMobileNumber() + "")) {
            holder.rlContainer.setBackgroundColor(activity.getResources().getColor(R.color.colorGrayExtraLight));
            holder.imgTick.setVisibility(View.VISIBLE);
        } else {
            holder.rlContainer.setBackgroundColor(activity.getResources().getColor(R.color.white));
            holder.imgTick.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mobileMoneyModels.size();
    }

    public void updateSelection(String selectedMobileNumber) {
        this.selectedMobileNumber = selectedMobileNumber;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rlContainer;
        CircleImageView imgOperator;
        TextView txtName, txtMobile;
        ImageView imgTick;

        public MyViewHolder(View itemView) {
            super(itemView);
            rlContainer = itemView.findViewById(R.id.rlContainer);
            imgOperator = itemView.findViewById(R.id.imgOperator);
            txtName = itemView.findViewById(R.id.txtName);
            txtMobile = itemView.findViewById(R.id.txtMobile);
            imgTick = itemView.findViewById(R.id.imgTick);
        }
    }
}
