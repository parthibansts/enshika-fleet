package com.enshika.fleet_management.enshika_corporate;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.layer_net.stepindicator.StepIndicator;

import cdflynn.android.library.checkview.CheckView;

/**
 * Created by Shamla Tech on 07-07-2018.
 */

public class RegCorporateStepThree extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    TextView txtProceed;
    StepIndicator step_indicator;
    CheckView checkView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_corporate_step_three);
        initUI();
    }

    private void initUI() {
        checkView = findViewById(R.id.checkView);
        step_indicator = findViewById(R.id.step_indicator);
        step_indicator.setCurrentStepPosition(2);
        txtProceed = findViewById(R.id.txtProceed);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Signup - Corporate");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        checkView.check();
        txtProceed.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtProceed:
                break;
        }
    }
}
