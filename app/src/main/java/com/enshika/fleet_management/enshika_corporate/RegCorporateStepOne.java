package com.enshika.fleet_management.enshika_corporate;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.RegisterCorporateCategoryModel;
import com.enshika.fleet_management.model.RegisterCorporateSubCategoryModel;
import com.enshika.fleet_management.utils.Vars;
import com.github.angads25.toggle.LabeledSwitch;
import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;

/**
 * Created by Shamla Tech on 07-07-2018.
 */

public class RegCorporateStepOne extends AppCompatActivity implements View.OnClickListener, OnToggledListener {
    Toolbar toolbar;
    TextView txtProceed;
    MaterialBetterSpinner sprCategory, sprSubCategory;
    ArrayList<RegisterCorporateSubCategoryModel> corporateSubCategoryModels;
    ArrayList<RegisterCorporateCategoryModel> corporateCategoryModels;
    LabeledSwitch switchPartnerOutlets;
    boolean isPartnerOutlet;
    Bundle bundle;
    String type = "", str_category_type = "", str_sub_category = "";
    LinearLayout lrPartnerOutlet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_corporate_step_one);
        initUI();
        getBundleDetails();
        if (type.equals("student")) {
            sprCategory.setText("Student");
            corporateSubCategoryModels = prepareSubCategory(Vars.CATEGORY_UNIVERSITY);
            prepareSubCategoryAdapter(corporateSubCategoryModels);
        } else {
            prepareCategory();
        }
    }

    private void getBundleDetails() {
        bundle = getIntent().getExtras();
        if (bundle.containsKey("type")) {
            type = bundle.getString("type");
        }

        if (type.equals("student")) {
            lrPartnerOutlet.setVisibility(View.GONE);
            str_category_type = Vars.CATEGORY_UNIVERSITY;
        }
    }

    private void prepareCategory() {
        corporateCategoryModels = new ArrayList<>();
        RegisterCorporateCategoryModel registerCorporateCategoryModel = new RegisterCorporateCategoryModel();

        registerCorporateCategoryModel.setName("ITES/BPO");
        registerCorporateCategoryModel.setType("1");
        corporateCategoryModels.add(registerCorporateCategoryModel);
        registerCorporateCategoryModel = new RegisterCorporateCategoryModel();
        registerCorporateCategoryModel.setName("Bank");
        registerCorporateCategoryModel.setType("2");
        corporateCategoryModels.add(registerCorporateCategoryModel);
        registerCorporateCategoryModel = new RegisterCorporateCategoryModel();
        registerCorporateCategoryModel.setName("Mobile Operator");
        registerCorporateCategoryModel.setType("3");
        corporateCategoryModels.add(registerCorporateCategoryModel);
        registerCorporateCategoryModel = new RegisterCorporateCategoryModel();
        registerCorporateCategoryModel.setName("University");
        registerCorporateCategoryModel.setType("4");
        corporateCategoryModels.add(registerCorporateCategoryModel);


        ArrayAdapter<RegisterCorporateCategoryModel> arrayAdapter = new ArrayAdapter<>(this,
                R.layout.spinner_dropdown_item, corporateCategoryModels);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        sprCategory.setAdapter(arrayAdapter);

        sprCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                str_category_type = corporateCategoryModels.get(position).getType();
                corporateSubCategoryModels = prepareSubCategory(corporateCategoryModels.get(position).getType());
                prepareSubCategoryAdapter(corporateSubCategoryModels);
            }
        });


    }

    private ArrayList<RegisterCorporateSubCategoryModel> prepareSubCategory(String type) {
        corporateSubCategoryModels = new ArrayList<>();
        RegisterCorporateSubCategoryModel subCategoryModel;
        switch (type) {
            case Vars.CATEGORY_OTHER:
                subCategoryModel = new RegisterCorporateSubCategoryModel();
                subCategoryModel.setName("CTS");
                corporateSubCategoryModels.add(subCategoryModel);
                subCategoryModel = new RegisterCorporateSubCategoryModel();
                subCategoryModel.setName("VIBRO");
                corporateSubCategoryModels.add(subCategoryModel);
                break;
            case Vars.CATEGORY_BANK:
                subCategoryModel = new RegisterCorporateSubCategoryModel();
                subCategoryModel.setName("SBI");
                corporateSubCategoryModels.add(subCategoryModel);
                subCategoryModel = new RegisterCorporateSubCategoryModel();
                subCategoryModel.setName("HDFC");
                corporateSubCategoryModels.add(subCategoryModel);
                break;
            case Vars.CATEGORY_MOBILE_OPERATOR:
                subCategoryModel = new RegisterCorporateSubCategoryModel();
                subCategoryModel.setName("JIO");
                corporateSubCategoryModels.add(subCategoryModel);
                subCategoryModel = new RegisterCorporateSubCategoryModel();
                subCategoryModel.setName("AIRTEL");
                corporateSubCategoryModels.add(subCategoryModel);
                break;
            case Vars.CATEGORY_UNIVERSITY:
                subCategoryModel = new RegisterCorporateSubCategoryModel();
                subCategoryModel.setName("Anna University");
                subCategoryModel = new RegisterCorporateSubCategoryModel();
                subCategoryModel.setName("Bharathiyar University");
                corporateSubCategoryModels.add(subCategoryModel);
                break;

        }
        return corporateSubCategoryModels;
    }

    private void prepareSubCategoryAdapter(final ArrayList<RegisterCorporateSubCategoryModel> list) {
        sprSubCategory.setText("");
        ArrayAdapter<RegisterCorporateSubCategoryModel> arrayAdapter = new ArrayAdapter<>(this,
                R.layout.spinner_dropdown_item, list);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        sprSubCategory.setAdapter(arrayAdapter);
        sprSubCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                str_sub_category = list.get(position).getName();
            }
        });
    }

    private void initUI() {
        lrPartnerOutlet = findViewById(R.id.lrPartnerOutlet);
        switchPartnerOutlets = findViewById(R.id.switchPartnerOutlets);
        sprCategory = findViewById(R.id.sprCategory);
        sprSubCategory = findViewById(R.id.sprSubCategory);
        txtProceed = findViewById(R.id.txtProceed);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Signup - Corporate");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        corporateSubCategoryModels = new ArrayList<>();
        corporateCategoryModels = new ArrayList<>();
        txtProceed.setOnClickListener(this);
        switchPartnerOutlets.setOnToggledListener(this);

        ArrayList<String> placeholderList = new ArrayList<>();
        placeholderList.add("Select");
        setSpinnerAdapter(sprSubCategory, placeholderList);

        ArrayList<String> categoryPlaceHolder = new ArrayList<>();
        placeholderList.add("Select");
        setSpinnerAdapter(sprCategory, categoryPlaceHolder);
    }


    private void setSpinnerAdapter(MaterialBetterSpinner spinner, ArrayList<String> arrayList) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_dropdown_item, arrayList);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtProceed:
                if (isValid()) {
                    Intent intent = new Intent(this, RegCorporateStepTwo.class);
                    intent.putExtra("type", type);
                    intent.putExtra("category_type", str_category_type);
                    startActivity(intent);
                    break;
                }
        }
    }

    private boolean isValid() {
        if (str_category_type.isEmpty()) {
            sprCategory.setError("Please select category!");
            return false;
        }
        if (str_sub_category.isEmpty()) {
            sprCategory.setError("Please select sub category!");
            return false;
        }
        return true;
    }

    @Override
    public void onSwitched(LabeledSwitch labeledSwitch, boolean isOn) {
        isPartnerOutlet = isOn;
    }
}
