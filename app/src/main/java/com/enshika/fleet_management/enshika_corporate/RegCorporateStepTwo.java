package com.enshika.fleet_management.enshika_corporate;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.utils.Vars;
import com.hbb20.CountryCodePicker;
import com.layer_net.stepindicator.StepIndicator;

/**
 * Created by Shamla Tech on 07-07-2018.
 */

public class RegCorporateStepTwo extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    TextView txtProceed;
    StepIndicator step_indicator;
    String type = "", str_category_type = "";
    LinearLayout linearBank, linearImage, linearUniversityStudent, linearMobOperator;
    TextView txtImageHeader;
    RadioGroup rgBankEmployee;
    RadioButton rbCustomer, rbEmployee;
    EditText edtBinNumber, edtCardNumber, edtNumberRange, edtStudentId, edtStudentValidity, edtEmail,
            edtFirstName, edtLastName, edtPhoneNumber, edtEmployeeId;
    RelativeLayout relativeImageUpload;
    ImageView imgDocument;
    CountryCodePicker ccp_country_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_corporate_step_two);
        initUI();
        getBundleDetails();
        prepareView();
    }

    private void getBundleDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("type")) {
                type = bundle.getString("type");
            }
            if (bundle.containsKey("category_type")) {
                str_category_type = bundle.getString("category_type");
            }
        }
    }

    private void prepareView() {
        switch (str_category_type) {
            case Vars.CATEGORY_BANK:
                linearBank.setVisibility(View.VISIBLE);
                linearMobOperator.setVisibility(View.GONE);
                linearUniversityStudent.setVisibility(View.GONE);
                txtImageHeader.setText("Employee ID");
                break;
            case Vars.CATEGORY_MOBILE_OPERATOR:
                linearBank.setVisibility(View.GONE);
                linearUniversityStudent.setVisibility(View.GONE);
                linearMobOperator.setVisibility(View.VISIBLE);
                txtImageHeader.setText("Employee ID");
                linearImage.setVisibility(View.VISIBLE);
                break;
            case Vars.CATEGORY_UNIVERSITY:
                linearBank.setVisibility(View.GONE);
                linearMobOperator.setVisibility(View.GONE);
                if (!type.equals("student")) {
                    txtImageHeader.setText("Employee ID");
                    linearUniversityStudent.setVisibility(View.GONE);
                } else {
                    edtEmail.setHint("Email");
                    txtImageHeader.setText("Student ID");
                    linearUniversityStudent.setVisibility(View.VISIBLE);
                }
                linearImage.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void initUI() {
        edtFirstName = findViewById(R.id.edtFirstName);
        edtLastName = findViewById(R.id.edtLastName);
        edtPhoneNumber = findViewById(R.id.edtPhoneNumber);
        edtEmployeeId = findViewById(R.id.edtEmployeeId);
        relativeImageUpload = findViewById(R.id.relativeImageUpload);
        imgDocument = findViewById(R.id.imgDocument);
        ccp_country_id = findViewById(R.id.ccp_country_id);
        edtEmail = findViewById(R.id.edtEmail);
        linearMobOperator = findViewById(R.id.linearMobOperator);
        edtStudentId = findViewById(R.id.edtStudentId);
        edtStudentValidity = findViewById(R.id.edtStudentValidity);
        linearUniversityStudent = findViewById(R.id.linearUniversityStudent);
        edtNumberRange = findViewById(R.id.edtNumberRange);
        edtBinNumber = findViewById(R.id.edtBinNumber);
        edtCardNumber = findViewById(R.id.edtCardNumber);
        rbCustomer = findViewById(R.id.rbCustomer);
        rbEmployee = findViewById(R.id.rbEmployee);
        rgBankEmployee = findViewById(R.id.rgBankEmployee);
        linearImage = findViewById(R.id.linearImage);
        txtImageHeader = findViewById(R.id.txtImageHeader);
        linearBank = findViewById(R.id.linearBank);
        step_indicator = findViewById(R.id.step_indicator);
        step_indicator.setCurrentStepPosition(1);
        txtProceed = findViewById(R.id.txtProceed);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Signup - Corporate");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        txtProceed.setOnClickListener(this);
        rgBankEmployee.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == rbEmployee.getId()) {
                    edtBinNumber.setVisibility(View.GONE);
                    edtCardNumber.setVisibility(View.GONE);
                    linearImage.setVisibility(View.VISIBLE);
                } else if (checkedId == rbCustomer.getId()) {
                    edtBinNumber.setVisibility(View.VISIBLE);
                    edtCardNumber.setVisibility(View.VISIBLE);
                    linearImage.setVisibility(View.GONE);
                }
            }
        });
        rbCustomer.setChecked(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtProceed:
                startActivity(new Intent(this, RegCorporateStepThree.class));
                break;
        }
    }
}
