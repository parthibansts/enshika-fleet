package com.enshika.fleet_management.development;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Shamla Tech on 27-07-2018.
 */

public class DataModels implements Serializable {
    private ArrayList<DataModel> dataModels;
    private int color;

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public ArrayList<DataModel> getDataModels() {
        return dataModels;
    }

    public void setDataModels(ArrayList<DataModel> dataModels) {
        this.dataModels = dataModels;
    }
}
