package com.enshika.fleet_management.development;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 27-07-2018.
 */

public class DataModel implements Serializable {
    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
