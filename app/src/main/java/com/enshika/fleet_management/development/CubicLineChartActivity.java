package com.enshika.fleet_management.development;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.enshika.fleet_management.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;

public class CubicLineChartActivity extends AppCompatActivity {

    ChartDataModel chartDataModel;
    private LineChart mChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cubic_line_chart);

        initUI();

        setData(getFirstValue(), getSecValue());

        mChart.animateXY(2000, 2000);

        mChart.invalidate();

        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                System.out.println(e.getX() + " " + e.getY());
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    private void initUI() {
        mChart = findViewById(R.id.lineChart);
        mChart.setViewPortOffsets(0, 0, 0, 0);
        mChart.getDescription().setEnabled(false);
        mChart.setTouchEnabled(true);

        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setPinchZoom(false);

        mChart.setDrawGridBackground(false);
        mChart.setMaxHighlightDistance(300);

        XAxis x = mChart.getXAxis();
        x.setAvoidFirstLastClipping(true);
        x.setEnabled(true);
        x.setLabelCount(6, false);
        x.setTextColor(Color.BLACK);
        x.setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE);
        x.setDrawGridLines(false);
        x.setAxisLineColor(Color.WHITE);

        mChart.getAxisLeft().setEnabled(false);
        mChart.getAxisRight().setEnabled(false);
        mChart.getLegend().setEnabled(false);
    }

    private void prepareMarkerView(ChartDataModel model) {
        CustomMarkerView mv = new CustomMarkerView(this, R.layout.custom_marker_view, model);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart
    }

    private void setData(ArrayList<Integer> firstValues, ArrayList<Integer> secondValues) {

        ArrayList<Entry> firstEntry = new ArrayList<Entry>();
        ArrayList<Entry> secEntry = new ArrayList<Entry>();

        prepareChartDataModel(firstValues, secondValues);

        for (int i = 0; i < firstValues.size(); i++) {
            firstEntry.add(new Entry(i, firstValues.get(i)));
        }

        for (int i = 0; i < secondValues.size(); i++) {
            secEntry.add(new Entry(i, secondValues.get(i)));
        }

        LineDataSet set1, set2;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(firstEntry);
            set2 = (LineDataSet) mChart.getData().getDataSetByIndex(1);
            set2.setValues(secEntry);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            set1 = prepareSetUI(firstEntry, Color.RED);
            set2 = prepareSetUI(secEntry, Color.GREEN);

            LineData data = new LineData(set1, set2);
//          data.setValueTypeface(mTfLight);
            data.setValueTextSize(9f);
            data.setDrawValues(false);

            mChart.setData(data);
        }
    }

    private void prepareChartDataModel(ArrayList<Integer> firstValues, ArrayList<Integer> secondValues) {
        ChartDataModel chartDataModel = new ChartDataModel();

        ArrayList<DataModels> dataModelsArrayList = new ArrayList<>();
        DataModels dataModels = new DataModels();

        ArrayList<DataModel> models = new ArrayList<>();
        for (int i = 0; i < firstValues.size(); i++) {
            DataModel dataModel = new DataModel();
            dataModel.setX(i);
            dataModel.setY(firstValues.get(i));
            models.add(dataModel);
        }
        dataModels.setColor(Color.RED);
        dataModels.setDataModels(models);

        dataModelsArrayList.add(dataModels);

        dataModels = new DataModels();
        models = new ArrayList<>();
        for (int i = 0; i < secondValues.size(); i++) {
            DataModel dataModel = new DataModel();
            dataModel.setX(i);
            dataModel.setY(secondValues.get(i));
            models.add(dataModel);
        }
        dataModels.setColor(Color.GREEN);
        dataModels.setDataModels(models);

        dataModelsArrayList.add(dataModels);

        chartDataModel.setDataModels(dataModelsArrayList);

        prepareMarkerView(chartDataModel);
    }

    private LineDataSet prepareSetUI(ArrayList<Entry> entries, int color) {
        LineDataSet set1 = new LineDataSet(entries, "");
        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set1.setCubicIntensity(0.2f);
//      set1.setDrawFilled(true);
        set1.setDrawCircles(false);
        set1.setLineWidth(5f);
        set1.setCircleRadius(4f);
        set1.setCircleColor(color);
        set1.setHighLightColor(Color.rgb(244, 117, 117));
        set1.setColor(color);
        set1.setFillColor(color);
        set1.setFillAlpha(60);
        set1.setDrawHorizontalHighlightIndicator(false);
        set1.setFillFormatter(new IFillFormatter() {
            @Override
            public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                return -10;
            }
        });
        return set1;
    }


    private ArrayList<Integer> getFirstValue() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(50);
        list.add(60);
        list.add(70);
        list.add(60);
        list.add(50);
        list.add(70);
        list.add(80);
        list.add(90);
        list.add(60);
        list.add(50);
        return list;
    }

    private ArrayList<Integer> getSecValue() {
        ArrayList<Integer> secVal = new ArrayList<>();
        secVal.add(20);
        secVal.add(30);
        secVal.add(40);
        secVal.add(50);
        secVal.add(30);
        secVal.add(40);
        secVal.add(50);
        secVal.add(60);
        secVal.add(40);
        secVal.add(20);
        return secVal;
    }
}
