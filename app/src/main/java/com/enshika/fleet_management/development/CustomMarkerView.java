package com.enshika.fleet_management.development;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;

/**
 * Custom implementation of the MarkerView.
 *
 * @author Philipp Jahoda
 */
public class CustomMarkerView extends MarkerView {

    ChartDataModel model;
    private TextView txtOne, txtTwo;
    private ImageView imgOne, imgTwo;

    public CustomMarkerView(Context context, int layoutResource, ChartDataModel model) {
        super(context, layoutResource);

        txtOne = findViewById(R.id.txtOne);
        txtTwo = findViewById(R.id.txtTwo);
        imgOne = findViewById(R.id.imgOne);
        imgTwo = findViewById(R.id.imgTwo);
        this.model = model;
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {


        DataModels dataModelsOne = model.getDataModels().get(0);
        DataModels dataModelsTwo = model.getDataModels().get(1);
        for (int i = 0; i < dataModelsOne.getDataModels().size(); i++) {
            if (dataModelsOne.getDataModels().get(i).getX() == Math.round(e.getX())) {
                txtOne.setText("$" + dataModelsOne.getDataModels().get(i).getY());
                imgOne.setColorFilter(dataModelsOne.getColor(), android.graphics.PorterDuff.Mode.SRC_IN);
            }
        }
        for (int i = 0; i < dataModelsTwo.getDataModels().size(); i++) {
            if (dataModelsTwo.getDataModels().get(i).getX() == Math.round(e.getX())) {
                txtTwo.setText("$" + dataModelsTwo.getDataModels().get(i).getY() + "");
                imgTwo.setColorFilter(dataModelsTwo.getColor(), android.graphics.PorterDuff.Mode.SRC_IN);
            }
        }

        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
