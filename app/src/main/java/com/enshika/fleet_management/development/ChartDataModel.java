package com.enshika.fleet_management.development;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Shamla Tech on 27-07-2018.
 */

public class ChartDataModel implements Serializable {
    private ArrayList<DataModels> dataModels = new ArrayList<>();

    public ArrayList<DataModels> getDataModels() {
        return dataModels;
    }

    public void setDataModels(ArrayList<DataModels> dataModels) {
        this.dataModels = dataModels;
    }
}
