package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 29-06-2018.
 */

public class DriverDetailModel implements Serializable {
    private DriverBankDetailsModel driverBankDetails;

    private String billAddressLineTwo;

    private String mailAddressLineTwo;

    private String password;

    private String joiningDate;

    private String notificationStatus;

    private String companyDriver;

    private String verified;

    private String userInfoId;

    private String userId;

    private String mailStateId;

    private String billStateId;

    private String gender;

    private String region;

    private String regionId;

    private DrivingLicenseModel drivingLicenseModel;

    private String roleId;

    private String lastName;

    private String billCountryId;

    private String companyName;

    private String deleted;

    private String phoneNo;

    private String modelName;

    private String createdBy;

    private String apiSessionKey;

    private String email;

    private String active;

    private String phoneNoCode;

    private String make;

    private String carDriversModel;

    private String companyAddress;

    private String mailAddressLineOne;

    private String billZipCode;

    private String billAddressLineOne;

    private String updatedBy;

    private String recordStatus;

    private String createdAt;

    private String verificationCode;

    private String badgeCount;

    private String firstName;

    private String drivingLicensephoto;

    private String onDuty;

    private String userRole;

    private String billCityId;

    private String updatedAt;

    private String driverReferralCode;

    private CarModel carModel;

    private String mailCityId;

    private String sameAsMailing;

    private String mailCountryId;

    private String applicationStatus;

    private String mailZipCode;

    private String fullName;

    private String photoUrl;

    private String drivingLicense;

    private double depositBalance;

    public double getDepositBalance() {
        return depositBalance;
    }

    public void setDepositBalance(double depositBalance) {
        this.depositBalance = depositBalance;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public DriverBankDetailsModel getDriverBankDetails() {
        return driverBankDetails;
    }

    public void setDriverBankDetails(DriverBankDetailsModel driverBankDetails) {
        this.driverBankDetails = driverBankDetails;
    }

    public String getBillAddressLineTwo() {
        return billAddressLineTwo;
    }

    public void setBillAddressLineTwo(String billAddressLineTwo) {
        this.billAddressLineTwo = billAddressLineTwo;
    }

    public String getMailAddressLineTwo() {
        return mailAddressLineTwo;
    }

    public void setMailAddressLineTwo(String mailAddressLineTwo) {
        this.mailAddressLineTwo = mailAddressLineTwo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(String notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public String getCompanyDriver() {
        return companyDriver;
    }

    public void setCompanyDriver(String companyDriver) {
        this.companyDriver = companyDriver;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public String getUserInfoId() {
        return userInfoId;
    }

    public void setUserInfoId(String userInfoId) {
        this.userInfoId = userInfoId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMailStateId() {
        return mailStateId;
    }

    public void setMailStateId(String mailStateId) {
        this.mailStateId = mailStateId;
    }

    public String getBillStateId() {
        return billStateId;
    }

    public void setBillStateId(String billStateId) {
        this.billStateId = billStateId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public DrivingLicenseModel getDrivingLicenseModel() {
        return drivingLicenseModel;
    }

    public void setDrivingLicenseModel(DrivingLicenseModel drivingLicenseModel) {
        this.drivingLicenseModel = drivingLicenseModel;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBillCountryId() {
        return billCountryId;
    }

    public void setBillCountryId(String billCountryId) {
        this.billCountryId = billCountryId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getApiSessionKey() {
        return apiSessionKey;
    }

    public void setApiSessionKey(String apiSessionKey) {
        this.apiSessionKey = apiSessionKey;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getPhoneNoCode() {
        return phoneNoCode;
    }

    public void setPhoneNoCode(String phoneNoCode) {
        this.phoneNoCode = phoneNoCode;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getCarDriversModel() {
        return carDriversModel;
    }

    public void setCarDriversModel(String carDriversModel) {
        this.carDriversModel = carDriversModel;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getMailAddressLineOne() {
        return mailAddressLineOne;
    }

    public void setMailAddressLineOne(String mailAddressLineOne) {
        this.mailAddressLineOne = mailAddressLineOne;
    }

    public String getBillZipCode() {
        return billZipCode;
    }

    public void setBillZipCode(String billZipCode) {
        this.billZipCode = billZipCode;
    }

    public String getBillAddressLineOne() {
        return billAddressLineOne;
    }

    public void setBillAddressLineOne(String billAddressLineOne) {
        this.billAddressLineOne = billAddressLineOne;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getBadgeCount() {
        return badgeCount;
    }

    public void setBadgeCount(String badgeCount) {
        this.badgeCount = badgeCount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDrivingLicensephoto() {
        return drivingLicensephoto;
    }

    public void setDrivingLicensephoto(String drivingLicensephoto) {
        this.drivingLicensephoto = drivingLicensephoto;
    }

    public String getOnDuty() {
        return onDuty;
    }

    public void setOnDuty(String onDuty) {
        this.onDuty = onDuty;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getBillCityId() {
        return billCityId;
    }

    public void setBillCityId(String billCityId) {
        this.billCityId = billCityId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDriverReferralCode() {
        return driverReferralCode;
    }

    public void setDriverReferralCode(String driverReferralCode) {
        this.driverReferralCode = driverReferralCode;
    }

    public CarModel getCarModel() {
        return carModel;
    }

    public void setCarModel(CarModel carModel) {
        this.carModel = carModel;
    }

    public String getMailCityId() {
        return mailCityId;
    }

    public void setMailCityId(String mailCityId) {
        this.mailCityId = mailCityId;
    }

    public String getSameAsMailing() {
        return sameAsMailing;
    }

    public void setSameAsMailing(String sameAsMailing) {
        this.sameAsMailing = sameAsMailing;
    }

    public String getMailCountryId() {
        return mailCountryId;
    }

    public void setMailCountryId(String mailCountryId) {
        this.mailCountryId = mailCountryId;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public String getMailZipCode() {
        return mailZipCode;
    }

    public void setMailZipCode(String mailZipCode) {
        this.mailZipCode = mailZipCode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getDrivingLicense() {
        return drivingLicense;
    }

    public void setDrivingLicense(String drivingLicense) {
        this.drivingLicense = drivingLicense;
    }

    @Override
    public String toString() {
        return "ClassPojo [driverBankDetails = " + driverBankDetails + ", billAddressLineTwo = " + billAddressLineTwo + ", mailAddressLineTwo = " + mailAddressLineTwo + ", password = " + password + ", joiningDate = " + joiningDate + ", notificationStatus = " + notificationStatus + ", companyDriver = " + companyDriver + ", verified = " + verified + ", userInfoId = " + userInfoId + ", userId = " + userId + ", mailStateId = " + mailStateId + ", billStateId = " + billStateId + ", gender = " + gender + ", drivingLicenseModel = " + drivingLicenseModel + ", roleId = " + roleId + ", lastName = " + lastName + ", billCountryId = " + billCountryId + ", companyName = " + companyName + ", deleted = " + deleted + ", phoneNo = " + phoneNo + ", modelName = " + modelName + ", createdBy = " + createdBy + ", apiSessionKey = " + apiSessionKey + ", email = " + email + ", active = " + active + ", phoneNoCode = " + phoneNoCode + ", make = " + make + ", carDriversModel = " + carDriversModel + ", companyAddress = " + companyAddress + ", mailAddressLineOne = " + mailAddressLineOne + ", billZipCode = " + billZipCode + ", billAddressLineOne = " + billAddressLineOne + ", updatedBy = " + updatedBy + ", recordStatus = " + recordStatus + ", createdAt = " + createdAt + ", verificationCode = " + verificationCode + ", badgeCount = " + badgeCount + ", firstName = " + firstName + ", drivingLicensephoto = " + drivingLicensephoto + ", onDuty = " + onDuty + ", userRole = " + userRole + ", billCityId = " + billCityId + ", updatedAt = " + updatedAt + ", driverReferralCode = " + driverReferralCode + ", carModel = " + carModel + ", mailCityId = " + mailCityId + ", sameAsMailing = " + sameAsMailing + ", mailCountryId = " + mailCountryId + ", applicationStatus = " + applicationStatus + ", mailZipCode = " + mailZipCode + ", fullName = " + fullName + ", photoUrl = " + photoUrl + ", drivingLicense = " + drivingLicense + "]";
    }
}
