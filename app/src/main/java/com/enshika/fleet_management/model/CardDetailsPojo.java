package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 24-07-2018.
 */

public class CardDetailsPojo implements Serializable {
    private String holder_name;

    private String wallet_name;

    private String wallet_number;

    private String expiry_date;

    public String getHolder_name ()
    {
        return holder_name;
    }

    public void setHolder_name (String holder_name)
    {
        this.holder_name = holder_name;
    }

    public String getWallet_name ()
    {
        return wallet_name;
    }

    public void setWallet_name (String wallet_name)
    {
        this.wallet_name = wallet_name;
    }

    public String getWallet_number ()
    {
        return wallet_number;
    }

    public void setWallet_number (String wallet_number)
    {
        this.wallet_number = wallet_number;
    }

    public String getExpiry_date ()
    {
        return expiry_date;
    }

    public void setExpiry_date (String expiry_date)
    {
        this.expiry_date = expiry_date;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [holder_name = "+holder_name+", wallet_name = "+wallet_name+", wallet_number = "+wallet_number+", expiry_date = "+expiry_date+"]";
    }
}
