package com.enshika.fleet_management.model;

import java.util.ArrayList;

public class DriversResponsePojo {
    private String message;

    private ArrayList<DriversPojo> driverLists;

    private String status;

    private String isDriverAddable;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DriversPojo> getDriverLists() {
        return driverLists;
    }

    public void setDriverLists(ArrayList<DriversPojo> driverLists) {
        this.driverLists = driverLists;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsDriverAddable() {
        return isDriverAddable;
    }

    public void setIsDriverAddable(String isDriverAddable) {
        this.isDriverAddable = isDriverAddable;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", driverLists = " + driverLists + ", status = " + status + ", isDriverAddable = " + isDriverAddable + "]";
    }
}
