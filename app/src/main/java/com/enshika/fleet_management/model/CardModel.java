package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 24-07-2018.
 */

public class CardModel implements Serializable {

    private String user_id;

    private String wallet_id;

    private CardDetailsPojo details;

    private String merchant_id;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public CardDetailsPojo getDetails() {
        return details;
    }

    public void setDetails(CardDetailsPojo details) {
        this.details = details;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getWallet_id() {
        return wallet_id;
    }

    public void setWallet_id(String wallet_id) {
        this.wallet_id = wallet_id;
    }

    @Override
    public String toString() {
        return "CardModel{" +
                "user_id='" + user_id + '\'' +
                ", wallet_id='" + wallet_id + '\'' +
                ", details=" + details +
                ", merchant_id='" + merchant_id + '\'' +
                '}';
    }
}
