package com.enshika.fleet_management.model;

/**
 * Created by Shamla Tech on 21-07-2018.
 */

public class WalletUpdatePinPojo {
    private String userId;
    private String walletPin;
    private String newWalletPin;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWalletPin() {
        return walletPin;
    }

    public void setWalletPin(String walletPin) {
        this.walletPin = walletPin;
    }

    public String getNewWalletPin() {
        return newWalletPin;
    }

    public void setNewWalletPin(String newWalletPin) {
        this.newWalletPin = newWalletPin;
    }
}
