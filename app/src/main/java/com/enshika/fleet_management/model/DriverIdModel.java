package com.enshika.fleet_management.model;

/**
 * Created by Shamla Tech on 03-07-2018.
 */

public class DriverIdModel {
    private String success;
    private String driverId;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }
}
