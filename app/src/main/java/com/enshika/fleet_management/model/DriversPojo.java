package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Vino on 20-03-2018.
 */

public class DriversPojo implements Serializable {
    private String rate;

    private String driverId;

    private String photoUrl;

    private String firstName;

    private String onLineStatus;

    private double balanceAmount;

    public double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOnLineStatus() {
        return onLineStatus;
    }

    public void setOnLineStatus(String onLineStatus) {
        this.onLineStatus = onLineStatus;
    }
}
