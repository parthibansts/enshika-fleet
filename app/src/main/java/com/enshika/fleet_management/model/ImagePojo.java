package com.enshika.fleet_management.model;

/**
 * Created by Shamla Tech on 19-07-2018.
 */

public class ImagePojo
{
    private String imageURL;

    public String getImageURL ()
    {
        return imageURL;
    }

    public void setImageURL (String imageURL)
    {
        this.imageURL = imageURL;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [imageURL = "+imageURL+"]";
    }
}
