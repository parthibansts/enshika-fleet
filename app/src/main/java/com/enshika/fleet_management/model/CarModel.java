package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 29-06-2018.
 */

public class CarModel implements Serializable {
    private String vehicleCommercialLicencePhotoUrl;

    private String carTypeId;

    private String carId;

    private String inspectionReportPhotoUrl;

    private String insurancePhotoUrl;

    private String backImgUrl;

    private String carYear;

    private String updatedBy;

    private String recordStatus;

    private String carType;

    private String updatedAt;

    private String modelName;

    private String registrationPhotoUrl;

    private String carTitle;

    private String createdBy;

    private String noOfPassenger;

    private String createdAt;

    private String userId;

    private String frontImgUrl;

    private String driverId;

    private String owner;

    private String carPlateNo;

    private String make;

    private String carColor;

    public String getVehicleCommercialLicencePhotoUrl ()
    {
        return vehicleCommercialLicencePhotoUrl;
    }

    public void setVehicleCommercialLicencePhotoUrl (String vehicleCommercialLicencePhotoUrl)
    {
        this.vehicleCommercialLicencePhotoUrl = vehicleCommercialLicencePhotoUrl;
    }

    public String getCarTypeId ()
    {
        return carTypeId;
    }

    public void setCarTypeId (String carTypeId)
    {
        this.carTypeId = carTypeId;
    }

    public String getCarId ()
    {
        return carId;
    }

    public void setCarId (String carId)
    {
        this.carId = carId;
    }

    public String getInspectionReportPhotoUrl ()
    {
        return inspectionReportPhotoUrl;
    }

    public void setInspectionReportPhotoUrl (String inspectionReportPhotoUrl)
    {
        this.inspectionReportPhotoUrl = inspectionReportPhotoUrl;
    }

    public String getInsurancePhotoUrl ()
    {
        return insurancePhotoUrl;
    }

    public void setInsurancePhotoUrl (String insurancePhotoUrl)
    {
        this.insurancePhotoUrl = insurancePhotoUrl;
    }

    public String getBackImgUrl ()
    {
        return backImgUrl;
    }

    public void setBackImgUrl (String backImgUrl)
    {
        this.backImgUrl = backImgUrl;
    }

    public String getCarYear ()
    {
        return carYear;
    }

    public void setCarYear (String carYear)
    {
        this.carYear = carYear;
    }

    public String getUpdatedBy ()
    {
        return updatedBy;
    }

    public void setUpdatedBy (String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getRecordStatus ()
    {
        return recordStatus;
    }

    public void setRecordStatus (String recordStatus)
    {
        this.recordStatus = recordStatus;
    }

    public String getCarType ()
    {
        return carType;
    }

    public void setCarType (String carType)
    {
        this.carType = carType;
    }

    public String getUpdatedAt ()
    {
        return updatedAt;
    }

    public void setUpdatedAt (String updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public String getModelName ()
    {
        return modelName;
    }

    public void setModelName (String modelName)
    {
        this.modelName = modelName;
    }

    public String getRegistrationPhotoUrl ()
    {
        return registrationPhotoUrl;
    }

    public void setRegistrationPhotoUrl (String registrationPhotoUrl)
    {
        this.registrationPhotoUrl = registrationPhotoUrl;
    }

    public String getCarTitle ()
    {
        return carTitle;
    }

    public void setCarTitle (String carTitle)
    {
        this.carTitle = carTitle;
    }

    public String getCreatedBy ()
    {
        return createdBy;
    }

    public void setCreatedBy (String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getNoOfPassenger ()
    {
        return noOfPassenger;
    }

    public void setNoOfPassenger (String noOfPassenger)
    {
        this.noOfPassenger = noOfPassenger;
    }

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public void setCreatedAt (String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String getUserId ()
    {
        return userId;
    }

    public void setUserId (String userId)
    {
        this.userId = userId;
    }

    public String getFrontImgUrl ()
    {
        return frontImgUrl;
    }

    public void setFrontImgUrl (String frontImgUrl)
    {
        this.frontImgUrl = frontImgUrl;
    }

    public String getDriverId ()
    {
        return driverId;
    }

    public void setDriverId (String driverId)
    {
        this.driverId = driverId;
    }

    public String getOwner ()
    {
        return owner;
    }

    public void setOwner (String owner)
    {
        this.owner = owner;
    }

    public String getCarPlateNo ()
    {
        return carPlateNo;
    }

    public void setCarPlateNo (String carPlateNo)
    {
        this.carPlateNo = carPlateNo;
    }

    public String getMake ()
    {
        return make;
    }

    public void setMake (String make)
    {
        this.make = make;
    }

    public String getCarColor ()
    {
        return carColor;
    }

    public void setCarColor (String carColor)
    {
        this.carColor = carColor;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [vehicleCommercialLicencePhotoUrl = "+vehicleCommercialLicencePhotoUrl+", carTypeId = "+carTypeId+", carId = "+carId+", inspectionReportPhotoUrl = "+inspectionReportPhotoUrl+", insurancePhotoUrl = "+insurancePhotoUrl+", backImgUrl = "+backImgUrl+", carYear = "+carYear+", updatedBy = "+updatedBy+", recordStatus = "+recordStatus+", carType = "+carType+", updatedAt = "+updatedAt+", modelName = "+modelName+", registrationPhotoUrl = "+registrationPhotoUrl+", carTitle = "+carTitle+", createdBy = "+createdBy+", noOfPassenger = "+noOfPassenger+", createdAt = "+createdAt+", userId = "+userId+", frontImgUrl = "+frontImgUrl+", driverId = "+driverId+", owner = "+owner+", carPlateNo = "+carPlateNo+", make = "+make+", carColor = "+carColor+"]";
    }
}
