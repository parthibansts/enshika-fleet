package com.enshika.fleet_management.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 24-07-2018.
 */

public class CardValidateReqModel implements Serializable {
    private String amount;

    @SerializedName("3d_url_response")
    private String url_response;

    private String user_id;

    private String wallet_id;

    private CardDetailsPojo details;

    private String cvv;

    private String merchant_id;

    public String getUrl_response() {
        return url_response;
    }

    public void setUrl_response(String url_response) {
        this.url_response = url_response;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public CardDetailsPojo getDetails() {
        return details;
    }

    public void setDetails(CardDetailsPojo details) {
        this.details = details;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getWallet_id() {
        return wallet_id;
    }

    public void setWallet_id(String wallet_id) {
        this.wallet_id = wallet_id;
    }

    @Override
    public String toString() {
        return "ClassPojo [amount = " + amount + ", details = " + details + ", cvv = " + cvv + ", merchant_id = " + merchant_id + "]";
    }
}
