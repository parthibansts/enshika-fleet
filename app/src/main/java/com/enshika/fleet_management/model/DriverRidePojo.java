package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 30-06-2018.
 */

public class DriverRidePojo implements Serializable {
    private String carType;

    private String total;

    private long bookingTime;

    private String destinationAddress;

    private String carPlateNo;

    private String sourceAddress;

    private String tourId;

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public long getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(long bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getCarPlateNo() {
        return carPlateNo;
    }

    public void setCarPlateNo(String carPlateNo) {
        this.carPlateNo = carPlateNo;
    }

    public String getSourceAddress() {
        return sourceAddress;
    }

    public void setSourceAddress(String sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    public String getTourId() {
        return tourId;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    @Override
    public String toString() {
        return "ClassPojo [carType = " + carType + ", total = " + total + ", bookingTime = " + bookingTime + ", destinationAddress = " + destinationAddress + ", carPlateNo = " + carPlateNo + ", sourceAddress = " + sourceAddress + ", tourId = " + tourId + "]";
    }
}
