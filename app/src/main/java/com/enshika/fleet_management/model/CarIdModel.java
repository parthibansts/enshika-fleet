package com.enshika.fleet_management.model;

/**
 * Created by Shamla Tech on 03-07-2018.
 */

public class CarIdModel {
    private boolean success;
    private String carId;

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }
}
