package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 21-07-2018.
 */

public class MobileMoneyOperators implements Serializable{
    private String operatorImage;

    private long operatorId;

    private String rSwitch;

    private String operator;

    private String countryCode;

    private long mobileNumber;

    public String getrSwitch() {
        return rSwitch;
    }

    public void setrSwitch(String rSwitch) {
        this.rSwitch = rSwitch;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getOperatorImage ()
    {
        return operatorImage;
    }

    public void setOperatorImage (String operatorImage)
    {
        this.operatorImage = operatorImage;
    }

    public long getOperatorId ()
    {
        return operatorId;
    }

    public void setOperatorId (long operatorId)
    {
        this.operatorId = operatorId;
    }

    public String getRSwitch ()
    {
        return rSwitch;
    }

    public void setRSwitch (String rSwitch)
    {
        this.rSwitch = rSwitch;
    }

    public String getOperator ()
    {
        return operator;
    }

    public void setOperator (String operator)
    {
        this.operator = operator;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [operatorImage = "+operatorImage+", operatorId = "+operatorId+", rSwitch = "+rSwitch+", operator = "+operator+"]";
    }
}
