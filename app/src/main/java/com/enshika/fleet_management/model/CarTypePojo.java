package com.enshika.fleet_management.model;

/**
 * Created by Shamla Tech on 28-06-2018.
 */

public class CarTypePojo {
    private String carTypeId, carType;

    public String getCarTypeId() {
        return carTypeId;
    }

    public void setCarTypeId(String carTypeId) {
        this.carTypeId = carTypeId;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    @Override
    public String toString() {
        return carType;
    }
}
