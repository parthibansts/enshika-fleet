package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 21-07-2018.
 */

public class NewMobileMoneyModel implements Serializable {
    private String userId;
    private String mobileOperatorId;
    private String mobileMoneyId;
    private String mobNo;
    private String mobileNo;
    private String mobileNumber;
    private String countryCode;
    private String otp;

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getMobileMoneyId() {
        return mobileMoneyId;
    }

    public void setMobileMoneyId(String mobileMoneyId) {
        this.mobileMoneyId = mobileMoneyId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMobileOperatorId() {
        return mobileOperatorId;
    }

    public void setMobileOperatorId(String mobileOperatorId) {
        this.mobileOperatorId = mobileOperatorId;
    }

    public String getMobNo() {
        return mobNo;
    }

    public void setMobNo(String mobNo) {
        this.mobNo = mobNo;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
