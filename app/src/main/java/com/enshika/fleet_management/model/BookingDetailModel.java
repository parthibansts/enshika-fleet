package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 02-07-2018.
 */

public class BookingDetailModel implements Serializable {
    private long waitingTime;

    private String waitingTimeFare;

    private long duration;

    private String surgeFare;

    private String baseFare;

    private String total;

    private String pNote;

    private String pickupFavouriteLocationsModel;

    private String invoiceTotal;

    private String driverManualBooking;

    private String pPhone;

    private String bookingType;

    private String minimumFare;

    private String refunded;

    private String refundAmount;

    private String surgePriceApplied;

    private String carPlateNo;

    private String tourRideLater;

    private String initialFare;

    private String totalTaxAmount;

    private String pRate;

    private String destinationFavouriteLocationsModel;

    private String rideLaterPickupTime;

    private String pLastName;

    private String sourceGeolocation;

    private String lastName;

    private String carTypeId;

    private String charges;

    private String perKmFare;

    private long bookingTime;

    private String status;

    private String multicityCountryId;

    private String adminSettlementAmount;

    private String paymentStatus;

    private String rideLaterPickupTimeLogs;

    private String driverAmountTotal;

    private String pEmail;

    private String pPhotoUrl;

    private double sLongitude;

    private double dLatitude;

    private String discount;

    private String phoneNo;

    private String promoCodeId;

    private String createdBy;

    private String email;

    private String destinationAddress;

    private String criticalTourRideLater;

    private String destinationGeolocation;

    private String phoneNoCode;

    private String language;

    private String paymentTriggerById;

    private String finalAmountCollected;

    private String pPhoneCode;

    private String tourId;

    private String paymentMode;

    private String carId;

    private String rideLaterLastNotification;

    private String paymentGatewayId;

    private String cardOwner;

    private String bookingFees;

    private String cardBooking;

    private String passengerId;

    private double dLongitude;

    private String pickupFavouriteLocationsId;

    private boolean rideLater;

    private String mode;

    private String updatedBy;

    private String recordStatus;

    private String carType;

    private String distance;

    private double sLatitude;

    private String subTotal;

    private String createdAt;

    private String driverId;

    private String multicitySurgePricingId;

    private String surgeRate;

    private String firstName;

    private String fine;

    private String dNote;

    private String sourceAddress;

    private String promoCode;

    private String destinationFavouriteLocationsId;

    private String freeDistance;

    private String userTourId;

    private String percentage;

    private String paymentType;

    private String tollAmount;

    private String perMinuteFare;

    private String usedCredits;

    private String multicityCityRegionId;

    private String fixedFare;

    private String promoCodeApplied;

    private String promoDiscount;

    private String pFirstName;

    private String updatedAt;

    private String acknowledged;

    private String photoUrl;

    private String driverAmount;

    private String dRate;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPNote() {
        return pNote;
    }

    public void setPNote(String pNote) {
        this.pNote = pNote;
    }

    public String getPickupFavouriteLocationsModel() {
        return pickupFavouriteLocationsModel;
    }

    public void setPickupFavouriteLocationsModel(String pickupFavouriteLocationsModel) {
        this.pickupFavouriteLocationsModel = pickupFavouriteLocationsModel;
    }

    public String getInvoiceTotal() {
        return invoiceTotal;
    }

    public void setInvoiceTotal(String invoiceTotal) {
        this.invoiceTotal = invoiceTotal;
    }

    public String getDriverManualBooking() {
        return driverManualBooking;
    }

    public void setDriverManualBooking(String driverManualBooking) {
        this.driverManualBooking = driverManualBooking;
    }

    public String getPPhone() {
        return pPhone;
    }

    public void setPPhone(String pPhone) {
        this.pPhone = pPhone;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getMinimumFare() {
        return minimumFare;
    }

    public void setMinimumFare(String minimumFare) {
        this.minimumFare = minimumFare;
    }

    public String getRefunded() {
        return refunded;
    }

    public void setRefunded(String refunded) {
        this.refunded = refunded;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getSurgePriceApplied() {
        return surgePriceApplied;
    }

    public void setSurgePriceApplied(String surgePriceApplied) {
        this.surgePriceApplied = surgePriceApplied;
    }

    public String getCarPlateNo() {
        return carPlateNo;
    }

    public void setCarPlateNo(String carPlateNo) {
        this.carPlateNo = carPlateNo;
    }

    public String getTourRideLater() {
        return tourRideLater;
    }

    public void setTourRideLater(String tourRideLater) {
        this.tourRideLater = tourRideLater;
    }

    public String getInitialFare() {
        return initialFare;
    }

    public void setInitialFare(String initialFare) {
        this.initialFare = initialFare;
    }

    public String getTotalTaxAmount() {
        return totalTaxAmount;
    }

    public void setTotalTaxAmount(String totalTaxAmount) {
        this.totalTaxAmount = totalTaxAmount;
    }

    public String getPRate() {
        return pRate;
    }

    public void setPRate(String pRate) {
        this.pRate = pRate;
    }

    public String getDestinationFavouriteLocationsModel() {
        return destinationFavouriteLocationsModel;
    }

    public void setDestinationFavouriteLocationsModel(String destinationFavouriteLocationsModel) {
        this.destinationFavouriteLocationsModel = destinationFavouriteLocationsModel;
    }

    public String getRideLaterPickupTime() {
        return rideLaterPickupTime;
    }

    public void setRideLaterPickupTime(String rideLaterPickupTime) {
        this.rideLaterPickupTime = rideLaterPickupTime;
    }

    public String getPLastName() {
        return pLastName;
    }

    public void setPLastName(String pLastName) {
        this.pLastName = pLastName;
    }

    public String getSourceGeolocation() {
        return sourceGeolocation;
    }

    public void setSourceGeolocation(String sourceGeolocation) {
        this.sourceGeolocation = sourceGeolocation;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCarTypeId() {
        return carTypeId;
    }

    public void setCarTypeId(String carTypeId) {
        this.carTypeId = carTypeId;
    }

    public String getCharges() {
        return charges;
    }

    public void setCharges(String charges) {
        this.charges = charges;
    }

    public String getPerKmFare() {
        return perKmFare;
    }

    public void setPerKmFare(String perKmFare) {
        this.perKmFare = perKmFare;
    }

    public long getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(long bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMulticityCountryId() {
        return multicityCountryId;
    }

    public void setMulticityCountryId(String multicityCountryId) {
        this.multicityCountryId = multicityCountryId;
    }

    public String getAdminSettlementAmount() {
        return adminSettlementAmount;
    }

    public void setAdminSettlementAmount(String adminSettlementAmount) {
        this.adminSettlementAmount = adminSettlementAmount;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getRideLaterPickupTimeLogs() {
        return rideLaterPickupTimeLogs;
    }

    public void setRideLaterPickupTimeLogs(String rideLaterPickupTimeLogs) {
        this.rideLaterPickupTimeLogs = rideLaterPickupTimeLogs;
    }

    public String getDriverAmountTotal() {
        return driverAmountTotal;
    }

    public void setDriverAmountTotal(String driverAmountTotal) {
        this.driverAmountTotal = driverAmountTotal;
    }

    public String getPEmail() {
        return pEmail;
    }

    public void setPEmail(String pEmail) {
        this.pEmail = pEmail;
    }

    public String getPPhotoUrl() {
        return pPhotoUrl;
    }

    public void setPPhotoUrl(String pPhotoUrl) {
        this.pPhotoUrl = pPhotoUrl;
    }

    public double getSLongitude() {
        return sLongitude;
    }

    public void setSLongitude(double sLongitude) {
        this.sLongitude = sLongitude;
    }

    public double getDLatitude() {
        return dLatitude;
    }

    public void setDLatitude(double dLatitude) {
        this.dLatitude = dLatitude;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getPromoCodeId() {
        return promoCodeId;
    }

    public void setPromoCodeId(String promoCodeId) {
        this.promoCodeId = promoCodeId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getCriticalTourRideLater() {
        return criticalTourRideLater;
    }

    public void setCriticalTourRideLater(String criticalTourRideLater) {
        this.criticalTourRideLater = criticalTourRideLater;
    }

    public String getDestinationGeolocation() {
        return destinationGeolocation;
    }

    public void setDestinationGeolocation(String destinationGeolocation) {
        this.destinationGeolocation = destinationGeolocation;
    }

    public String getPhoneNoCode() {
        return phoneNoCode;
    }

    public void setPhoneNoCode(String phoneNoCode) {
        this.phoneNoCode = phoneNoCode;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPaymentTriggerById() {
        return paymentTriggerById;
    }

    public void setPaymentTriggerById(String paymentTriggerById) {
        this.paymentTriggerById = paymentTriggerById;
    }

    public String getFinalAmountCollected() {
        return finalAmountCollected;
    }

    public void setFinalAmountCollected(String finalAmountCollected) {
        this.finalAmountCollected = finalAmountCollected;
    }

    public String getPPhoneCode() {
        return pPhoneCode;
    }

    public void setPPhoneCode(String pPhoneCode) {
        this.pPhoneCode = pPhoneCode;
    }

    public String getTourId() {
        return tourId;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getRideLaterLastNotification() {
        return rideLaterLastNotification;
    }

    public void setRideLaterLastNotification(String rideLaterLastNotification) {
        this.rideLaterLastNotification = rideLaterLastNotification;
    }

    public String getPaymentGatewayId() {
        return paymentGatewayId;
    }

    public void setPaymentGatewayId(String paymentGatewayId) {
        this.paymentGatewayId = paymentGatewayId;
    }

    public String getCardOwner() {
        return cardOwner;
    }

    public void setCardOwner(String cardOwner) {
        this.cardOwner = cardOwner;
    }

    public String getBookingFees() {
        return bookingFees;
    }

    public void setBookingFees(String bookingFees) {
        this.bookingFees = bookingFees;
    }

    public String getCardBooking() {
        return cardBooking;
    }

    public void setCardBooking(String cardBooking) {
        this.cardBooking = cardBooking;
    }

    public String getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }

    public double getDLongitude() {
        return dLongitude;
    }

    public void setDLongitude(double dLongitude) {
        this.dLongitude = dLongitude;
    }

    public String getPickupFavouriteLocationsId() {
        return pickupFavouriteLocationsId;
    }

    public void setPickupFavouriteLocationsId(String pickupFavouriteLocationsId) {
        this.pickupFavouriteLocationsId = pickupFavouriteLocationsId;
    }

    public boolean getRideLater() {
        return rideLater;
    }

    public void setRideLater(boolean rideLater) {
        this.rideLater = rideLater;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public double getSLatitude() {
        return sLatitude;
    }

    public void setSLatitude(double sLatitude) {
        this.sLatitude = sLatitude;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getMulticitySurgePricingId() {
        return multicitySurgePricingId;
    }

    public void setMulticitySurgePricingId(String multicitySurgePricingId) {
        this.multicitySurgePricingId = multicitySurgePricingId;
    }

    public String getSurgeRate() {
        return surgeRate;
    }

    public void setSurgeRate(String surgeRate) {
        this.surgeRate = surgeRate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFine() {
        return fine;
    }

    public void setFine(String fine) {
        this.fine = fine;
    }

    public String getDNote() {
        return dNote;
    }

    public void setDNote(String dNote) {
        this.dNote = dNote;
    }

    public String getSourceAddress() {
        return sourceAddress;
    }

    public void setSourceAddress(String sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getDestinationFavouriteLocationsId() {
        return destinationFavouriteLocationsId;
    }

    public void setDestinationFavouriteLocationsId(String destinationFavouriteLocationsId) {
        this.destinationFavouriteLocationsId = destinationFavouriteLocationsId;
    }

    public String getFreeDistance() {
        return freeDistance;
    }

    public void setFreeDistance(String freeDistance) {
        this.freeDistance = freeDistance;
    }

    public String getUserTourId() {
        return userTourId;
    }

    public void setUserTourId(String userTourId) {
        this.userTourId = userTourId;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(String tollAmount) {
        this.tollAmount = tollAmount;
    }

    public String getPerMinuteFare() {
        return perMinuteFare;
    }

    public void setPerMinuteFare(String perMinuteFare) {
        this.perMinuteFare = perMinuteFare;
    }

    public String getUsedCredits() {
        return usedCredits;
    }

    public void setUsedCredits(String usedCredits) {
        this.usedCredits = usedCredits;
    }

    public String getMulticityCityRegionId() {
        return multicityCityRegionId;
    }

    public void setMulticityCityRegionId(String multicityCityRegionId) {
        this.multicityCityRegionId = multicityCityRegionId;
    }

    public String getFixedFare() {
        return fixedFare;
    }

    public void setFixedFare(String fixedFare) {
        this.fixedFare = fixedFare;
    }

    public String getPromoCodeApplied() {
        return promoCodeApplied;
    }

    public void setPromoCodeApplied(String promoCodeApplied) {
        this.promoCodeApplied = promoCodeApplied;
    }

    public String getPromoDiscount() {
        return promoDiscount;
    }

    public void setPromoDiscount(String promoDiscount) {
        this.promoDiscount = promoDiscount;
    }

    public String getPFirstName() {
        return pFirstName;
    }

    public void setPFirstName(String pFirstName) {
        this.pFirstName = pFirstName;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getAcknowledged() {
        return acknowledged;
    }

    public void setAcknowledged(String acknowledged) {
        this.acknowledged = acknowledged;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getDriverAmount() {
        return driverAmount;
    }

    public void setDriverAmount(String driverAmount) {
        this.driverAmount = driverAmount;
    }

    public String getDRate() {
        return dRate;
    }

    public void setDRate(String dRate) {
        this.dRate = dRate;
    }


    public long getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(long waitingTime) {
        this.waitingTime = waitingTime;
    }

    public String getWaitingTimeFare() {
        return waitingTimeFare;
    }

    public void setWaitingTimeFare(String waitingTimeFare) {
        this.waitingTimeFare = waitingTimeFare;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getSurgeFare() {
        return surgeFare;
    }

    public void setSurgeFare(String surgeFare) {
        this.surgeFare = surgeFare;
    }

    public String getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(String baseFare) {
        this.baseFare = baseFare;
    }

    public String getpNote() {
        return pNote;
    }

    public void setpNote(String pNote) {
        this.pNote = pNote;
    }

    public String getpPhone() {
        return pPhone;
    }

    public void setpPhone(String pPhone) {
        this.pPhone = pPhone;
    }

    public String getpRate() {
        return pRate;
    }

    public void setpRate(String pRate) {
        this.pRate = pRate;
    }

    public String getpLastName() {
        return pLastName;
    }

    public void setpLastName(String pLastName) {
        this.pLastName = pLastName;
    }

    public String getpEmail() {
        return pEmail;
    }

    public void setpEmail(String pEmail) {
        this.pEmail = pEmail;
    }

    public String getpPhotoUrl() {
        return pPhotoUrl;
    }

    public void setpPhotoUrl(String pPhotoUrl) {
        this.pPhotoUrl = pPhotoUrl;
    }

    public double getsLongitude() {
        return sLongitude;
    }

    public void setsLongitude(double sLongitude) {
        this.sLongitude = sLongitude;
    }

    public double getdLatitude() {
        return dLatitude;
    }

    public void setdLatitude(double dLatitude) {
        this.dLatitude = dLatitude;
    }

    public String getpPhoneCode() {
        return pPhoneCode;
    }

    public void setpPhoneCode(String pPhoneCode) {
        this.pPhoneCode = pPhoneCode;
    }

    public double getdLongitude() {
        return dLongitude;
    }

    public void setdLongitude(double dLongitude) {
        this.dLongitude = dLongitude;
    }

    public double getsLatitude() {
        return sLatitude;
    }

    public void setsLatitude(double sLatitude) {
        this.sLatitude = sLatitude;
    }

    public String getdNote() {
        return dNote;
    }

    public void setdNote(String dNote) {
        this.dNote = dNote;
    }

    public String getpFirstName() {
        return pFirstName;
    }

    public void setpFirstName(String pFirstName) {
        this.pFirstName = pFirstName;
    }

    public String getdRate() {
        return dRate;
    }

    public void setdRate(String dRate) {
        this.dRate = dRate;
    }

    @Override
    public String toString() {
        return "BookingDetailModel{" +
                "waitingTime='" + waitingTime + '\'' +
                ", waitingTimeFare='" + waitingTimeFare + '\'' +
                ", duration='" + duration + '\'' +
                ", surgeFare='" + surgeFare + '\'' +
                ", baseFare='" + baseFare + '\'' +
                ", total='" + total + '\'' +
                ", pNote='" + pNote + '\'' +
                ", pickupFavouriteLocationsModel='" + pickupFavouriteLocationsModel + '\'' +
                ", invoiceTotal='" + invoiceTotal + '\'' +
                ", driverManualBooking='" + driverManualBooking + '\'' +
                ", pPhone='" + pPhone + '\'' +
                ", bookingType='" + bookingType + '\'' +
                ", minimumFare='" + minimumFare + '\'' +
                ", refunded='" + refunded + '\'' +
                ", refundAmount='" + refundAmount + '\'' +
                ", surgePriceApplied='" + surgePriceApplied + '\'' +
                ", carPlateNo='" + carPlateNo + '\'' +
                ", tourRideLater='" + tourRideLater + '\'' +
                ", initialFare='" + initialFare + '\'' +
                ", totalTaxAmount='" + totalTaxAmount + '\'' +
                ", pRate='" + pRate + '\'' +
                ", destinationFavouriteLocationsModel='" + destinationFavouriteLocationsModel + '\'' +
                ", rideLaterPickupTime='" + rideLaterPickupTime + '\'' +
                ", pLastName='" + pLastName + '\'' +
                ", sourceGeolocation='" + sourceGeolocation + '\'' +
                ", lastName='" + lastName + '\'' +
                ", carTypeId='" + carTypeId + '\'' +
                ", charges='" + charges + '\'' +
                ", perKmFare='" + perKmFare + '\'' +
                ", bookingTime=" + bookingTime +
                ", status='" + status + '\'' +
                ", multicityCountryId='" + multicityCountryId + '\'' +
                ", adminSettlementAmount='" + adminSettlementAmount + '\'' +
                ", paymentStatus='" + paymentStatus + '\'' +
                ", rideLaterPickupTimeLogs='" + rideLaterPickupTimeLogs + '\'' +
                ", driverAmountTotal='" + driverAmountTotal + '\'' +
                ", pEmail='" + pEmail + '\'' +
                ", pPhotoUrl='" + pPhotoUrl + '\'' +
                ", sLongitude=" + sLongitude +
                ", dLatitude=" + dLatitude +
                ", discount='" + discount + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", promoCodeId='" + promoCodeId + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", email='" + email + '\'' +
                ", destinationAddress='" + destinationAddress + '\'' +
                ", criticalTourRideLater='" + criticalTourRideLater + '\'' +
                ", destinationGeolocation='" + destinationGeolocation + '\'' +
                ", phoneNoCode='" + phoneNoCode + '\'' +
                ", language='" + language + '\'' +
                ", paymentTriggerById='" + paymentTriggerById + '\'' +
                ", finalAmountCollected='" + finalAmountCollected + '\'' +
                ", pPhoneCode='" + pPhoneCode + '\'' +
                ", tourId='" + tourId + '\'' +
                ", paymentMode='" + paymentMode + '\'' +
                ", carId='" + carId + '\'' +
                ", rideLaterLastNotification='" + rideLaterLastNotification + '\'' +
                ", paymentGatewayId='" + paymentGatewayId + '\'' +
                ", cardOwner='" + cardOwner + '\'' +
                ", bookingFees='" + bookingFees + '\'' +
                ", cardBooking='" + cardBooking + '\'' +
                ", passengerId='" + passengerId + '\'' +
                ", dLongitude=" + dLongitude +
                ", pickupFavouriteLocationsId='" + pickupFavouriteLocationsId + '\'' +
                ", rideLater='" + rideLater + '\'' +
                ", mode='" + mode + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", recordStatus='" + recordStatus + '\'' +
                ", carType='" + carType + '\'' +
                ", distance='" + distance + '\'' +
                ", sLatitude=" + sLatitude +
                ", subTotal='" + subTotal + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", driverId='" + driverId + '\'' +
                ", multicitySurgePricingId='" + multicitySurgePricingId + '\'' +
                ", surgeRate='" + surgeRate + '\'' +
                ", firstName='" + firstName + '\'' +
                ", fine='" + fine + '\'' +
                ", dNote='" + dNote + '\'' +
                ", sourceAddress='" + sourceAddress + '\'' +
                ", promoCode='" + promoCode + '\'' +
                ", destinationFavouriteLocationsId='" + destinationFavouriteLocationsId + '\'' +
                ", freeDistance='" + freeDistance + '\'' +
                ", userTourId='" + userTourId + '\'' +
                ", percentage='" + percentage + '\'' +
                ", paymentType='" + paymentType + '\'' +
                ", tollAmount='" + tollAmount + '\'' +
                ", perMinuteFare='" + perMinuteFare + '\'' +
                ", usedCredits='" + usedCredits + '\'' +
                ", multicityCityRegionId='" + multicityCityRegionId + '\'' +
                ", fixedFare='" + fixedFare + '\'' +
                ", promoCodeApplied='" + promoCodeApplied + '\'' +
                ", promoDiscount='" + promoDiscount + '\'' +
                ", pFirstName='" + pFirstName + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", acknowledged='" + acknowledged + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", driverAmount='" + driverAmount + '\'' +
                ", dRate='" + dRate + '\'' +
                '}';
    }
}
