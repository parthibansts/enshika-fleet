package com.enshika.fleet_management.model;

public class UserModel {

    private String companyAddress;

    private String cardAvailable;

    private String invoiceTotal;

    private String referralCode;

    private String driverCardIncome;

    private String creditCardDetails;

    private String driverBookingFees;

    private String driverCashAmountCollected;

    private String password;

    private String tip;

    private String recordStatus;

    private String updatedBy;

    private String notificationStatus;

    private String timezone;

    private String rate;

    private String driverCashIncome;

    private boolean verified;

    private String createdAt;

    private String userId;

    private String verificationCode;

    private String gender;

    private String badgeCount;

    private String credit;

    private String firstName;

    private String roleId;

    private String lastName;

    private String status;

    private String driverReceivableAmount;

    private String onDuty;

    private String adminSettlementAmount;

    private String driverAmountTotal;

    private String userRole;

    private String companyName;

    private String deleted;

    private String updatedAt;

    private String phoneNo;

    private String firstTime;

    private String createdBy;

    private String apiSessionKey;

    private String email;

    private String active;

    private String fullName;

    private String phoneNoCode;

    private String photoUrl;

    private double current_balance;

    private String wallet_pin;

    public String getWallet_pin() {
        return wallet_pin;
    }

    public void setWallet_pin(String wallet_pin) {
        this.wallet_pin = wallet_pin;
    }

    public double getCurrent_balance() {
        return current_balance;
    }

    public void setCurrent_balance(double current_balance) {
        this.current_balance = current_balance;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCardAvailable() {
        return cardAvailable;
    }

    public void setCardAvailable(String cardAvailable) {
        this.cardAvailable = cardAvailable;
    }

    public String getInvoiceTotal() {
        return invoiceTotal;
    }

    public void setInvoiceTotal(String invoiceTotal) {
        this.invoiceTotal = invoiceTotal;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getDriverCardIncome() {
        return driverCardIncome;
    }

    public void setDriverCardIncome(String driverCardIncome) {
        this.driverCardIncome = driverCardIncome;
    }

    public String getCreditCardDetails() {
        return creditCardDetails;
    }

    public void setCreditCardDetails(String creditCardDetails) {
        this.creditCardDetails = creditCardDetails;
    }

    public String getDriverBookingFees() {
        return driverBookingFees;
    }

    public void setDriverBookingFees(String driverBookingFees) {
        this.driverBookingFees = driverBookingFees;
    }

    public String getDriverCashAmountCollected() {
        return driverCashAmountCollected;
    }

    public void setDriverCashAmountCollected(String driverCashAmountCollected) {
        this.driverCashAmountCollected = driverCashAmountCollected;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(String notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getDriverCashIncome() {
        return driverCashIncome;
    }

    public void setDriverCashIncome(String driverCashIncome) {
        this.driverCashIncome = driverCashIncome;
    }

    public boolean getVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBadgeCount() {
        return badgeCount;
    }

    public void setBadgeCount(String badgeCount) {
        this.badgeCount = badgeCount;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDriverReceivableAmount() {
        return driverReceivableAmount;
    }

    public void setDriverReceivableAmount(String driverReceivableAmount) {
        this.driverReceivableAmount = driverReceivableAmount;
    }

    public String getOnDuty() {
        return onDuty;
    }

    public void setOnDuty(String onDuty) {
        this.onDuty = onDuty;
    }

    public String getAdminSettlementAmount() {
        return adminSettlementAmount;
    }

    public void setAdminSettlementAmount(String adminSettlementAmount) {
        this.adminSettlementAmount = adminSettlementAmount;
    }

    public String getDriverAmountTotal() {
        return driverAmountTotal;
    }

    public void setDriverAmountTotal(String driverAmountTotal) {
        this.driverAmountTotal = driverAmountTotal;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getFirstTime() {
        return firstTime;
    }

    public void setFirstTime(String firstTime) {
        this.firstTime = firstTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getApiSessionKey() {
        return apiSessionKey;
    }

    public void setApiSessionKey(String apiSessionKey) {
        this.apiSessionKey = apiSessionKey;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNoCode() {
        return phoneNoCode;
    }

    public void setPhoneNoCode(String phoneNoCode) {
        this.phoneNoCode = phoneNoCode;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    @Override
    public String toString() {
        return "ClassPojo [companyAddress = " + companyAddress + ", cardAvailable = " + cardAvailable + ", invoiceTotal = " + invoiceTotal + ", referralCode = " + referralCode + ", driverCardIncome = " + driverCardIncome + ", creditCardDetails = " + creditCardDetails + ", driverBookingFees = " + driverBookingFees + ", driverCashAmountCollected = " + driverCashAmountCollected + ", password = " + password + ", tip = " + tip + ", recordStatus = " + recordStatus + ", updatedBy = " + updatedBy + ", notificationStatus = " + notificationStatus + ", timezone = " + timezone + ", rate = " + rate + ", driverCashIncome = " + driverCashIncome + ", verified = " + verified + ", createdAt = " + createdAt + ", userId = " + userId + ", verificationCode = " + verificationCode + ", gender = " + gender + ", badgeCount = " + badgeCount + ", credit = " + credit + ", firstName = " + firstName + ", roleId = " + roleId + ", lastName = " + lastName + ", status = " + status + ", driverReceivableAmount = " + driverReceivableAmount + ", onDuty = " + onDuty + ", adminSettlementAmount = " + adminSettlementAmount + ", driverAmountTotal = " + driverAmountTotal + ", userRole = " + userRole + ", companyName = " + companyName + ", deleted = " + deleted + ", updatedAt = " + updatedAt + ", phoneNo = " + phoneNo + ", firstTime = " + firstTime + ", createdBy = " + createdBy + ", apiSessionKey = " + apiSessionKey + ", email = " + email + ", active = " + active + ", fullName = " + fullName + ", phoneNoCode = " + phoneNoCode + ", photoUrl = " + photoUrl + "]";
    }
}
