package com.enshika.fleet_management.model;

/**
 * Created by Shamla Tech on 02-08-2018.
 */

public class CommonResponseModel {
    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
