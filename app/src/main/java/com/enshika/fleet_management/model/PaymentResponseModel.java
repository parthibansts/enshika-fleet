package com.enshika.fleet_management.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 21-07-2018.
 */

public class PaymentResponseModel implements Serializable {
    private String message;

    private int status;

    private DataModel data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DataModel getData() {
        return data;
    }

    public void setData(DataModel data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", status = " + status + ", data = " + data + "]";
    }
}
