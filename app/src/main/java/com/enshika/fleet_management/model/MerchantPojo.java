package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 24-07-2018.
 */

public class MerchantPojo implements Serializable {
    private String passCode = "";

    private String userId = "";

    private String merchantId = "";

    private String minBalance = "";

    private String transactionId = "";

    private String walletBalance = "";

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(String walletBalance) {
        this.walletBalance = walletBalance;
    }

    public String getPassCode() {
        return passCode;
    }

    public void setPassCode(String passCode) {
        this.passCode = passCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMinBalance() {
        return minBalance;
    }

    public void setMinBalance(String minBalance) {
        this.minBalance = minBalance;
    }

    @Override
    public String toString() {
        return "ClassPojo [passCode = " + passCode + ", userId = " + userId + ", merchantId = " + merchantId + ", minBalance = " + minBalance + "]";
    }
}
