package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 17-04-2018.
 */

public class RegionPojo implements Serializable {
    String countryId,countryName;

    public String getId() {
        return countryId;
    }

    public void setId(String id) {
        this.countryId = id;
    }

    public String getName() {
        return countryName;
    }

    public void setName(String name) {
        this.countryName = name;
    }
}
