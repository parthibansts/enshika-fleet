package com.enshika.fleet_management.model;

/**
 * Created by Shamla Tech on 07-07-2018.
 */

public class RegisterCorporateSubCategoryModel {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return  name;
    }
}
