package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 29-06-2018.
 */

public class DrivingLicenseModel implements Serializable {
    private long licenceExpirationDate;

    private String socilaSecurityPhotoUrl;

    private String drivingLicenseBackPhotoUrl;

    private String insurancePhotoUrl;

    private long insuranceExpirationDate;

    private String socialSecurityNumber;

    private String auBusinessNo;

    private String lName;

    private String driverLicenseCardNumber;

    private String updatedBy;

    private String recordStatus;

    private String fName;

    private String criminalHistoryPhotoUrl;

    private String updatedAt;

    private long insuranceEffectiveDate;

    private String drivingLicenseInfoId;

    private String createdBy;

    private String drivingLicensePhotoUrl;

    private String createdAt;

    private String userId;

    private long dob;

    private String birthAccreditationPassportPhotoUrl;

    private String mName;

    public long getLicenceExpirationDate ()
    {
        return licenceExpirationDate;
    }

    public void setLicenceExpirationDate (long licenceExpirationDate)
    {
        this.licenceExpirationDate = licenceExpirationDate;
    }

    public String getSocilaSecurityPhotoUrl ()
    {
        return socilaSecurityPhotoUrl;
    }

    public void setSocilaSecurityPhotoUrl (String socilaSecurityPhotoUrl)
    {
        this.socilaSecurityPhotoUrl = socilaSecurityPhotoUrl;
    }

    public String getDrivingLicenseBackPhotoUrl ()
    {
        return drivingLicenseBackPhotoUrl;
    }

    public void setDrivingLicenseBackPhotoUrl (String drivingLicenseBackPhotoUrl)
    {
        this.drivingLicenseBackPhotoUrl = drivingLicenseBackPhotoUrl;
    }

    public String getInsurancePhotoUrl ()
    {
        return insurancePhotoUrl;
    }

    public void setInsurancePhotoUrl (String insurancePhotoUrl)
    {
        this.insurancePhotoUrl = insurancePhotoUrl;
    }

    public long getInsuranceExpirationDate ()
    {
        return insuranceExpirationDate;
    }

    public void setInsuranceExpirationDate (long insuranceExpirationDate)
    {
        this.insuranceExpirationDate = insuranceExpirationDate;
    }

    public String getSocialSecurityNumber ()
    {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber (String socialSecurityNumber)
    {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getAuBusinessNo ()
    {
        return auBusinessNo;
    }

    public void setAuBusinessNo (String auBusinessNo)
    {
        this.auBusinessNo = auBusinessNo;
    }

    public String getLName ()
    {
        return lName;
    }

    public void setLName (String lName)
    {
        this.lName = lName;
    }

    public String getDriverLicenseCardNumber ()
    {
        return driverLicenseCardNumber;
    }

    public void setDriverLicenseCardNumber (String driverLicenseCardNumber)
    {
        this.driverLicenseCardNumber = driverLicenseCardNumber;
    }

    public String getUpdatedBy ()
    {
        return updatedBy;
    }

    public void setUpdatedBy (String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getRecordStatus ()
    {
        return recordStatus;
    }

    public void setRecordStatus (String recordStatus)
    {
        this.recordStatus = recordStatus;
    }

    public String getFName ()
    {
        return fName;
    }

    public void setFName (String fName)
    {
        this.fName = fName;
    }

    public String getCriminalHistoryPhotoUrl ()
    {
        return criminalHistoryPhotoUrl;
    }

    public void setCriminalHistoryPhotoUrl (String criminalHistoryPhotoUrl)
    {
        this.criminalHistoryPhotoUrl = criminalHistoryPhotoUrl;
    }

    public String getUpdatedAt ()
    {
        return updatedAt;
    }

    public void setUpdatedAt (String updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public long getInsuranceEffectiveDate ()
    {
        return insuranceEffectiveDate;
    }

    public void setInsuranceEffectiveDate (long insuranceEffectiveDate)
    {
        this.insuranceEffectiveDate = insuranceEffectiveDate;
    }

    public String getDrivingLicenseInfoId ()
    {
        return drivingLicenseInfoId;
    }

    public void setDrivingLicenseInfoId (String drivingLicenseInfoId)
    {
        this.drivingLicenseInfoId = drivingLicenseInfoId;
    }

    public String getCreatedBy ()
    {
        return createdBy;
    }

    public void setCreatedBy (String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getDrivingLicensePhotoUrl ()
    {
        return drivingLicensePhotoUrl;
    }

    public void setDrivingLicensePhotoUrl (String drivingLicensePhotoUrl)
    {
        this.drivingLicensePhotoUrl = drivingLicensePhotoUrl;
    }

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public void setCreatedAt (String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String getUserId ()
    {
        return userId;
    }

    public void setUserId (String userId)
    {
        this.userId = userId;
    }

    public long getDob ()
    {
        return dob;
    }

    public void setDob (long dob)
    {
        this.dob = dob;
    }

    public String getBirthAccreditationPassportPhotoUrl ()
    {
        return birthAccreditationPassportPhotoUrl;
    }

    public void setBirthAccreditationPassportPhotoUrl (String birthAccreditationPassportPhotoUrl)
    {
        this.birthAccreditationPassportPhotoUrl = birthAccreditationPassportPhotoUrl;
    }

    public String getMName ()
    {
        return mName;
    }

    public void setMName (String mName)
    {
        this.mName = mName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [licenceExpirationDate = "+licenceExpirationDate+", socilaSecurityPhotoUrl = "+socilaSecurityPhotoUrl+", drivingLicenseBackPhotoUrl = "+drivingLicenseBackPhotoUrl+", insurancePhotoUrl = "+insurancePhotoUrl+", insuranceExpirationDate = "+insuranceExpirationDate+", socialSecurityNumber = "+socialSecurityNumber+", auBusinessNo = "+auBusinessNo+", lName = "+lName+", driverLicenseCardNumber = "+driverLicenseCardNumber+", updatedBy = "+updatedBy+", recordStatus = "+recordStatus+", fName = "+fName+", criminalHistoryPhotoUrl = "+criminalHistoryPhotoUrl+", updatedAt = "+updatedAt+", insuranceEffectiveDate = "+insuranceEffectiveDate+", drivingLicenseInfoId = "+drivingLicenseInfoId+", createdBy = "+createdBy+", drivingLicensePhotoUrl = "+drivingLicensePhotoUrl+", createdAt = "+createdAt+", userId = "+userId+", dob = "+dob+", birthAccreditationPassportPhotoUrl = "+birthAccreditationPassportPhotoUrl+", mName = "+mName+"]";
    }
}
