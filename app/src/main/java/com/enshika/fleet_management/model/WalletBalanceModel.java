package com.enshika.fleet_management.model;

/**
 * Created by Shamla Tech on 18-07-2018.
 */

public class WalletBalanceModel {
    private double walletBalance;

    public double getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(double walletBalance) {
        this.walletBalance = walletBalance;
    }
}
