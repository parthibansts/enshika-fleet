package com.enshika.fleet_management.model;

/**
 * Created by Shamla Tech on 21-07-2018.
 */

public class WalletSetPinPojo {
    private String userId;
    private String walletPin;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWalletPin() {
        return walletPin;
    }

    public void setWalletPin(String walletPin) {
        this.walletPin = walletPin;
    }
}
