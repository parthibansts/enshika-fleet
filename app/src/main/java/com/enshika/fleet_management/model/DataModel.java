package com.enshika.fleet_management.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Shamla Tech on 21-07-2018.
 */

public class DataModel implements Serializable {
    private ArrayList<PaymentOptionPojo> paymentmodes = new ArrayList<>();

    private ArrayList<MobileMoneyOperators> mobileMoneyOperators = new ArrayList<>();

    private ArrayList<MobileMoneyOperators> mobileOperator = new ArrayList<>();

    public ArrayList<MobileMoneyOperators> getMobileOperator() {
        return mobileOperator;
    }

    public void setMobileOperator(ArrayList<MobileMoneyOperators> mobileOperator) {
        this.mobileOperator = mobileOperator;
    }

    public ArrayList<MobileMoneyOperators> getMobileMoneyOperators() {
        return mobileMoneyOperators;
    }

    public void setMobileMoneyOperators(ArrayList<MobileMoneyOperators> mobileMoneyOperators) {
        this.mobileMoneyOperators = mobileMoneyOperators;
    }

    public ArrayList<PaymentOptionPojo> getPaymentmodes() {
        return paymentmodes;
    }

    public void setPaymentmodes(ArrayList<PaymentOptionPojo> paymentmodes) {
        this.paymentmodes = paymentmodes;
    }

    @Override
    public String toString() {
        return "ClassPojo [paymentmodes = " + paymentmodes + "]";
    }
}
