package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 29-06-2018.
 */

public class DriverBankDetailsModel implements Serializable {
    private String updatedAt;

    private String accountName;

    private String accountNumber;

    private String createdBy;

    private String createdAt;

    private String userId;

    private String driverBankDetailsId;

    private String bankName;

    private String type;

    private String routingNumber;

    private String recordStatus;

    private String updatedBy;

    public String getUpdatedAt ()
    {
        return updatedAt;
    }

    public void setUpdatedAt (String updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public String getAccountName ()
    {
        return accountName;
    }

    public void setAccountName (String accountName)
    {
        this.accountName = accountName;
    }

    public String getAccountNumber ()
    {
        return accountNumber;
    }

    public void setAccountNumber (String accountNumber)
    {
        this.accountNumber = accountNumber;
    }

    public String getCreatedBy ()
    {
        return createdBy;
    }

    public void setCreatedBy (String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public void setCreatedAt (String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String getUserId ()
    {
        return userId;
    }

    public void setUserId (String userId)
    {
        this.userId = userId;
    }

    public String getDriverBankDetailsId ()
    {
        return driverBankDetailsId;
    }

    public void setDriverBankDetailsId (String driverBankDetailsId)
    {
        this.driverBankDetailsId = driverBankDetailsId;
    }

    public String getBankName ()
    {
        return bankName;
    }

    public void setBankName (String bankName)
    {
        this.bankName = bankName;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getRoutingNumber ()
    {
        return routingNumber;
    }

    public void setRoutingNumber (String routingNumber)
    {
        this.routingNumber = routingNumber;
    }

    public String getRecordStatus ()
    {
        return recordStatus;
    }

    public void setRecordStatus (String recordStatus)
    {
        this.recordStatus = recordStatus;
    }

    public String getUpdatedBy ()
    {
        return updatedBy;
    }

    public void setUpdatedBy (String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [updatedAt = "+updatedAt+", accountName = "+accountName+", accountNumber = "+accountNumber+", createdBy = "+createdBy+", createdAt = "+createdAt+", userId = "+userId+", driverBankDetailsId = "+driverBankDetailsId+", bankName = "+bankName+", type = "+type+", routingNumber = "+routingNumber+", recordStatus = "+recordStatus+", updatedBy = "+updatedBy+"]";
    }
}
