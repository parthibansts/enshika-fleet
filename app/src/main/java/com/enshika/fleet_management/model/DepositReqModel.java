package com.enshika.fleet_management.model;

/**
 * Created by Shamla Tech on 25-07-2018.
 */

public class DepositReqModel {
    private String userId;
    private String amount;
    private String comments;

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
