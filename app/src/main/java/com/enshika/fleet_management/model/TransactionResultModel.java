package com.enshika.fleet_management.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 25-07-2018.
 */

public class TransactionResultModel implements Serializable {
    private String amount;
    private String desc;
    private String merchant_id;
    @SerializedName("r-switch")
    private String operator;
    private String processing_code;
    private String subscriber_number;
    private String transaction_id;
    private String status;
    private int code;
    private String reason;
    private String description;
    @SerializedName("3d_url_response")
    private String url_response;

    public String getUrl_response() {
        return url_response;
    }

    public void setUrl_response(String url_response) {
        this.url_response = url_response;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getProcessing_code() {
        return processing_code;
    }

    public void setProcessing_code(String processing_code) {
        this.processing_code = processing_code;
    }

    public String getSubscriber_number() {
        return subscriber_number;
    }

    public void setSubscriber_number(String subscriber_number) {
        this.subscriber_number = subscriber_number;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
