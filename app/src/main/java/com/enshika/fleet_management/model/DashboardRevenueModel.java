package com.enshika.fleet_management.model;

/**
 * Created by Shamla Tech on 17-08-2018.
 */

public class DashboardRevenueModel {
    private long date;
    private float amount;
    private float day_of_year;

    public float getDay_of_year() {
        return day_of_year;
    }

    public void setDay_of_year(float day_of_year) {
        this.day_of_year = day_of_year;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
