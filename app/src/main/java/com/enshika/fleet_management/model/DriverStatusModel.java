package com.enshika.fleet_management.model;

/**
 * Created by Shamla Tech on 30-06-2018.
 */

public class DriverStatusModel {
    private long driverOnline;
    private long driverOffline;

    public long getDriverOnline() {
        return driverOnline;
    }

    public void setDriverOnline(long driverOnline) {
        this.driverOnline = driverOnline;
    }

    public long getDriverOffline() {
        return driverOffline;
    }

    public void setDriverOffline(long driverOffline) {
        this.driverOffline = driverOffline;
    }
}
