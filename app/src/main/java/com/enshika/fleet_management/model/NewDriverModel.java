package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 02-07-2018.
 */

public class NewDriverModel implements Serializable {
    private String car;

    private String userId;

    private String region;

    private String mailAddressLineOne;

    private long insuranceExpirationDate;

    private String type;

    private String mailAddressLineTwo;

    private long insuranceEffectiveDate;

    private String companyDriver;

    private String accountNumber;

    private String mailStateId;

    private String gender;

    private String firstName;

    private String lastName;

    private String socialSecurityNumber;

    private long licenseExpiration;

    private String bankName;

    private String routingNumber;

    private String phoneNo;

    private String accountName;

    private String mailCityId;

    private String email;

    private String mailCountryId;

    private long dob;

    private String phoneNoCode;

    private String drivingLicense;

    public String getCar ()
    {
        return car;
    }

    public void setCar (String car)
    {
        this.car = car;
    }

    public String getRegion ()
    {
        return region;
    }

    public void setRegion (String region)
    {
        this.region = region;
    }

    public String getMailAddressLineOne ()
    {
        return mailAddressLineOne;
    }

    public void setMailAddressLineOne (String mailAddressLineOne)
    {
        this.mailAddressLineOne = mailAddressLineOne;
    }

    public long getInsuranceExpirationDate ()
    {
        return insuranceExpirationDate;
    }

    public void setInsuranceExpirationDate (long insuranceExpirationDate)
    {
        this.insuranceExpirationDate = insuranceExpirationDate;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getMailAddressLineTwo ()
    {
        return mailAddressLineTwo;
    }

    public void setMailAddressLineTwo (String mailAddressLineTwo)
    {
        this.mailAddressLineTwo = mailAddressLineTwo;
    }

    public long getInsuranceEffectiveDate ()
    {
        return insuranceEffectiveDate;
    }

    public void setInsuranceEffectiveDate (long insuranceEffectiveDate)
    {
        this.insuranceEffectiveDate = insuranceEffectiveDate;
    }

    public String getCompanyDriver ()
    {
        return companyDriver;
    }

    public void setCompanyDriver (String companyDriver)
    {
        this.companyDriver = companyDriver;
    }

    public String getAccountNumber ()
    {
        return accountNumber;
    }

    public void setAccountNumber (String accountNumber)
    {
        this.accountNumber = accountNumber;
    }

    public String getMailStateId ()
    {
        return mailStateId;
    }

    public void setMailStateId (String mailStateId)
    {
        this.mailStateId = mailStateId;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    public String getSocialSecurityNumber ()
    {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber (String socialSecurityNumber)
    {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public long getLicenseExpiration ()
    {
        return licenseExpiration;
    }

    public void setLicenseExpiration (long licenseExpiration)
    {
        this.licenseExpiration = licenseExpiration;
    }

    public String getBankName ()
    {
        return bankName;
    }

    public void setBankName (String bankName)
    {
        this.bankName = bankName;
    }

    public String getRoutingNumber ()
    {
        return routingNumber;
    }

    public void setRoutingNumber (String routingNumber)
    {
        this.routingNumber = routingNumber;
    }

    public String getPhoneNo ()
    {
        return phoneNo;
    }

    public void setPhoneNo (String phoneNo)
    {
        this.phoneNo = phoneNo;
    }

    public String getAccountName ()
    {
        return accountName;
    }

    public void setAccountName (String accountName)
    {
        this.accountName = accountName;
    }

    public String getMailCityId ()
    {
        return mailCityId;
    }

    public void setMailCityId (String mailCityId)
    {
        this.mailCityId = mailCityId;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getMailCountryId ()
    {
        return mailCountryId;
    }

    public void setMailCountryId (String mailCountryId)
    {
        this.mailCountryId = mailCountryId;
    }

    public long getDob ()
    {
        return dob;
    }

    public void setDob (long dob)
    {
        this.dob = dob;
    }

    public String getPhoneNoCode ()
    {
        return phoneNoCode;
    }

    public void setPhoneNoCode (String phoneNoCode)
    {
        this.phoneNoCode = phoneNoCode;
    }

    public String getDrivingLicense ()
    {
        return drivingLicense;
    }

    public void setDrivingLicense (String drivingLicense)
    {
        this.drivingLicense = drivingLicense;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "NewDriverModel{" +
                "car='" + car + '\'' +
                ", userId='" + userId + '\'' +
                ", region='" + region + '\'' +
                ", mailAddressLineOne='" + mailAddressLineOne + '\'' +
                ", insuranceExpirationDate='" + insuranceExpirationDate + '\'' +
                ", type='" + type + '\'' +
                ", mailAddressLineTwo='" + mailAddressLineTwo + '\'' +
                ", insuranceEffectiveDate='" + insuranceEffectiveDate + '\'' +
                ", companyDriver='" + companyDriver + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", mailStateId='" + mailStateId + '\'' +
                ", gender='" + gender + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", socialSecurityNumber='" + socialSecurityNumber + '\'' +
                ", licenseExpiration='" + licenseExpiration + '\'' +
                ", bankName='" + bankName + '\'' +
                ", routingNumber='" + routingNumber + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", accountName='" + accountName + '\'' +
                ", mailCityId='" + mailCityId + '\'' +
                ", email='" + email + '\'' +
                ", mailCountryId='" + mailCountryId + '\'' +
                ", dob='" + dob + '\'' +
                ", phoneNoCode='" + phoneNoCode + '\'' +
                ", drivingLicense='" + drivingLicense + '\'' +
                '}';
    }
}
