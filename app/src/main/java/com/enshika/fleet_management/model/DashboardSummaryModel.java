package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 04-07-2018.
 */

public class DashboardSummaryModel implements Serializable {
    private String name;
    private String value;
    private String imageUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
