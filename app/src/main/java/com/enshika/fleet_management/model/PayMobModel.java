package com.enshika.fleet_management.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Shamla Tech on 25-07-2018.
 */

public class PayMobModel {
    @SerializedName("r-switch")
    private String operator;
    private String amount;
    private String processing_code;
    private String transaction_id;
    private String desc;
    private String merchant_id;
    private String subscriber_number;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getProcessing_code() {
        return processing_code;
    }

    public void setProcessing_code(String processing_code) {
        this.processing_code = processing_code;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getSubscriber_number() {
        return subscriber_number;
    }

    public void setSubscriber_number(String subscriber_number) {
        this.subscriber_number = subscriber_number;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}
