package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 03-07-2018.
 */

public class AppSettingsModel implements Serializable {
    private String currencySymbolHtml;

    private String distanceUnits;

    private String currencySymbol;

    private String distanceType;

    private String paymentGatewayId;

    private String countryCode;

    private String googleApiServerKey;

    private String androidKey;

    private String paymentTriggerById;

    public String getCurrencySymbolHtml ()
    {
        return currencySymbolHtml;
    }

    public void setCurrencySymbolHtml (String currencySymbolHtml)
    {
        this.currencySymbolHtml = currencySymbolHtml;
    }

    public String getDistanceUnits ()
    {
        return distanceUnits;
    }

    public void setDistanceUnits (String distanceUnits)
    {
        this.distanceUnits = distanceUnits;
    }

    public String getCurrencySymbol ()
    {
        return currencySymbol;
    }

    public void setCurrencySymbol (String currencySymbol)
    {
        this.currencySymbol = currencySymbol;
    }

    public String getDistanceType ()
    {
        return distanceType;
    }

    public void setDistanceType (String distanceType)
    {
        this.distanceType = distanceType;
    }

    public String getPaymentGatewayId ()
    {
        return paymentGatewayId;
    }

    public void setPaymentGatewayId (String paymentGatewayId)
    {
        this.paymentGatewayId = paymentGatewayId;
    }

    public String getCountryCode ()
    {
        return countryCode;
    }

    public void setCountryCode (String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String getGoogleApiServerKey ()
    {
        return googleApiServerKey;
    }

    public void setGoogleApiServerKey (String googleApiServerKey)
    {
        this.googleApiServerKey = googleApiServerKey;
    }

    public String getAndroidKey ()
    {
        return androidKey;
    }

    public void setAndroidKey (String androidKey)
    {
        this.androidKey = androidKey;
    }

    public String getPaymentTriggerById ()
    {
        return paymentTriggerById;
    }

    public void setPaymentTriggerById (String paymentTriggerById)
    {
        this.paymentTriggerById = paymentTriggerById;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [currencySymbolHtml = "+currencySymbolHtml+", distanceUnits = "+distanceUnits+", currencySymbol = "+currencySymbol+", distanceType = "+distanceType+", paymentGatewayId = "+paymentGatewayId+", countryCode = "+countryCode+", googleApiServerKey = "+googleApiServerKey+", androidKey = "+androidKey+", paymentTriggerById = "+paymentTriggerById+"]";
    }
}
