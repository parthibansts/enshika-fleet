package com.enshika.fleet_management.model;

/**
 * Created by Shamla Tech on 21-07-2018.
 */

public class StatusMessageModel {
    private String message;
    private String status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
