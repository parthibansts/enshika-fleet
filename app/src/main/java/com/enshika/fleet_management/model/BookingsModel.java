package com.enshika.fleet_management.model;

import java.io.Serializable;

/**
 * Created by Shamla Tech on 30-06-2018.
 */

public class BookingsModel implements Serializable {
    private String carType;

    private String total;

    private String distance;

    private long bookingTime;

    private String status;

    private String passengerName;

    private String bookingType;

    private String driverName;

    private String tourId;

    public String getCarType ()
    {
        return carType;
    }

    public void setCarType (String carType)
    {
        this.carType = carType;
    }

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

    public String getDistance ()
    {
        return distance;
    }

    public void setDistance (String distance)
    {
        this.distance = distance;
    }

    public long getBookingTime ()
    {
        return bookingTime;
    }

    public void setBookingTime (long bookingTime)
    {
        this.bookingTime = bookingTime;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getPassengerName ()
    {
        return passengerName;
    }

    public void setPassengerName (String passengerName)
    {
        this.passengerName = passengerName;
    }

    public String getBookingType ()
    {
        return bookingType;
    }

    public void setBookingType (String bookingType)
    {
        this.bookingType = bookingType;
    }

    public String getDriverName ()
    {
        return driverName;
    }

    public void setDriverName (String driverName)
    {
        this.driverName = driverName;
    }

    public String getTourId ()
    {
        return tourId;
    }

    public void setTourId (String tourId)
    {
        this.tourId = tourId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [carType = "+carType+", total = "+total+", distance = "+distance+", bookingTime = "+bookingTime+", status = "+status+", passengerName = "+passengerName+", bookingType = "+bookingType+", driverName = "+driverName+", tourId = "+tourId+"]";
    }
}
