package com.enshika.fleet_management.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.StatusMessageModel;
import com.enshika.fleet_management.model.WalletSetPinPojo;
import com.enshika.fleet_management.model.WalletUpdatePinPojo;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;

/**
 * Created by Shamla Tech on 06-07-2018.
 */

public class ManagePinActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    TextView txtUpdate;
    EditText edtOldPin, edtNewPin, edtConfirmPin;
    ResponseParser responseParser;
    CircleImageView ciFleet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_pin);
        initUI();
        prepareScreen();
    }

    private void prepareScreen() {
        if (Support.getPref(getApplicationContext(), Vars.PIN).isEmpty()) {
            edtOldPin.setVisibility(View.GONE);
        } else
            edtOldPin.setVisibility(View.VISIBLE);
        Support.loadImage(Support.getPref(ManagePinActivity.this, Vars.PHOTO_URL), ciFleet,
                R.drawable.ic_user_placeholder_white);
    }

    private void initUI() {
        ciFleet = findViewById(R.id.ciFleet);
        edtOldPin = findViewById(R.id.edtOldPin);
        edtNewPin = findViewById(R.id.edtNewPin);
        edtConfirmPin = findViewById(R.id.edtConfirmPin);
        txtUpdate = findViewById(R.id.txtUpdate);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Manage Pin");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        responseParser = new ResponseParser(this);
        txtUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtUpdate:
                if (isValidate()) {
                    if (Support.getPref(getApplicationContext(), Vars.PIN).isEmpty())
                        setPin(edtNewPin.getText().toString());
                    else
                        updatePin(edtOldPin.getText().toString(), edtNewPin.getText().toString());
                }
                break;
        }
    }

    private boolean isValidate() {
        boolean isValid = true;
        if (Support.getPref(getApplicationContext(), Vars.PIN).isEmpty()) {
            if (edtNewPin.getText().toString().isEmpty()) {
                edtNewPin.setError("please enter new pin");
                isValid = false;
            }
            if (edtConfirmPin.getText().toString().isEmpty()) {
                edtConfirmPin.setError("please enter new pin");
                isValid = false;
            }
            if (!edtNewPin.getText().toString().isEmpty() &&
                    !edtConfirmPin.getText().toString().isEmpty() &&
                    !edtNewPin.getText().toString().equals(edtConfirmPin.getText().toString())) {
                Toast.makeText(this, "new pin's are mismatched!", Toast.LENGTH_SHORT).show();
                isValid = false;
            }
        } else {
            if (edtOldPin.getText().toString().isEmpty()) {
                edtOldPin.setError("please enter old pin");
                isValid = false;
            }
            if (edtNewPin.getText().toString().isEmpty()) {
                edtNewPin.setError("please enter new pin");
                isValid = false;
            }
            if (edtConfirmPin.getText().toString().isEmpty()) {
                edtConfirmPin.setError("please retype new pin");
                isValid = false;
            }
            if (!edtNewPin.getText().toString().isEmpty() &&
                    !edtConfirmPin.getText().toString().isEmpty() &&
                    !edtNewPin.getText().toString().equals(edtConfirmPin.getText().toString())) {
                Toast.makeText(this, "new pin's are mismatched!", Toast.LENGTH_SHORT).show();
                isValid = false;
            }
        }
        return isValid;
    }

    private void setPin(String pin) {
        WalletSetPinPojo walletPinPojo = new WalletSetPinPojo();
        walletPinPojo.setUserId(Support.getPref(getApplicationContext(), Vars.USER_ID));
        walletPinPojo.setWalletPin(pin);
        ApiCall.getApiCall(ManagePinActivity.this, apiService.setPin(Support.getHeader(getApplicationContext()), walletPinPojo),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        StatusMessageModel model = responseParser.getStatusMessage(objectResponse.body());
                        if (model.getStatus().equals("1")) {
                            Support.setPref(getApplicationContext(), Vars.PIN, edtNewPin.getText().toString());
                            Toast.makeText(ManagePinActivity.this, model.getMessage(), Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(ManagePinActivity.this, model.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(ManagePinActivity.this, message);
                    }
                });
    }

    private void updatePin(String oldPin, String newPin) {
        WalletUpdatePinPojo walletPinPojo = new WalletUpdatePinPojo();
        walletPinPojo.setUserId(Support.getPref(getApplicationContext(), Vars.USER_ID));
        walletPinPojo.setWalletPin(oldPin);
        walletPinPojo.setNewWalletPin(newPin);
        ApiCall.getApiCall(ManagePinActivity.this, apiService.updatePin(Support.getHeader(getApplicationContext()), walletPinPojo),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        StatusMessageModel model = responseParser.getStatusMessage(objectResponse.body());
                        if (model.getStatus().equals("1")) {
                            Support.setPref(getApplicationContext(), Vars.PIN, edtNewPin.getText().toString());
                            Toast.makeText(ManagePinActivity.this, model.getMessage(), Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(ManagePinActivity.this, model.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(ManagePinActivity.this, message);
                    }
                });
    }
}
