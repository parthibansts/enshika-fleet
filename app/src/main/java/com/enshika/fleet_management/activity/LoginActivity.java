package com.enshika.fleet_management.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.AppSettingsModel;
import com.enshika.fleet_management.model.UserModel;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.hbb20.CountryCodePicker;

import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.Support.displayToastMessage;

/**
 * Created by Vino on 20-03-2018.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText et_email_mobile, et_password;
    CountryCodePicker ccp_country_id;
    TextView txt_forgot_password;
    Button btn_login;
    ResponseParser responseParser;
    String userName = "", password = "", phoneCode = "";
    UserModel userModel;
    Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initUI();
        setListeners();
        getBundleDetails();

    }

    private void getBundleDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("exit") && bundle.getBoolean("exit"))
                finish();
            if (bundle.containsKey("reqPhoneVerification") && bundle.getBoolean("reqPhoneVerification")) {
                if (!Support.getPref(getApplicationContext(), Vars.USER_EMAIL).isEmpty()) {
                    et_email_mobile.setText(Support.getPref(getApplicationContext(), Vars.USER_EMAIL));
                } else if (!Support.getPref(getApplicationContext(), Vars.USER_MOB).isEmpty()) {
                    et_email_mobile.setText(Support.getPref(getApplicationContext(), Vars.USER_MOB));
                    ccp_country_id.setCountryForPhoneCode(Integer.parseInt(Support.getPref(getApplicationContext(), Vars.USER_MOB_CODE)));
                }
                et_password.setText(Support.getPref(getApplicationContext(), Vars.USER_PASSWORD));
                checkPhoneVerified();
            }
        }
    }

    private void initUI() {
        et_email_mobile = findViewById(R.id.et_email_mobile);
        ccp_country_id = findViewById(R.id.ccp_country_id);
        et_password = findViewById(R.id.et_password);
        txt_forgot_password = findViewById(R.id.txt_forgot_password);
        btn_login = findViewById(R.id.btn_login);

        responseParser = new ResponseParser(LoginActivity.this);
    }

    private void setListeners() {
        btn_login.setOnClickListener(this);
        et_email_mobile.addTextChangedListener(new EmailTextWatcher());
        txt_forgot_password.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                userName = et_email_mobile.getText().toString().trim();
                password = et_password.getText().toString();
                phoneCode = ccp_country_id.getSelectedCountryCodeAsInt() + "";

                if (isValidFields()) {
                    UserModel model = new UserModel();
                    model.setEmail(userName);
                    model.setPassword(password);
                    model.setRoleId(Vars.USER_ROLE);

                    if (Support.isPhoneNumber(userName))
                        model.setPhoneNoCode(phoneCode);
                    else
                        model.setPhoneNoCode("");

                    ApiCall.getApiCall(LoginActivity.this, apiService.makeLogin(model), true,
                            new ApiCall.OnApiResult() {
                                @Override
                                public void onSuccess(Response<Object> objectResponse) {
                                    try {
                                        if (!((LinkedTreeMap) objectResponse.body()).containsKey("type")) {
                                            userModel = responseParser.getUserModel(objectResponse.body());
                                            if (userModel == null) {
                                                return;
                                            }

                                            if (Support.isPhoneNumber(userName)) {
                                                Support.setPref(getApplicationContext(), Vars.USER_EMAIL, "");
                                                Support.setPref(getApplicationContext(), Vars.USER_MOB, userName);
                                                Support.setPref(getApplicationContext(), Vars.USER_MOB_CODE, phoneCode);
                                            } else {
                                                Support.setPref(getApplicationContext(), Vars.USER_EMAIL, userName);
                                                Support.setPref(getApplicationContext(), Vars.USER_PASSWORD, password);
                                            }

                                            Support.setPref(getApplicationContext(), Vars.FIRST_NAME, userModel.getFirstName());
                                            Support.setPref(getApplicationContext(), Vars.LAST_NAME, userModel.getLastName());
                                            Support.setPref(getApplicationContext(), Vars.EMAIL, userModel.getEmail());
                                            Support.setPref(getApplicationContext(), Vars.PHONE_NO, userModel.getPhoneNo());
                                            Support.setPref(getApplicationContext(), Vars.PHONE_CODE, userModel.getPhoneNoCode());
                                            Support.setPref(getApplicationContext(), Vars.PASSWORD, password);
                                            Support.setPref(getApplicationContext(), Vars.PHOTO_URL, userModel.getPhotoUrl());
                                            Support.setPref(getApplicationContext(), Vars.USER_ID, userModel.getUserId());
                                            Support.setPref(getApplicationContext(), Vars.PHONE_VERIFIED, userModel.getVerified());
//                                            if (userModel.getWallet_pin() != null)
//                                                Support.setPref(getApplicationContext(), Vars.PIN, userModel.getWallet_pin());
                                            Support.setSessionKey(getApplicationContext(), userModel.getApiSessionKey());
                                            ApiCall.getApiCall(LoginActivity.this, apiService.getAppSettings(), true,
                                                    new ApiCall.OnApiResult() {
                                                        @Override
                                                        public void onSuccess(Response<Object> objectResponse) {
                                                            AppSettingsModel appSettingsModel = responseParser.getAppSettingsModel(objectResponse.body());
                                                            Support.setCurrencySymbol(getApplicationContext(),
                                                                    appSettingsModel.getCurrencySymbol());
                                                            Support.setPref(getApplicationContext(), Vars.DISTANCE_TYPE, appSettingsModel.getDistanceType());
                                                            Support.setPref(getApplicationContext(), Vars.DISTANCE_UNITS, appSettingsModel.getDistanceUnits());
                                                            checkPhoneVerified();

                                                        }

                                                        @Override
                                                        public void onFailure(String message) {
                                                            checkPhoneVerified();
                                                        }
                                                    });
                                        } else {
                                            Support.displayToastMessage(LoginActivity.this, responseParser.getErrorMessage(new Gson().toJson(objectResponse.body())));
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(String message) {
//                                    Log.e("onSuccess: ", message);
                                    Support.displayToastMessage(LoginActivity.this, message);
                                }
                            });
                }
                break;
            case R.id.txt_forgot_password:
                showForgotPassword();
                break;
        }
    }

    private void showForgotPassword() {
        if (dialog == null) {
            dialog = new Dialog(LoginActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.pop_forgot_password);
            final CountryCodePicker ccp_country_id = dialog.findViewById(R.id.ccp_country_id);
            final EditText edtEmailPhone = dialog.findViewById(R.id.edtEmailPhone);
            TextView txtCancel = dialog.findViewById(R.id.txtCancel);
            TextView txtSubmit = dialog.findViewById(R.id.txtSubmit);

            edtEmailPhone.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (editable != null && !editable.toString().isEmpty() && Support.isPhoneNumber(editable.toString()))
                        ccp_country_id.setVisibility(View.VISIBLE);
                    else
                        ccp_country_id.setVisibility(View.GONE);
                }
            });


            txtCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            txtSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String userName = edtEmailPhone.getText().toString();
                    if (!userName.isEmpty()) {
                        if (!Support.isPhoneNumber(userName) && !Support.isValidEmail(userName)) {
                            displayToastMessage(LoginActivity.this, getResources().getString(R.string.invalid_email));
                        } else if (Support.isPhoneNumber(userName) && userName.length() < 7) {
                            displayToastMessage(LoginActivity.this, getResources().getString(R.string.invalid_mobile));
                        } else {
                            int country_code;
                            country_code = ccp_country_id.getSelectedCountryCodeAsInt();
                            forgotPassword(country_code, userName);
                        }
                    } else
                        edtEmailPhone.setError("Please enter valid Email / Phone");
                }
            });
            dialog.show();
        } else if (dialog.isShowing()) {
            dialog.dismiss();
            showForgotPassword();
        } else {
            dialog = null;
            showForgotPassword();
        }
    }

    private void forgotPassword(int country_code, String userName) {
        UserModel userModel = new UserModel();
        userModel.setRoleId(Vars.USER_ROLE);
        if (Support.isPhoneNumber(userName)) {
            userModel.setPhoneNoCode(country_code + "");
            userModel.setPhoneNo(userName);
        } else {
            userModel.setEmail(userName);
        }
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        ApiCall.getApiCall(LoginActivity.this, ApiCall.apiService.forgotPassword(userModel), true,
                new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Toast.makeText(LoginActivity.this,
                                responseParser.getErrorMessage(new Gson().toJson(objectResponse.body())), Toast.LENGTH_SHORT).show();
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        if (dialog != null) {
                            dialog.show();
                        }
                    }
                });
    }

    private void checkPhoneVerified() {
        if (Support.getPrefBoolean(getApplicationContext(), Vars.PHONE_VERIFIED)) {
            startActivity(new Intent(LoginActivity.this, MenuPageActivity.class));
            finish();
        } else {
            startActivity(new Intent(LoginActivity.this, VerificationActivity.class));
        }
    }

    private boolean isValidFields() {


        if (userName == null || userName.equalsIgnoreCase("")) {
            displayToastMessage(LoginActivity.this, getResources().getString(R.string.msg_email_id));
            return false;
        }

        if (!Support.isPhoneNumber(userName) && !Support.isValidEmail(userName)) {
            displayToastMessage(LoginActivity.this, getResources().getString(R.string.invalid_email));
            et_email_mobile.requestFocus();
            return false;
        }

        if (Support.isPhoneNumber(userName) && userName.length() < 7) {
            displayToastMessage(LoginActivity.this, getResources().getString(R.string.invalid_mobile));
            et_email_mobile.requestFocus();
            return false;
        }

        if (password == null || password.equalsIgnoreCase("")) {
            displayToastMessage(LoginActivity.this, getResources().getString(R.string.msg_password));
            return false;
        }

        if (!Support.isPasswordLengthValid(password)) {
            displayToastMessage(LoginActivity.this, getResources().getString(R.string.invalid_password_length));
            return false;
        }

        return true;
    }

    private class EmailTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable != null && !editable.toString().isEmpty() && Support.isPhoneNumber(editable.toString()))
                ccp_country_id.setVisibility(View.VISIBLE);
            else
                ccp_country_id.setVisibility(View.GONE);
        }
    }
}
