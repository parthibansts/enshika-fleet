package com.enshika.fleet_management.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.NewMobileMoneyModel;
import com.enshika.fleet_management.model.PaymentResponseModel;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;

import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;

/**
 * Created by Shamla Tech on 06-07-2018.
 */

public class PhoneVerificationActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    TextView txtProceed;
    PinView pinView;
    TextView txtHint, txtResend;
    String country_code = "", mobile_number = "";
    ResponseParser responseParser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verification);
        initUI();
        getBundleDetails();
    }

    private void sendOTP(String countryCode, String mobileNumber) {
        NewMobileMoneyModel model = new NewMobileMoneyModel();
        model.setMobileNumber(mobileNumber);
        model.setUserId(Support.getPref(getApplicationContext(), Vars.USER_ID));
        model.setCountryCode(countryCode);
        ApiCall.getApiCall(PhoneVerificationActivity.this,
                apiService.sendOtp(Support.getHeader(PhoneVerificationActivity.this), model),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        PaymentResponseModel model = responseParser.getPaymentModel(objectResponse.body());
                        if (model.getStatus() == 0)
                            Support.displayToastMessage(PhoneVerificationActivity.this, model.getMessage());
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(PhoneVerificationActivity.this, message);
                    }
                });
    }

    private void verifyOTP(String otp) {
        NewMobileMoneyModel model = new NewMobileMoneyModel();
        model.setUserId(Support.getPref(getApplicationContext(), Vars.USER_ID));
        model.setOtp(otp);
        ApiCall.getApiCall(PhoneVerificationActivity.this,
                apiService.verifyOtp(Support.getHeader(PhoneVerificationActivity.this), model),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        PaymentResponseModel model = responseParser.getPaymentModel(objectResponse.body());
                        Support.displayToastMessage(PhoneVerificationActivity.this, model.getMessage());
                        if (model.getStatus() == 1) {
                            Intent intent = new Intent();
                            intent.putExtra("mobile",mobile_number);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(PhoneVerificationActivity.this, message);
                    }
                });
    }

    private void getBundleDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("cc"))
                country_code = bundle.getString("cc");
            if (bundle.containsKey("mobile"))
                mobile_number = bundle.getString("mobile");
            txtHint.setText("Please enter verification code which you receive via SMS on " + country_code + mobile_number);
            sendOTP(country_code, mobile_number);
        }
    }


    private void initUI() {
        txtHint = findViewById(R.id.txtHint);
        txtResend = findViewById(R.id.txtResend);
        pinView = findViewById(R.id.pinView);
        txtProceed = findViewById(R.id.txtProceed);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Phone Verification");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        txtProceed.setOnClickListener(this);
        txtResend.setOnClickListener(this);

        responseParser = new ResponseParser(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtProceed:
                if (!pinView.getText().toString().isEmpty() && pinView.getText().toString().length() == 4) {
                    verifyOTP(pinView.getText().toString());
                } else {
                    Toast.makeText(this, "Please enter OTP!", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.txtResend:
                sendOTP(country_code, mobile_number);
                break;
        }
    }
}
