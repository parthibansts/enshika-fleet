package com.enshika.fleet_management.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.adapter.ServiceProviderAdapter;
import com.enshika.fleet_management.model.MobileMoneyOperators;
import com.enshika.fleet_management.model.NewMobileMoneyModel;
import com.enshika.fleet_management.model.PaymentResponseModel;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;
import com.hbb20.CountryCodePicker;

import java.util.ArrayList;

import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.Vars.REFRESH_SCREEN;

/**
 * Created by Shamla Tech on 06-07-2018.
 */

public class AddMobileMoneyActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txtProceed;
    Toolbar toolbar;
    EditText edtPhoneNumber;
    RecyclerView recyclerServiceProvider;
    ServiceProviderAdapter serviceProviderAdapter;
    ArrayList<MobileMoneyOperators> mobileMoneyModels;
    String selectedProviderId = "";
    ResponseParser responseParser;
    CountryCodePicker ccp_country_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mobile_money);
        initUI();
        prepareServiceProviders();
    }

    private void prepareRecyclerServiceProviders() {
        serviceProviderAdapter = new ServiceProviderAdapter(this, mobileMoneyModels, selectedProviderId);
        recyclerServiceProvider.setLayoutManager(new LinearLayoutManager(this));
        recyclerServiceProvider.setAdapter(serviceProviderAdapter);
    }

    private void prepareServiceProviders() {
        ApiCall.getApiCall(AddMobileMoneyActivity.this,
                apiService.getMobileOperatorList(Support.getHeader(AddMobileMoneyActivity.this)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        mobileMoneyModels = new ArrayList<>();
                        PaymentResponseModel model = responseParser.getPaymentModel(objectResponse.body());
                        if (model.getStatus() == 1) {
                            mobileMoneyModels = model.getData().getMobileMoneyOperators();
                            prepareRecyclerServiceProviders();
                        } else {
                            Support.displayToastMessage(AddMobileMoneyActivity.this, model.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(AddMobileMoneyActivity.this, message);
                    }
                });
    }

    private void addMobileMoney(String mobileOperatorId, final String countryCodeWithPlus,
                                final String countryCode, final String mobileNumber) {
        NewMobileMoneyModel model = new NewMobileMoneyModel();
        model.setMobileOperatorId(mobileOperatorId);
        model.setMobNo(mobileNumber);
        model.setUserId(Support.getPref(getApplicationContext(), Vars.USER_ID));
        model.setCountryCode(countryCode);
        ApiCall.getApiCall(AddMobileMoneyActivity.this,
                apiService.addMobileMoney(Support.getHeader(AddMobileMoneyActivity.this), model),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        PaymentResponseModel model = responseParser.getPaymentModel(objectResponse.body());
                        if (model.getStatus() == 1) {
                            Intent intent = new Intent(AddMobileMoneyActivity.this, PhoneVerificationActivity.class);
                            intent.putExtra("cc", countryCode);
                            intent.putExtra("mobile", mobileNumber);
                            startActivityForResult(intent, REFRESH_SCREEN);
                        } else {
                            Support.displayToastMessage(AddMobileMoneyActivity.this, model.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(AddMobileMoneyActivity.this, message);
                    }
                });
    }

    private void initUI() {
        ccp_country_id = findViewById(R.id.ccp_country_id);
        recyclerServiceProvider = findViewById(R.id.recyclerServiceProvider);
        edtPhoneNumber = findViewById(R.id.edtPhoneNumber);
        txtProceed = findViewById(R.id.txtProceed);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Add Mobile Money");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mobileMoneyModels = new ArrayList<>();
        responseParser = new ResponseParser(this);
        txtProceed.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtProceed:
                if (isValid()) {
                    addMobileMoney(selectedProviderId, ccp_country_id.getSelectedCountryCodeWithPlus(),
                            ccp_country_id.getSelectedCountryCode(), edtPhoneNumber.getText().toString());
                }
                break;
        }
    }

    private boolean isValid() {
        boolean isValid = true;
        if (edtPhoneNumber.getText().toString().isEmpty() || edtPhoneNumber.getText().toString().length() != 10) {
            edtPhoneNumber.setError("please enter valid mobile number");
            isValid = false;
        } else if (selectedProviderId.isEmpty()) {
            Toast.makeText(getApplicationContext(), "please select mobile operator!", Toast.LENGTH_LONG).show();
            isValid = false;
        }
        return isValid;
    }

    public void onProviderListClick(int position) {
        selectedProviderId = mobileMoneyModels.get(position).getOperatorId() + "";
        serviceProviderAdapter.updateSelectedProvider(selectedProviderId);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REFRESH_SCREEN && resultCode == RESULT_OK) {
            Intent intent = new Intent();
            intent.putExtra("mobile",data.getStringExtra("mobile"));
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
