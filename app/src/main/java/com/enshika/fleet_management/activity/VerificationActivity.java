package com.enshika.fleet_management.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;
import com.google.gson.Gson;
import com.hbb20.CountryCodePicker;
import com.rengwuxian.materialedittext.MaterialEditText;

import retrofit2.Response;

/**
 * Created by Shamla Tech on 01-08-2018.
 */

public class VerificationActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txtPhoneHint, txtResend, txtSubmit, txtChangePhoneNumber;
    MaterialEditText edtVerificationCode;

    ResponseParser responseParser;
    Toolbar toolbar;
    Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        initUI();
        prepareScreen();
    }

    private void prepareScreen() {
        String strCode, strPhone;
        strCode = Support.getPref(getApplicationContext(), Vars.PHONE_CODE);
        strPhone = Support.getPref(getApplicationContext(), Vars.PHONE_NO);
        txtPhoneHint.setText("Please enter verification code which you receive via SMS on " + strCode + " " + strPhone);
    }

    private void initUI() {
        txtPhoneHint = findViewById(R.id.txtPhoneHint);
        txtResend = findViewById(R.id.txtResend);
        txtSubmit = findViewById(R.id.txtSubmit);
        txtChangePhoneNumber = findViewById(R.id.txtChangePhoneNumber);
        edtVerificationCode = findViewById(R.id.edtVerificationCode);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Phone Verification");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        txtChangePhoneNumber.setOnClickListener(this);
        txtSubmit.setOnClickListener(this);
        txtResend.setOnClickListener(this);

        responseParser = new ResponseParser(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtSubmit:
                if (isValid()) {
                    checkVerificationCodeValid(edtVerificationCode.getText().toString());
                }
                break;
            case R.id.txtResend:
                resendVerificationCode();
                break;
            case R.id.txtChangePhoneNumber:
                showChangeMobileNumber();
                break;
        }
    }

    private void resendVerificationCode() {
        ApiCall.getApiCall(VerificationActivity.this, ApiCall.apiService.resendVerification(Support.getHeader(getApplicationContext())),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Toast.makeText(VerificationActivity.this, responseParser.getErrorMessage(new Gson().toJson(objectResponse.body())), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(String message) {
                        Toast.makeText(VerificationActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void checkVerificationCodeValid(String s) {
        ApiCall.getApiCall(VerificationActivity.this,
                ApiCall.apiService.updateVerification(Support.getHeader(getApplicationContext()), s), true,
                new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Toast.makeText(VerificationActivity.this, responseParser.getErrorMessage(new Gson().toJson(objectResponse.body())), Toast.LENGTH_SHORT).show();
                        Support.setPref(getApplicationContext(), Vars.PHONE_VERIFIED, true);
                        Intent intent = new Intent(VerificationActivity.this, MenuPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(String message) {
                        Toast.makeText(VerificationActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void showChangeMobileNumber() {
        if (dialog == null) {
            dialog = new Dialog(VerificationActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.pop_change_mobile);
            final CountryCodePicker ccp_country_id = dialog.findViewById(R.id.ccp_country_id);
            final EditText edtPhoneNumber = dialog.findViewById(R.id.edtPhoneNumber);
            TextView txtCancel = dialog.findViewById(R.id.txtCancel);
            TextView txtSubmit = dialog.findViewById(R.id.txtSubmit);

            txtCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            txtSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!edtPhoneNumber.getText().toString().isEmpty() && edtPhoneNumber.getText().toString().length() == 10) {
                        updatePhoneNumber(ccp_country_id.getSelectedCountryCodeWithPlus(), ccp_country_id.getSelectedCountryCodeAsInt(), edtPhoneNumber.getText().toString());
                    } else
                        edtPhoneNumber.setError("Please enter valid phone number");
                }
            });
            dialog.show();
        } else if (dialog.isShowing()) {
            dialog.dismiss();
            showChangeMobileNumber();
        } else {
            dialog = null;
            showChangeMobileNumber();
        }
    }

    private void updatePhoneNumber(final String ccp, final int cc, final String mobile) {
        ApiCall.getApiCall(VerificationActivity.this,
                ApiCall.apiService.updatePhone(Support.getHeader(getApplicationContext()), cc + "", mobile),
                true,
                new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Toast.makeText(VerificationActivity.this, responseParser.getErrorMessage(new Gson().toJson(objectResponse.body())), Toast.LENGTH_SHORT).show();
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        Support.setPref(getApplicationContext(), Vars.USER_MOB, mobile);
                        Support.setPref(getApplicationContext(), Vars.USER_MOB_CODE, ccp + "");
                        Support.setPref(getApplicationContext(), Vars.PHONE_CODE, ccp + "");
                        Support.setPref(getApplicationContext(), Vars.PHONE_NO, mobile);
                        prepareScreen();
                    }

                    @Override
                    public void onFailure(String message) {
                        Toast.makeText(VerificationActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private boolean isValid() {
        boolean isValid = true;
        if (edtVerificationCode.getText().toString().isEmpty())
            isValid = false;
        return isValid;
    }
}
