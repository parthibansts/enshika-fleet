package com.enshika.fleet_management.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.BookingDetailModel;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.Support.getTimeFromLong;

/**
 * Created by Shamla Tech on 12-06-2018.
 */

public class BookingDetailActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {
    Toolbar toolbar;
    String tourId = "";
    GoogleMap googleMap;
    SupportMapFragment supportMapFragment;
    LinearLayout linearDetails;
    NestedScrollView nestedScrollContent;
    ImageView imgFullScreen;
    TextView txtBookingStatus, txtSatellite, txtMap;
    TextView txtPassengerName, txtPassengerPhone, txtPassengerEmail, txtDriverName, txtDriverPhone, txtDriverEmail, txtTripTime, txtPickupLocation,
            txtDestinationLocation, txtClockedFare, txtRefundAmount, txtPaymentMode, txtPaymentStatus, txtSurgeRate, txtServiceType,
            txtCarType, txtFreeDistance, txtDistance, txtDuration, txtWaitingTime, txtBaseFare, txtDistanceFare, txttimeFare, txtWaitingFare,
            txtSurgeFare, txtFare, txtPromoDiscount, txtTotalTax, txtTotal, txtUsedCredits, txtAmountCollected, txtPassengerComments,
            txtDriverComments, txtCashCollected, txtTripId;
    MaterialRatingBar materialRatingPassenger, materialRatingDriver;
    RelativeLayout relativeMapView;
    boolean isFullScreen = false;
    ResponseParser responseParser;
    BookingDetailModel bookingDetailModel;
    private int defaultZoom = 14;
    private Marker markerPickup;
    private Marker markerDestination;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_detail);

        initUI();
        getBundleDetails();
        initializeMap();
        prepareDatas();

    }

    private void prepareDatas() {
        ApiCall.getApiCall(this,
                apiService.getBookingDetails(Support.getHeader(getApplicationContext()), tourId),
                true, new ApiCall.OnApiResult() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        bookingDetailModel = responseParser.getBookingDetail(objectResponse.body());
                        txtTripId.setText(tourId);
                        txtPassengerName.setText(bookingDetailModel.getPFirstName() + " " + bookingDetailModel.getPLastName());
                        txtPassengerPhone.setText(bookingDetailModel.getPPhone());
                        txtPassengerEmail.setText(bookingDetailModel.getPEmail());
                        txtDriverName.setText(bookingDetailModel.getFirstName());
                        txtDriverPhone.setText(bookingDetailModel.getPhoneNoCode() + bookingDetailModel.getPhoneNo());
                        txtDriverEmail.setText(bookingDetailModel.getEmail());
                        txtTripTime.setText(Support.getDateFromLong(bookingDetailModel.getBookingTime(), Vars.DATE_FORMAT));
                        txtPickupLocation.setText(bookingDetailModel.getSourceAddress());
                        txtDestinationLocation.setText(bookingDetailModel.getDestinationAddress());
                        txtClockedFare.setText(bookingDetailModel.getBookingFees() + " " + Support.getCurrencySymbol(BookingDetailActivity.this)); //to be confirm
                        txtRefundAmount.setText(bookingDetailModel.getRefundAmount() + " " + Support.getCurrencySymbol(BookingDetailActivity.this));
                        txtPaymentMode.setText(bookingDetailModel.getPaymentMode());
                        txtPaymentStatus.setText(bookingDetailModel.getPaymentStatus());
                        txtSurgeRate.setText(bookingDetailModel.getSurgeRate());
                        if (bookingDetailModel.getRideLater())
                            txtServiceType.setText("Ride Later"); //ride now or later
                        else
                            txtServiceType.setText("Ride Now");
                        txtCarType.setText(bookingDetailModel.getCarType());
                        txtFreeDistance.setText(Support.convertIntoUnitTypeValue(getApplicationContext(), bookingDetailModel.getFreeDistance()));
                        txtDistance.setText(Support.convertIntoUnitTypeValue(getApplicationContext(), bookingDetailModel.getDistance()));
                        txtDuration.setText(getTimeFromLong(bookingDetailModel.getDuration()));
                        txtWaitingTime.setText(getTimeFromLong(bookingDetailModel.getWaitingTime()));
                        txtBaseFare.setText(bookingDetailModel.getMinimumFare() + " " + Support.getCurrencySymbol(BookingDetailActivity.this));
                        txtDistanceFare.setText(bookingDetailModel.getPerKmFare() + " " + Support.getCurrencySymbol(BookingDetailActivity.this));
                        txttimeFare.setText(bookingDetailModel.getPerMinuteFare() + " " + Support.getCurrencySymbol(BookingDetailActivity.this));
                        txtWaitingFare.setText(bookingDetailModel.getWaitingTimeFare() + " " + Support.getCurrencySymbol(BookingDetailActivity.this));
                        txtSurgeFare.setText(bookingDetailModel.getSurgeFare() + " " + Support.getCurrencySymbol(BookingDetailActivity.this));
                        txtFare.setText(bookingDetailModel.getInitialFare() + " " + Support.getCurrencySymbol(BookingDetailActivity.this));
                        txtPromoDiscount.setText(bookingDetailModel.getPromoDiscount() + " " + Support.getCurrencySymbol(BookingDetailActivity.this));
                        txtTotalTax.setText(bookingDetailModel.getTotalTaxAmount() + " " + Support.getCurrencySymbol(BookingDetailActivity.this));
                        txtTotal.setText(bookingDetailModel.getTotal() + " " + Support.getCurrencySymbol(BookingDetailActivity.this));
                        txtUsedCredits.setText(bookingDetailModel.getUsedCredits() + " " + Support.getCurrencySymbol(BookingDetailActivity.this));
                        txtAmountCollected.setText(bookingDetailModel.getFinalAmountCollected() + " " + Support.getCurrencySymbol(BookingDetailActivity.this));
                        materialRatingPassenger.setRating(Float.parseFloat(bookingDetailModel.getPRate()));
                        materialRatingDriver.setRating(Float.parseFloat(bookingDetailModel.getDRate()));
                        txtPassengerComments.setText(bookingDetailModel.getPNote());
                        txtDriverComments.setText(bookingDetailModel.getDNote());
                        txtCashCollected.setText("Cash Collected - " + bookingDetailModel.getFinalAmountCollected() + " " + Support.getCurrencySymbol(BookingDetailActivity.this));
                        txtBookingStatus.setText(bookingDetailModel.getStatus());
                        if (googleMap != null) {
                            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(bookingDetailModel.getSLatitude(), bookingDetailModel.getSLongitude()), defaultZoom);
                            googleMap.moveCamera(cameraUpdate);
                            prepareMarkers();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(BookingDetailActivity.this, message);
                    }
                });
    }

    private void getBundleDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            tourId = bundle.getString("tourId");
        }
    }

    private void initUI() {
        txtTripId = findViewById(R.id.txtTripId);
        materialRatingPassenger = findViewById(R.id.materialRatingPassenger);
        materialRatingDriver = findViewById(R.id.materialRatingDriver);
        txtPassengerName = findViewById(R.id.txtPassengerName);
        txtPassengerPhone = findViewById(R.id.txtPassengerPhone);
        txtPassengerEmail = findViewById(R.id.txtPassengerEmail);
        txtDriverName = findViewById(R.id.txtDriverName);
        txtDriverPhone = findViewById(R.id.txtDriverPhone);
        txtDriverEmail = findViewById(R.id.txtDriverEmail);
        txtTripTime = findViewById(R.id.txtTripTime);
        txtPickupLocation = findViewById(R.id.txtPickupLocation);
        txtDestinationLocation = findViewById(R.id.txtDestinationLocation);
        txtClockedFare = findViewById(R.id.txtClockedFare);
        txtRefundAmount = findViewById(R.id.txtRefundAmount);
        txtPaymentMode = findViewById(R.id.txtPaymentMode);
        txtPaymentStatus = findViewById(R.id.txtPaymentStatus);
        txtSurgeRate = findViewById(R.id.txtSurgeRate);
        txtServiceType = findViewById(R.id.txtServiceType);
        txtCarType = findViewById(R.id.txtCarType);
        txtFreeDistance = findViewById(R.id.txtFreeDistance);
        txtDistance = findViewById(R.id.txtDistance);
        txtDuration = findViewById(R.id.txtDuration);
        txtWaitingTime = findViewById(R.id.txtWaitingTime);
        txtBaseFare = findViewById(R.id.txtBaseFare);
        txtDistanceFare = findViewById(R.id.txtDistanceFare);
        txttimeFare = findViewById(R.id.txttimeFare);
        txtWaitingFare = findViewById(R.id.txtWaitingFare);
        txtSurgeFare = findViewById(R.id.txtSurgeFare);
        txtFare = findViewById(R.id.txtFare);
        txtPromoDiscount = findViewById(R.id.txtPromoDiscount);
        txtTotalTax = findViewById(R.id.txtTotalTax);
        txtTotal = findViewById(R.id.txtTotal);
        txtUsedCredits = findViewById(R.id.txtUsedCredits);
        txtAmountCollected = findViewById(R.id.txtAmountCollected);
        txtPassengerComments = findViewById(R.id.txtPassengerComments);
        txtDriverComments = findViewById(R.id.txtDriverComments);
        txtCashCollected = findViewById(R.id.txtCashCollected);
        nestedScrollContent = findViewById(R.id.nestedScrollContent);
        imgFullScreen = findViewById(R.id.imgFullScreen);
        txtMap = findViewById(R.id.txtMap);
        txtSatellite = findViewById(R.id.txtSatellite);
        relativeMapView = findViewById(R.id.relativeMapView);
        txtBookingStatus = findViewById(R.id.txtBookingStatus);
        linearDetails = findViewById(R.id.linearDetails);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Booking Details");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        bookingDetailModel = new BookingDetailModel();
        responseParser = new ResponseParser(this);
        linearDetails.setVisibility(View.VISIBLE);
        txtBookingStatus.setVisibility(View.VISIBLE);

        txtMap.setOnClickListener(this);
        txtSatellite.setOnClickListener(this);
        imgFullScreen.setOnClickListener(this);
    }

    private void initializeMap() {
        if (googleMap == null) {
            supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            supportMapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap mGoogleMap) {
        try {
            googleMap = mGoogleMap;
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(bookingDetailModel.getSLatitude(), bookingDetailModel.getSLongitude()), defaultZoom);
            googleMap.moveCamera(cameraUpdate);
//            prepareMarkers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepareMarkers() {
        try {
            markerPickup = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(bookingDetailModel.getSLatitude(), bookingDetailModel.getSLongitude()))
                    .snippet(bookingDetailModel.getSourceAddress())
                    .title("Pickup Location"));
            markerPickup.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.user_pin));

            markerDestination = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(bookingDetailModel.getDLatitude(), bookingDetailModel.getDLongitude()))
                    .snippet(bookingDetailModel.getDestinationAddress())
                    .title("Destination Location"));
            markerDestination.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.destination_pin));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtMap:
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.txtSatellite:
                googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.imgFullScreen:
                isFullScreen = !isFullScreen;
                showFullScreen(isFullScreen);
                break;
        }
    }

    private void showFullScreen(boolean isShow) {
        if (isShow) {
            txtBookingStatus.setVisibility(View.GONE);
            linearDetails.setVisibility(View.GONE);
            imgFullScreen.setImageResource(R.drawable.ic_close_full_screen);
            nestedScrollContent.setVisibility(View.GONE);
        } else {
            txtBookingStatus.setVisibility(View.VISIBLE);
            linearDetails.setVisibility(View.VISIBLE);
            imgFullScreen.setImageResource(R.drawable.ic_full_screen);
            nestedScrollContent.setVisibility(View.VISIBLE);
        }
    }
}
