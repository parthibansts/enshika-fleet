package com.enshika.fleet_management.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.SaveTransactionModel;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;

import retrofit2.Response;

/**
 * Created by Shamla Tech on 25-07-2018.
 */

public class TransactionFailureActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txtOk, txtReason, txtTransactionId;
    Toolbar toolbar;
    String payment_mode, transaction_id;
    ResponseParser responseParser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_failure);
        iniUI();
        getBundleDetails();
    }

    private void getBundleDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            txtReason.setText("Transaction Declined : " + bundle.getString("reason"));
            payment_mode = bundle.getInt("payment_mode") + "";
            transaction_id = bundle.getString("id");
            txtTransactionId.setText(transaction_id);
        }
    }

    private void iniUI() {
        txtOk = findViewById(R.id.txtOk);
        txtReason = findViewById(R.id.txtReason);
        txtTransactionId = findViewById(R.id.txtTransactionId);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Transaction Failure");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        txtOk.setOnClickListener(this);

        responseParser = new ResponseParser(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtOk:
                saveTransaction();
                break;
        }
    }

    private void saveTransaction() {
        SaveTransactionModel saveTransactionModel = new SaveTransactionModel();
        saveTransactionModel.setTransactionId(transaction_id);
        saveTransactionModel.setStatus("0");
//        saveTransactionModel.setUser_id(Support.getPref(getApplicationContext(), Vars.USER_ID));
        saveTransactionModel.setPaymentMode(payment_mode);
        ApiCall.getApiCall(TransactionFailureActivity.this, ApiCall.apiService.saveTransaction(saveTransactionModel),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        setResult(RESULT_CANCELED, new Intent());
                        finish();
                    }

                    @Override
                    public void onFailure(String message) {
                        setResult(RESULT_CANCELED, new Intent());
                        finish();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        saveTransaction();
    }
}
