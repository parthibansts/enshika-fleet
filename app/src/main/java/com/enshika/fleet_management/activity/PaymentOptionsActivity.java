package com.enshika.fleet_management.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.cardform.view.CardForm;
import com.enshika.fleet_management.R;

/**
 * Created by Shamla Tech on 12-06-2018.
 */

public class PaymentOptionsActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnPay;
    ImageView imgWallet, imgCardPayment;
    TextView txtWallet, txtCardPayment, txtWalletBalance, txtPayableAmount;
    View viewInactiveWallet, viewInactiveCard;
    RelativeLayout relativeWallet, relativeCard;
    String selectedPaymentOption = "wallet";
    CardForm card_form;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_options);
        initUI();
        initCardForm();
        prepareViews();
    }

    private void initCardForm() {
        card_form.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(true)
                .postalCodeRequired(false)
                .actionLabel("Purchase")
                .setup(this);
    }

    private void initUI() {
        card_form = findViewById(R.id.card_form);
        txtWalletBalance = findViewById(R.id.txtWalletBalance);
        txtPayableAmount = findViewById(R.id.txtPayableAmount);
        btnPay = findViewById(R.id.btnPay);
        imgWallet = findViewById(R.id.imgWallet);
        imgCardPayment = findViewById(R.id.imgCardPayment);
        txtWallet = findViewById(R.id.txtWallet);
        txtCardPayment = findViewById(R.id.txtCardPayment);
        viewInactiveCard = findViewById(R.id.viewInactiveCard);
        viewInactiveWallet = findViewById(R.id.viewInactiveWallet);
        relativeWallet = findViewById(R.id.relativeWallet);
        relativeCard = findViewById(R.id.relativeCard);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Payment Options");
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnPay.setOnClickListener(this);
        relativeWallet.setOnClickListener(this);
        relativeCard.setOnClickListener(this);
    }

    private void prepareViews() {
        prepareSelectedItem("wallet");
    }

    private void prepareSelectedItem(String selected_item) {
        switch (selected_item) {
            case "wallet":
                viewInactiveWallet.setVisibility(View.GONE);
                txtWallet.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLightTrans));
                viewInactiveCard.setVisibility(View.VISIBLE);
                txtCardPayment.setBackgroundColor(getResources().getColor(R.color.colorGrayLightTrans));
                txtWalletBalance.setVisibility(View.VISIBLE);
                card_form.setVisibility(View.GONE);
                break;
            case "card":
                viewInactiveCard.setVisibility(View.GONE);
                txtCardPayment.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLightTrans));
                viewInactiveWallet.setVisibility(View.VISIBLE);
                txtWallet.setBackgroundColor(getResources().getColor(R.color.colorGrayLightTrans));
                txtWalletBalance.setVisibility(View.GONE);
                card_form.setVisibility(View.VISIBLE);
                break;
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPay:
                if (selectedPaymentOption.equals("card")) {
                    if (card_form.isValid()) {
                        setResult(RESULT_OK, new Intent());
                        finish();
                    } else
                        Toast.makeText(this, "Please enter valid details!", Toast.LENGTH_SHORT).show();
                } else if (selectedPaymentOption.equals("wallet")) {
                    setResult(RESULT_OK, new Intent());
                    finish();
                }
                break;
            case R.id.relativeCard:
                selectedPaymentOption = "card";
                prepareSelectedItem(selectedPaymentOption);
                break;
            case R.id.relativeWallet:
                selectedPaymentOption = "wallet";
                prepareSelectedItem(selectedPaymentOption);
                break;
        }
    }
}
