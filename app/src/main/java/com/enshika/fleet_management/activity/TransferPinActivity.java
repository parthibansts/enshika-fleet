package com.enshika.fleet_management.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.StatusMessageModel;
import com.enshika.fleet_management.model.WalletSetPinPojo;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;

import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;

/**
 * Created by Shamla Tech on 06-07-2018.
 */

public class TransferPinActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    TextView txtProceed;
    ResponseParser responseParser;
    PinView pinView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_complete_pin);
        initUI();
    }

    private void initUI() {
        pinView = findViewById(R.id.pinView);
        txtProceed = findViewById(R.id.txtProceed);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Complete Transfer");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        txtProceed.setOnClickListener(this);

        responseParser = new ResponseParser(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtProceed:
                if (!pinView.getText().toString().isEmpty() && pinView.getText().toString().length() == 4) {
                    validatePin(pinView.getText().toString());
                } else {
                    Toast.makeText(this, "Please enter 4 digit pin!", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void validatePin(String pin) {
        WalletSetPinPojo walletPinPojo = new WalletSetPinPojo();
        walletPinPojo.setUserId(Support.getPref(getApplicationContext(), Vars.USER_ID));
        walletPinPojo.setWalletPin(pin);
        ApiCall.getApiCall(TransferPinActivity.this, apiService.validatePin(Support.getHeader(getApplicationContext()), walletPinPojo),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        StatusMessageModel model = responseParser.getStatusMessage(objectResponse.body());
                        if (model.getStatus().equals("1")) {
                            setResult(RESULT_OK, new Intent());
                            finish();
                        } else {
                            Toast.makeText(TransferPinActivity.this, model.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(TransferPinActivity.this, message);
                    }
                });
    }
}
