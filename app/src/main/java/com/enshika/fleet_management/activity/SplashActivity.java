package com.enshika.fleet_management.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.enshika_corporate.RegCorporateStepOne;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;

import org.json.JSONObject;

import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
//        ShowUserTypePopUp();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Support.getPref(SplashActivity.this, Vars.USER_ID).isEmpty()) {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                } else if (Support.getPrefBoolean(SplashActivity.this, Vars.PHONE_VERIFIED)) {
                    startActivity(new Intent(SplashActivity.this, MenuPageActivity.class));
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    intent.putExtra("reqPhoneVerification", true);
                    startActivity(intent);
                    finish();
                }
            }
        }, 2000);
    }

    private void ShowUserTypePopUp() {
        if (dialog == null) {
            dialog = new Dialog(SplashActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.pop_choose_reg_type);
            LinearLayout lrStudent = dialog.findViewById(R.id.lrStudent);
            LinearLayout lrCorporate = dialog.findViewById(R.id.lrCorporate);
            LinearLayout lrIndividual = dialog.findViewById(R.id.lrIndividual);

            lrIndividual.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            lrCorporate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent intent = new Intent(SplashActivity.this, RegCorporateStepOne.class);
                    intent.putExtra("type", "corporate");
                    startActivity(intent);
                }
            });
            lrStudent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent intent = new Intent(SplashActivity.this, RegCorporateStepOne.class);
                    intent.putExtra("type", "student");
                    startActivity(intent);
                }
            });
            dialog.show();
        } else if (dialog.isShowing()) {
            dialog.dismiss();
            ShowUserTypePopUp();
        } else {
            dialog = null;
            ShowUserTypePopUp();
        }
    }

}
