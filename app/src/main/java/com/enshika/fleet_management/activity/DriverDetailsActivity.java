package com.enshika.fleet_management.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.adapter.DriverFragmentAdapter;
import com.enshika.fleet_management.model.DriverDetailModel;
import com.enshika.fleet_management.model.DriversPojo;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ADMIN on 20-Apr-18.
 */

public class DriverDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    int PAYMENT_REFRESH = 23;
    Toolbar toolbar;
    TabLayout tab_driver;
    ViewPager viewpager_driver;
    CollapsingToolbarLayout collapsing_toolbar;
    LinearLayout linearDesposit;
    TextView txtDriver, txtRating, txtSecurityDeposit;
    DriverFragmentAdapter driverFragmentAdapter;
    DriversPojo driversPojo;
    CircleImageView imgDriver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_detail);
        initUI();
        getBundleDetails();
        initFragmentViewPager();

    }


    private void getBundleDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("detail")) {
                driversPojo = (DriversPojo) bundle.getSerializable("detail");

                if (driversPojo != null) {
                    initToolbar(driversPojo.getFirstName());
                    txtDriver.setText(driversPojo.getFirstName());
                    txtRating.setText(driversPojo.getRate() + "*");
                    Support.loadImage(driversPojo.getPhotoUrl(), imgDriver, R.drawable.ic_user_placeholder);
                }
            }
        }
    }

    private void initUI() {
        imgDriver = findViewById(R.id.imgDriver);
        txtRating = findViewById(R.id.txtRating);
        txtDriver = findViewById(R.id.txtDriver);
        txtSecurityDeposit = findViewById(R.id.txtSecurityDeposit);
        linearDesposit = findViewById(R.id.linearDesposit);
        collapsing_toolbar = findViewById(R.id.collapsing_toolbar);
        tab_driver = findViewById(R.id.tab_driver);
        viewpager_driver = findViewById(R.id.viewpager_driver);


    }

    private void initToolbar(String title) {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        collapsing_toolbar.setTitle(title);
        collapsing_toolbar.setCollapsedTitleTextColor(Color.WHITE);
        collapsing_toolbar.setExpandedTitleColor(Color.TRANSPARENT);
    }


    private void initFragmentViewPager() {
        driverFragmentAdapter = new DriverFragmentAdapter(DriverDetailsActivity.this, getSupportFragmentManager(), driversPojo.getDriverId());
        viewpager_driver.setAdapter(driverFragmentAdapter);
        tab_driver.setupWithViewPager(viewpager_driver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.txtNoActivePass:
//                Intent intent = new Intent(this, PaymentOptionsActivity.class);
//                startActivityForResult(intent, PAYMENT_REFRESH);
//                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYMENT_REFRESH && resultCode == RESULT_OK) {
//            prepareValidityView();
        }
    }

    public void onClickImage(int position, ArrayList<String> urls) {
        new ImageViewer.Builder(DriverDetailsActivity.this, urls)
                .setStartPosition(position).hideStatusBar(false).allowSwipeToDismiss(true).allowZooming(true)
                .show();
    }

    public void loadDriverDatas(DriverDetailModel driverDetailModel) {
        Support.loadImage(driverDetailModel.getPhotoUrl(), imgDriver, R.drawable.ic_user_placeholder_white);
        txtDriver.setText(driverDetailModel.getFirstName());
        linearDesposit.setVisibility(View.VISIBLE);
        txtSecurityDeposit.setText(driverDetailModel.getDepositBalance() + " " + Support.getPref(getApplicationContext(), Vars.CURRENCY_SYMBOL));
        initToolbar(driverDetailModel.getFirstName());
        setResult(RESULT_OK, new Intent());
    }
}
