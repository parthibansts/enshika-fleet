package com.enshika.fleet_management.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.adapter.CardListAdapter;
import com.enshika.fleet_management.model.CardDetailsPojo;
import com.enshika.fleet_management.model.CardModel;
import com.enshika.fleet_management.model.CardPojo;
import com.enshika.fleet_management.model.MerchantPojo;
import com.enshika.fleet_management.model.MerchantResponseModel;
import com.enshika.fleet_management.model.PayCardModel;
import com.enshika.fleet_management.model.SaveTransactionModel;
import com.enshika.fleet_management.model.TransactionResultModel;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.ApiCall.apiServiceTeller;
import static com.enshika.fleet_management.utils.Vars.PAYMENT_FAILURE;
import static com.enshika.fleet_management.utils.Vars.PAYMENT_WEBVIEW;
import static com.enshika.fleet_management.utils.Vars.REFRESH_SCREEN;
import static com.enshika.fleet_management.utils.Vars.TELLER_CALLBACK_URL;
import static com.enshika.fleet_management.utils.Vars.TELLER_DESCRIPTION;
import static com.enshika.fleet_management.utils.Vars.TELLER_URL;

/**
 * Created by Shamla Tech on 20-07-2018.
 */

public class CardListActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView recyclerCardList;
    CardListAdapter cardListAdapter;
    Toolbar toolbar;
    ArrayList<CardPojo> cardPojos;
    TextView txtAddNew;
    boolean isWalletTransfer = false;
    ResponseParser responseParser;
    ArrayList<CardModel> cardModels;
    MerchantPojo merchantPojo;
    Dialog dialog;
    String strCvv = "";
    String money = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_list);
        initUI();
        getBundleDetails();

    }

    private void getBundleDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("isWalletTransfer"))
                isWalletTransfer = bundle.getBoolean("isWalletTransfer");
            money = bundle.getString("money");
            if (bundle.containsKey("merchantPojo"))
                merchantPojo = (MerchantPojo) bundle.getSerializable("merchantPojo");
        }
        prepareScreen();
    }

    private void prepareScreen() {
        if (isWalletTransfer) {
            getCardList();
        } else {
            getMerchantDetails();
        }
    }

    private void getMerchantDetails() {
        ApiCall.getApiCall(CardListActivity.this, apiService.getMerchantDetails(
                Support.getHeader(getApplicationContext()), Support.getPref(getApplicationContext(), Vars.USER_ID)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        MerchantResponseModel model = responseParser.getMerchantResponseModel(objectResponse.body());
                        if (model.getStatus() == 1) {
                            merchantPojo = model.getData();
                            getCardList();
                        } else {
                            Toast.makeText(CardListActivity.this, model.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Toast.makeText(CardListActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void getCardList() {
        ApiCall.getApiCall(CardListActivity.this, apiServiceTeller.getCardList(merchantPojo.getMerchantId(),
                merchantPojo.getUserId()),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        try {
                            cardModels = responseParser.getCardList(objectResponse.body());
                            prepareCardAdapter();
                            if (!isWalletTransfer)
                                enableSwipe();
                        } catch (Exception e) {
                            e.printStackTrace();
                            prepareCardList();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        prepareCardList();
                    }
                });
    }


    private void enableSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
                    removeCard(position);
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;
                    Paint p = new Paint();
                    if (dX > 0) {
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_white);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    } else {
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_white);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerCardList);
    }

    private void removeCard(final int position) {
        final CardModel cardModel = cardModels.get(position);
        cardModels.remove(position);
        cardListAdapter.notifyDataSetChanged();
        ApiCall.getApiCall(CardListActivity.this, ApiCall.apiServiceTeller.removeCard(cardModel),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
//                        objectResponse.body();
                    }

                    @Override
                    public void onFailure(String message) {
                        cardModels.add(cardModel);
                        cardListAdapter.notifyDataSetChanged();
                    }
                });
    }

    private void prepareCardAdapter() {
        cardListAdapter = new CardListAdapter(this, cardModels, isWalletTransfer);
        recyclerCardList.setAdapter(cardListAdapter);
    }

    private void prepareCardList() {
        cardModels = new ArrayList<>();
        CardModel cardPojo;
        for (int i = 0; i < 6; i++) {
            cardPojo = new CardModel();
            CardDetailsPojo cardDetailsPojo = new CardDetailsPojo();
            cardDetailsPojo.setHolder_name("Fleet Manager");
            cardDetailsPojo.setWallet_number("378282246310005");
            cardDetailsPojo.setWallet_name(Vars.VISA_CARD);
            cardDetailsPojo.setExpiry_date("0924");
            cardPojo.setDetails(cardDetailsPojo);
            cardModels.add(cardPojo);
        }

        prepareCardAdapter();
        if (!isWalletTransfer)
            enableSwipe();
    }

    private void initUI() {
        txtAddNew = findViewById(R.id.txtAddNew);
        recyclerCardList = findViewById(R.id.recyclerCardList);
        recyclerCardList.setLayoutManager(new LinearLayoutManager(this));

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Select your card");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        cardPojos = new ArrayList<>();
        merchantPojo = new MerchantPojo();
        responseParser = new ResponseParser(this);
        cardModels = new ArrayList<>();
        txtAddNew.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtAddNew:
                Intent intent = new Intent(this, NewCardActivity.class);
                intent.putExtra("isWalletTransfer", isWalletTransfer);
                startActivityForResult(intent, REFRESH_SCREEN);
                break;
        }
    }

    public void onClickCard(int position) {
        showEnterCVV(position);
    }

    private void showEnterCVV(final int position) {
        if (dialog == null) {
            dialog = new Dialog(CardListActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.pop_enter_cvv);
            TextView txtCardNumber = dialog.findViewById(R.id.txtCardNumber);
            TextView txtCancel = dialog.findViewById(R.id.txtCancel);
            TextView txtSubmit = dialog.findViewById(R.id.txtSubmit);
            final EditText edtCvv = dialog.findViewById(R.id.edtCvv);

            txtCardNumber.setText(cardModels.get(position).getDetails().getWallet_number());
            txtCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            txtSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!edtCvv.getText().toString().isEmpty() && edtCvv.getText().toString().length() == 3) {
                        dialog.dismiss();
                        strCvv = edtCvv.getText().toString();
                        CardModel cardModel = cardModels.get(position);
                        makePayment(cardModel);
                    } else
                        edtCvv.setError("Please enter valid CVV");

                }
            });
            dialog.show();
        } else if (dialog.isShowing()) {
            dialog.dismiss();
            showEnterCVV(position);
        } else {
            dialog = null;
            showEnterCVV(position);
        }
    }

    private void makePayment(CardModel cardModel) {
        final PayCardModel model = new PayCardModel();
        model.setTransaction_id(merchantPojo.getTransactionId());
        model.setAmount(money);
        model.setCvv(strCvv);
        model.setDesc(TELLER_DESCRIPTION);
        model.setMerchant_id(merchantPojo.getMerchantId());
        model.setPass_code(merchantPojo.getPassCode());
        model.setProcessing_code("000000");
        model.setResponse_url(TELLER_CALLBACK_URL);
        model.setUser_id(merchantPojo.getUserId());
        model.setWallet_id(cardModel.getWallet_id());

        Map<String, String> map = new HashMap<>();
        map.put("Authorization", "Basic ZW5zaGlrYTpPRFkzTXpRMk5Ea3hOMlZ1YzJocGEyRlVkV1V0U25WdUlERXpMVEl3TVRjPQ==");
        map.put("Content-Type", "application/json");

        ApiCall.getApiCall(CardListActivity.this, ApiCall.apiServiceTeller.makePaymentCard(map, model),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        TransactionResultModel model1 = responseParser.getTransactionResult(objectResponse.body());
                        if (model1.getCode() == 000) {
                            saveTransaction(model1.getTransaction_id(),model1.getReason());
                        } else if (model1.getCode() == 200) {
                            Intent intent = new Intent(CardListActivity.this, WebviewActivity.class);
                            intent.putExtra("url", model1.getReason());
                            intent.putExtra("callback_url", model.getResponse_url());
                            startActivityForResult(intent, PAYMENT_WEBVIEW);
                        } else {
                            Intent intent = new Intent(CardListActivity.this, TransactionFailureActivity.class);
                            intent.putExtra("id", model.getTransaction_id());
                            if (model1.getReason() != null && !model1.getReason().isEmpty())
                                intent.putExtra("reason", model1.getReason());
                            else
                                intent.putExtra("reason", model1.getDescription());
                            intent.putExtra("payment_mode", Vars.PAYMENT_MOMO);
                            startActivityForResult(intent, PAYMENT_FAILURE);
                        }
                    }

                    @Override
                    public void onFailure(String message) {

                    }
                });
    }

    private void saveTransaction(String transaction_id, final String reason) {
        SaveTransactionModel saveTransactionModel = new SaveTransactionModel();
        saveTransactionModel.setTransactionId(transaction_id);
        saveTransactionModel.setStatus("1");
//        saveTransactionModel.setUser_id(Support.getPref(getApplicationContext(), Vars.USER_ID));
        saveTransactionModel.setPaymentMode(Vars.PAYMENT_CARD + "");
        ApiCall.getApiCall(CardListActivity.this, ApiCall.apiService.saveTransaction(saveTransactionModel),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Toast.makeText(CardListActivity.this, reason, Toast.LENGTH_SHORT).show();
                        setResult(RESULT_OK, new Intent());
                        finish();
                    }

                    @Override
                    public void onFailure(String message) {
//                        setResult(RESULT_OK, new Intent());
//                        finish();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REFRESH_SCREEN && resultCode == RESULT_OK) {
            if (isWalletTransfer) {
                CardModel model = (CardModel) data.getSerializableExtra("detail");
                strCvv = data.getStringExtra("cvv");
                makePayment(model);
            } else {
                prepareScreen();
            }
        } else if (requestCode == PAYMENT_FAILURE && resultCode == RESULT_CANCELED) {
            setResult(RESULT_CANCELED, new Intent());
            finish();
        } else if (requestCode == PAYMENT_WEBVIEW && resultCode == RESULT_OK) {
            setResult(RESULT_OK, new Intent());
            finish();
        }
    }
}
