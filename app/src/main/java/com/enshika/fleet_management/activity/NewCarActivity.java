package com.enshika.fleet_management.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.CarDetailPojo;
import com.enshika.fleet_management.model.CarIdModel;
import com.enshika.fleet_management.model.CarTypePojo;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.RuntimePermissionsActivity;
import com.enshika.fleet_management.utils.Support;
import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;
import com.theartofdev.edmodo.cropper.CropImage;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.Vars.PERISSION_READ_WRITE;

/**
 * Created by Vino on 29-03-2018.
 */

public class NewCarActivity extends RuntimePermissionsActivity implements View.OnClickListener {
    ArrayList<CarTypePojo> carTypePojos;
    CarDetailPojo carDetailPojo;
    ArrayList<String> carTypeList, carYearList, seatingCapacityList;
    MaterialBetterSpinner sprCarType, sprCarYear, sprSeatingCapacity;
    EditText edtCarColor, edtModelName, edtCarOwner, edtCarMake, edtCarPlateNumber;
    Toolbar toolbar;
    Button btn_continue;
    ResponseParser responseParser;
    String str_car_id = "";
    RelativeLayout relativeImgFront, relativeImgBack, relativeImgInsurance, relativeImgInspection, relativeImgVehicleCommercial,
            relativeImgVehicleRegistration;
    ImageView imgFront, imgBack, imgInsurance, imgInspection, imgVehicleCommercial, imgVehicleRegistration;
    String pick_image_type = "";
    String str_img_front = "", str_img_back = "", str_img_insurance = "", str_img_inspection = "", str_img_commercial = "",
            str_img_reg = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_car);
        initUI();
        prepareBundleDatas();
        prepareCarType();
        prepareCarYear();
        prepareSeatingCapacity();
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        if (requestCode == PERISSION_READ_WRITE) {
            pickImage();
        }
    }

    private void prepareBundleDatas() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("id")) {
                str_car_id = bundle.getString("id");
                btn_continue.setText("Update");
                setToolbar(true);
            } else {
                setToolbar(false);
            }
        } else
            setToolbar(false);
    }

    private void prepareSeatingCapacity() {
        seatingCapacityList = new ArrayList<>();
        for (int i = 2; i < 8; i++) {
            seatingCapacityList.add(i + "");
        }
        setSpinnerAdapter(sprSeatingCapacity, seatingCapacityList);
    }

    private void prepareCarYear() {
        carYearList = new ArrayList<>();
        for (int i = Calendar.getInstance().get(Calendar.YEAR); i >= 1980; i--) {
            carYearList.add(i + "");
        }
        setSpinnerAdapter(sprCarYear, carYearList);
    }

    @SuppressLint("SetTextI18n")
    private void prepareCarType() {

        carTypeList = new ArrayList<>();
        carTypeList.add("Select");
        setSpinnerAdapter(sprCarType, carTypeList);

        ApiCall.getApiCall(this,
                apiService.getCarType(Support.getHeader(this)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        carTypePojos = responseParser.getCarType(objectResponse.body());

                        if (!str_car_id.isEmpty()) {

                            ApiCall.getApiCall(NewCarActivity.this,
                                    apiService.getCarDetail(str_car_id, Support.getHeader(NewCarActivity.this)),
                                    true, new ApiCall.OnApiResult() {
                                        @Override
                                        public void onSuccess(Response<Object> objectResponse) {
                                            Log.e("onSuccess: ", objectResponse.body() + "");

                                            carDetailPojo = responseParser.getCarDetail(objectResponse.body());

                                            for (int i = 0; i < carTypePojos.size(); i++) {
                                                if (carTypePojos.get(i).getCarTypeId().equals(carDetailPojo.getCarTypeId())) {
                                                    sprCarType.setText(carTypePojos.get(i).getCarType());
                                                }
                                            }

                                            setCarTypeSpinnerAdapter(sprCarType, carTypePojos);

                                            edtCarColor.setText(carDetailPojo.getCarColor());
                                            edtModelName.setText(carDetailPojo.getModelName());
                                            edtCarOwner.setText(carDetailPojo.getOwner());
                                            edtCarMake.setText(carDetailPojo.getMake());
                                            edtCarPlateNumber.setText(carDetailPojo.getCarPlateNo());
                                            sprCarYear.setText(carDetailPojo.getCarYear() + "");
                                            sprSeatingCapacity.setText(carDetailPojo.getNoOfPassenger() + "");
                                            Support.loadImage(carDetailPojo.getFrontImgUrl(), imgFront, R.drawable.ic_placeholder_gallery);
                                            Support.loadImage(carDetailPojo.getBackImgUrl(), imgBack, R.drawable.ic_placeholder_gallery);
                                            Support.loadImage(carDetailPojo.getInsurancePhotoUrl(), imgInsurance, R.drawable.ic_placeholder_gallery);
                                            Support.loadImage(carDetailPojo.getInspectionReportPhotoUrl(), imgInspection, R.drawable.ic_placeholder_gallery);
                                            Support.loadImage(carDetailPojo.getVehicleCommercialLicencePhotoUrl(), imgVehicleCommercial, R.drawable.ic_placeholder_gallery);
                                            Support.loadImage(carDetailPojo.getRegistrationPhotoUrl(), imgVehicleRegistration, R.drawable.ic_placeholder_gallery);
                                        }

                                        @Override
                                        public void onFailure(String message) {
                                            Log.e("onSuccess: ", message);
                                            Support.displayToastMessage(NewCarActivity.this, message);
                                        }
                                    });
                        } else {
                            setCarTypeSpinnerAdapter(sprCarType, carTypePojos);
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Log.e("onSuccess: ", message);
                        Support.displayToastMessage(NewCarActivity.this, message);
                    }
                });
    }

    private void setSpinnerAdapter(MaterialBetterSpinner spinner, ArrayList<String> arrayList) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_dropdown_item, arrayList);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
    }

    private void setCarTypeSpinnerAdapter(MaterialBetterSpinner spinner, ArrayList<CarTypePojo> arrayList) {
        ArrayAdapter<CarTypePojo> arrayAdapter = new ArrayAdapter<>(this,
                R.layout.spinner_dropdown_item, arrayList);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
    }

    private void initUI() {

        relativeImgFront = findViewById(R.id.relativeImgFront);
        relativeImgBack = findViewById(R.id.relativeImgBack);
        relativeImgInsurance = findViewById(R.id.relativeImgInsurance);
        relativeImgInspection = findViewById(R.id.relativeImgInspection);
        relativeImgVehicleCommercial = findViewById(R.id.relativeImgVehicleCommercial);
        relativeImgVehicleRegistration = findViewById(R.id.relativeImgVehicleRegistration);
        imgFront = findViewById(R.id.imgFront);
        imgBack = findViewById(R.id.imgBack);
        imgInsurance = findViewById(R.id.imgInsurance);
        imgInspection = findViewById(R.id.imgInspection);
        imgVehicleCommercial = findViewById(R.id.imgVehicleCommercial);
        imgVehicleRegistration = findViewById(R.id.imgVehicleRegistration);

        edtCarColor = findViewById(R.id.edtCarColor);
        edtModelName = findViewById(R.id.edtModelName);
        edtCarOwner = findViewById(R.id.edtCarOwner);
        edtCarMake = findViewById(R.id.edtCarMake);
        edtCarPlateNumber = findViewById(R.id.edtCarPlateNumber);
        btn_continue = findViewById(R.id.btn_continue);
        toolbar = findViewById(R.id.toolbar);
        sprCarType = findViewById(R.id.sprCarType);
        sprCarYear = findViewById(R.id.sprCarYear);
        sprSeatingCapacity = findViewById(R.id.sprSeatingCapacity);

        responseParser = new ResponseParser(this);
        carTypePojos = new ArrayList<>();

        btn_continue.setOnClickListener(this);
        relativeImgFront.setOnClickListener(this);
        relativeImgBack.setOnClickListener(this);
        relativeImgInsurance.setOnClickListener(this);
        relativeImgInspection.setOnClickListener(this);
        relativeImgVehicleCommercial.setOnClickListener(this);
        relativeImgVehicleRegistration.setOnClickListener(this);
    }

    private void setToolbar(boolean isEdit) {
        if (isEdit)
            toolbar.setTitle("Update Car");
        else
            toolbar.setTitle("Add New Car");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_continue:
                if (isValidFields()) {
                    String carTypeId = "";
                    for (CarTypePojo carTypePojo : carTypePojos) {
                        if (carTypePojo.getCarType().equals(sprCarType.getText().toString())) {
                            carTypeId = carTypePojo.getCarTypeId();
                        }
                    }
                    if (str_car_id.isEmpty())
                        carDetailPojo = new CarDetailPojo();
                    carDetailPojo.setCarTypeId(carTypeId);
                    carDetailPojo.setCarColor(edtCarColor.getText().toString());
                    carDetailPojo.setModelName(edtModelName.getText().toString());
                    carDetailPojo.setOwner(edtCarOwner.getText().toString());
                    carDetailPojo.setMake(edtCarMake.getText().toString());
                    carDetailPojo.setCarPlateNo(edtCarPlateNumber.getText().toString());
                    carDetailPojo.setCarYear(Long.parseLong(sprCarYear.getText().toString()));
                    carDetailPojo.setNoOfPassenger(Long.parseLong(sprSeatingCapacity.getText().toString()));
                    if (str_car_id.isEmpty())
                        updateCar(true);
                    else
                        updateCar(false);
                }
                break;

            case R.id.relativeImgFront:
                pick_image_type = "front";
                pickImageWithPermission();
                break;
            case R.id.relativeImgBack:
                pick_image_type = "back";
                pickImageWithPermission();
                break;
            case R.id.relativeImgInsurance:
                pick_image_type = "insurance";
                pickImageWithPermission();
                break;
            case R.id.relativeImgInspection:
                pick_image_type = "inspection";
                pickImageWithPermission();
                break;
            case R.id.relativeImgVehicleCommercial:
                pick_image_type = "commercial";
                pickImageWithPermission();
                break;
            case R.id.relativeImgVehicleRegistration:
                pick_image_type = "reg";
                pickImageWithPermission();
                break;
        }
    }

    private void updateCar(boolean isNew) {
        if (isNew)
            ApiCall.getApiCall(NewCarActivity.this,
                    apiService.addCar(Support.getHeader(NewCarActivity.this), carDetailPojo),
                    true, new ApiCall.OnApiResult() {
                        @Override
                        public void onSuccess(Response<Object> objectResponse) {
                            Log.e("onSuccess: ", objectResponse.body() + "");
                            CarIdModel carIdModel = responseParser.getCarIdModel(objectResponse.body());
                            if (carIdModel.getSuccess()) {
                                str_car_id = carIdModel.getCarId();
                                LinkedHashMap<String, String> images = new LinkedHashMap<>();
                                images.put("frontImgUrl", str_img_front);
                                images.put("backImgUrl", str_img_back);
                                images.put("inspectionReportPhotoUrl", str_img_inspection);
                                images.put("insurancePhotoUrl", str_img_inspection);
                                images.put("vehicleCommercialLicencePhotoUrl", str_img_commercial);
                                images.put("registrationPhotoUrl", str_img_reg);
                                uploadImage(true, images, 0);
                            } else
                                Support.displayToastMessage(NewCarActivity.this, responseParser.getErrorMessage(objectResponse.body().toString()));
                        }

                        @Override
                        public void onFailure(String message) {
                            Support.displayToastMessage(NewCarActivity.this, message);
                        }
                    });
        else
            ApiCall.getApiCall(NewCarActivity.this,
                    apiService.updateCar(Support.getHeader(NewCarActivity.this), carDetailPojo),
                    true, new ApiCall.OnApiResult() {
                        @Override
                        public void onSuccess(Response<Object> objectResponse) {
                            Log.e("onSuccess: ", objectResponse.body() + "");
                            String status = responseParser.getStatus(objectResponse.body().toString());
                            if (status.equals("SUCCESS")) {
                                LinkedHashMap<String, String> images = new LinkedHashMap<>();
                                if (!str_img_front.isEmpty())
                                    images.put("frontImgUrl", str_img_front);
                                if (!str_img_back.isEmpty())
                                    images.put("backImgUrl", str_img_back);
                                if (!str_img_insurance.isEmpty())
                                    images.put("inspectionReportPhotoUrl", str_img_insurance);
                                if (!str_img_inspection.isEmpty())
                                    images.put("insurancePhotoUrl", str_img_inspection);
                                if (!str_img_commercial.isEmpty())
                                    images.put("vehicleCommercialLicencePhotoUrl", str_img_commercial);
                                if (!str_img_reg.isEmpty())
                                    images.put("registrationPhotoUrl", str_img_reg);
                                if (!images.isEmpty()) {
                                    uploadImage(false, images, 0);
                                } else {
                                    Support.displayToastMessage(NewCarActivity.this, "Car Updated!");
                                    setResult(RESULT_OK, new Intent());
                                    finish();
                                }
                            } else
                                Support.displayToastMessage(NewCarActivity.this, responseParser.getErrorMessage(objectResponse.body().toString()));
                        }

                        @Override
                        public void onFailure(String message) {
                            Support.displayToastMessage(NewCarActivity.this, message);
                        }
                    });
    }

    private void pickImageWithPermission() {
        super.requestAppPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERISSION_READ_WRITE);
    }

    private boolean isValidFields() {
        boolean isValid = true;
        if (sprCarType.getText().toString().isEmpty() || sprCarType.getText().toString().equals("Select")) {
            sprCarType.setError("Please select car type");
            sprCarType.requestFocus();
            isValid = false;
        }
        if (edtCarColor.getText().toString().isEmpty()) {
            edtCarColor.setError("Please enter car color");
            edtCarColor.requestFocus();
            isValid = false;
        }
        if (edtModelName.getText().toString().isEmpty()) {
            edtModelName.setError("Please enter model name");
            edtModelName.requestFocus();
            isValid = false;
        }
        if (edtCarOwner.getText().toString().isEmpty()) {
            edtCarOwner.setError("Please enter car owner name");
            edtCarOwner.requestFocus();
            isValid = false;
        }
        if (edtCarMake.getText().toString().isEmpty()) {
            edtCarMake.setError("Please enter car make");
            edtCarMake.requestFocus();
            isValid = false;
        }
        if (edtCarPlateNumber.getText().toString().isEmpty()) {
            edtCarPlateNumber.setError("Please enter car plate number");
            edtCarPlateNumber.requestFocus();
            isValid = false;
        }
        if (sprCarYear.getText().toString().isEmpty()) {
            sprCarYear.setError("Please select car year");
            sprCarYear.requestFocus();
            isValid = false;
        }
        if (sprSeatingCapacity.getText().toString().isEmpty()) {
            sprSeatingCapacity.setError("Please select car capacity");
            sprSeatingCapacity.requestFocus();
            isValid = false;
        }

        if (str_car_id.isEmpty()) {
            if (str_img_front.isEmpty()) {
                Support.displayToastMessage(this, "Please upload car front image.");
                isValid = false;
            }
            if (str_img_back.isEmpty()) {
                Support.displayToastMessage(this, "Please upload car back image.");
                isValid = false;
            }
            if (str_img_insurance.isEmpty()) {
                Support.displayToastMessage(this, "Please upload insurance image.");
                isValid = false;
            }
            if (str_img_inspection.isEmpty()) {
                Support.displayToastMessage(this, "Please upload inspection image.");
                isValid = false;
            }
            if (str_img_commercial.isEmpty()) {
                Support.displayToastMessage(this, "Please upload vehicle license image.");
                isValid = false;
            }
            if (str_img_reg.isEmpty()) {
                Support.displayToastMessage(this, "Please upload vehicle registration image.");
                isValid = false;
            }
        }
        return isValid;
    }


    private void pickImage() {
        ImagePicker.with(this)                         //  Initialize ImagePicker with activity or fragment context
                .setToolbarColor("#fd6600")         //  Toolbar color
                .setStatusBarColor("#fd6600")       //  StatusBar color (works with SDK >= 21  )
                .setToolbarTextColor("#FFFFFF")     //  Toolbar text color (Title and Done button)
                .setToolbarIconColor("#FFFFFF")     //  Toolbar icon color (Back and Camera button)
                .setProgressBarColor("#ed1b25")     //  ProgressBar color
                .setBackgroundColor("#212121")      //  Background color
                .setCameraOnly(false)               //  Camera mode
                .setMultipleMode(false)              //  Select multiple images or single image
                .setFolderMode(true)                //  Folder mode
                .setShowCamera(true)                //  Show camera button
                .setFolderTitle("Albums")           //  Folder title (works with FolderMode = true)
                .setImageTitle("Galleries")         //  Image title (works with FolderMode = false)
                .setDoneTitle("Done")               //  Done button title
//                .setLimitMessage("You have reached selection limit")    // Selection limit message
//                .setMaxSize(1)                     //  Max images can be selected
                .setSavePath("Enshika Fleet")         //  Image capture folder name
//                .setSelectedImages(images)          //  Selected images
                .setAlwaysShowDoneButton(true)      //  Set always show done button in multiple mode
                .setKeepScreenOn(true)              //  Keep screen on when selecting images
                .start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
            Log.e("onActivityResult: ", images.get(0).getPath());
            CropImage.activity(Uri.fromFile(new File(images.get(0).getPath())))
                    .start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Support.refreshMediaScanner(this, resultUri);
                switch (pick_image_type) {
                    case "front":
                        str_img_front = resultUri.getPath();
                        Support.loadImage(resultUri, imgFront);
                        break;
                    case "back":
                        str_img_back = resultUri.getPath();
                        Support.loadImage(resultUri, imgBack);
                        break;
                    case "insurance":
                        str_img_insurance = resultUri.getPath();
                        Support.loadImage(resultUri, imgInsurance);
                        break;
                    case "inspection":
                        str_img_inspection = resultUri.getPath();
                        Support.loadImage(resultUri, imgInspection);
                        break;
                    case "commercial":
                        str_img_commercial = resultUri.getPath();
                        Support.loadImage(resultUri, imgVehicleCommercial);
                        break;
                    case "reg":
                        str_img_reg = resultUri.getPath();
                        Support.loadImage(resultUri, imgVehicleRegistration);
                        break;
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void uploadImage(final boolean isNew, final LinkedHashMap<String, String> images, final int position) {
        String path = (new ArrayList<String>(images.values())).get(position);
        final String imageName = (new ArrayList<String>(images.keySet())).get(position);
        File file = new File(path);
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData(imageName, file.getName(), reqFile);
//        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), imageName);
        ApiCall.getApiCall(this, apiService.uploadCarImages(Support.getHeader(getApplicationContext()), str_car_id, body/*, name*/), true, new ApiCall.OnApiResult() {
            @Override
            public void onSuccess(Response<Object> objectResponse) {
                if (position + 1 < images.size()) {
                    uploadImage(isNew, images, position + 1);
                } else {
                    if (isNew) {
                        Support.displayToastMessage(NewCarActivity.this, "Car Added!");
                        setResult(RESULT_OK, new Intent());
                        finish();
                    } else {
                        Support.displayToastMessage(NewCarActivity.this, "Car Updated!");
                        setResult(RESULT_OK, new Intent());
                        finish();
                    }
                }

            }

            @Override
            public void onFailure(String message) {
                Support.displayToastMessage(NewCarActivity.this, message);
            }
        });
    }
}
