package com.enshika.fleet_management.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;

/**
 * Created by Shamla Tech on 26-07-2018.
 */

public class WebviewActivity extends AppCompatActivity {
    Toolbar toolbar;
    WebView webView;
    String strUrl = "";
    String strCallbackUrl = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        initUI();
        getBundleDetails();

    }

    private void getBundleDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("url"))
                strUrl = bundle.getString("url");
            if (bundle.containsKey("callback_url"))
                strCallbackUrl = bundle.getString("callback_url");
            webView.loadUrl(strUrl);
        }
    }

    private void initUI() {
        webView = findViewById(R.id.webView);
        webView.setWebViewClient(new myWebClient());
        webView.getSettings().setJavaScriptEnabled(true);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Payment");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
            Support.showLoader(WebviewActivity.this);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Support.hideLoader(WebviewActivity.this);
            if (url.contains(strCallbackUrl)) {
                setResult(RESULT_OK, new Intent());
                finish();
            }
        }
    }
}
