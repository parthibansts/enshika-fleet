package com.enshika.fleet_management.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.fragment.BookingFragment;
import com.enshika.fleet_management.fragment.DashboardFragment;
import com.enshika.fleet_management.fragment.DriverLocationsFragment;
import com.enshika.fleet_management.fragment.DriversCarsFragment;
import com.enshika.fleet_management.fragment.FleetFeeFragment;
import com.enshika.fleet_management.fragment.SettingsFragment;
import com.enshika.fleet_management.fragment.WalletFragment;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.SearchViewInterface;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;

import static com.enshika.fleet_management.utils.Vars.REFRESH_SCREEN;

public class MenuPageActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    DrawerLayout drawer;
    CircleImageView ciFleet;
    TextView txtFleetName, txtFleetEmail, txtFleetFee;
    MaterialSearchView searchView;
    MenuItem searchMenu;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_page);
        initUI();
    }

    private void initUI() {
        drawer = findViewById(R.id.drawer_layout);
        searchView = findViewById(R.id.search_view);
        Toolbar toolbar = findViewById(R.id.menu_toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        View navHeader = navigationView.getHeaderView(0);
        ciFleet = navHeader.findViewById(R.id.ciFleet);
        txtFleetName = navHeader.findViewById(R.id.txtFleetName);
        txtFleetEmail = navHeader.findViewById(R.id.txtFleetEmail);
        ciFleet.setOnClickListener(this);

        Menu menu = navigationView.getMenu();
        MenuItem menuItem = menu.findItem(R.id.nav_fleet_fee);
        View actionView = MenuItemCompat.getActionView(menuItem);
        txtFleetFee = actionView.findViewById(R.id.txtFleetFee);

        prepareNavigationData();
        getInsufficientFleetFeeCount();

        navigationView.getMenu().getItem(0).setChecked(true);
        drawer.closeDrawer(GravityCompat.START);
        Support.loadFragment(MenuPageActivity.this, new DashboardFragment());
    }

    public void getSearchViewAction(final SearchViewInterface searchViewInterface) {
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewInterface.onSearch(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                searchViewInterface.onTextChanged(query);
                return true;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                searchViewInterface.searchViewOpened();
            }

            @Override
            public void onSearchViewClosed() {
                searchViewInterface.searchViewClosed();
            }
        });
    }

    public void getInsufficientFleetFeeCount() {
        ApiCall.getApiCall(MenuPageActivity.this, ApiCall.apiService.getInsufficientFleetFeeCount
                (Support.getHeader(getApplicationContext())), true, new ApiCall.OnApiResult() {
            @Override
            public void onSuccess(Response<Object> objectResponse) {
                try {
                    JSONObject jsonObject = new JSONObject(objectResponse.body().toString());
                    Support.setPref(getApplicationContext(), Vars.INSUFFICIENT_FLEET_FEE, jsonObject.getLong("driverCount") + "");
                    prepareFeeCount(Support.getPref(getApplicationContext(), Vars.INSUFFICIENT_FLEET_FEE));
                } catch (Exception e) {
                    e.printStackTrace();
                    Support.setPref(getApplicationContext(), Vars.INSUFFICIENT_FLEET_FEE, "");
                    prepareFeeCount(Support.getPref(getApplicationContext(), Vars.INSUFFICIENT_FLEET_FEE));
                }
            }

            @Override
            public void onFailure(String message) {
                Support.setPref(getApplicationContext(), Vars.INSUFFICIENT_FLEET_FEE, "");
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (searchView != null && searchView.isSearchOpen())
                searchView.closeSearch();
            else {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }
                doubleBackToExitPressedOnce = true;
                Toast.makeText(this, getResources().getString(R.string.back_press_hint), Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        Fragment fragment = null;
        if (id == R.id.nav_dashboard) {
            fragment = new DashboardFragment();
            showSearchView(false);
        } else if (id == R.id.nav_fleet) {
            fragment = new DriversCarsFragment();
            showSearchView(false);
        } else if (id == R.id.nav_current_bookings) {
            fragment = new BookingFragment(true);
            showSearchView(false);
        } else if (id == R.id.nav_bookings) {
            fragment = new BookingFragment(false);
            showSearchView(false);
        } else if (id == R.id.nav_active_drivers) {
            fragment = new DriverLocationsFragment();
            showSearchView(false);
        } else if (id == R.id.nav_settings) {
            fragment = new SettingsFragment();
            showSearchView(false);
        } else if (id == R.id.nav_wallet) {
            fragment = new WalletFragment();
            showSearchView(false);
        } else if (id == R.id.nav_fleet_fee) {
            fragment = new FleetFeeFragment();
            showSearchView(true);
        } else if (id == R.id.nav_logout) {
            Support.showAlertWithNegativeButton(this, "Are you sure do you want to logout?", true, "Logout");
        }
        if (fragment != null) {
            FragmentTransaction referNEarnFragmentTransaction1 = getSupportFragmentManager().beginTransaction();
            referNEarnFragmentTransaction1.replace(R.id.frame, fragment);
            referNEarnFragmentTransaction1.commit();
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ciFleet:
                startActivityForResult(new Intent(this, ProfileActivity.class), REFRESH_SCREEN);
                drawer.closeDrawer(GravityCompat.START);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REFRESH_SCREEN && resultCode == RESULT_OK) {
            prepareNavigationData();
        }
    }

    public void prepareNavigationData() {
        Support.loadImage(Support.getPref(getApplicationContext(), Vars.PHOTO_URL), ciFleet, R.drawable.ic_user_placeholder_white);
        txtFleetName.setText(Support.getPref(getApplicationContext(), Vars.FIRST_NAME) + " " + Support.getPref(getApplicationContext(), Vars.LAST_NAME));
        txtFleetEmail.setText(Support.getPref(getApplicationContext(), Vars.EMAIL));
        prepareFeeCount(Support.getPref(getApplicationContext(), Vars.INSUFFICIENT_FLEET_FEE));
    }

    public void prepareFeeCount(String count) {
        if (count.equals("") || count.equals("0")) {
            txtFleetFee.setVisibility(View.GONE);
        } else {
            txtFleetFee.setVisibility(View.VISIBLE);
        }
        txtFleetFee.setText(count);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_page, menu);

        searchMenu = menu.findItem(R.id.action_search);
        searchView.setMenuItem(searchMenu);

        return true;
    }

    public void showSearchView(boolean isShow) {
        if (searchMenu != null)
            if (isShow) {
                searchMenu.setVisible(true);
            } else
                searchMenu.setVisible(false);
    }
}
