package com.enshika.fleet_management.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.adapter.MobileMoneyAdapter;
import com.enshika.fleet_management.model.MerchantPojo;
import com.enshika.fleet_management.model.MobileMoneyOperators;
import com.enshika.fleet_management.model.NewMobileMoneyModel;
import com.enshika.fleet_management.model.PayMobModel;
import com.enshika.fleet_management.model.PaymentResponseModel;
import com.enshika.fleet_management.model.SaveTransactionModel;
import com.enshika.fleet_management.model.TransactionResultModel;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.ApiCall.apiServiceTeller;
import static com.enshika.fleet_management.utils.Vars.PAYMENT_FAILURE;
import static com.enshika.fleet_management.utils.Vars.PAYMENT_TRANSFER;
import static com.enshika.fleet_management.utils.Vars.REFRESH_SCREEN;

/**
 * Created by Shamla Tech on 06-07-2018.
 */

public class MobileMoneyActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txtAddNew, txtUpdate;
    Toolbar toolbar;
    ArrayList<MobileMoneyOperators> mobileMoneyModels;
    MobileMoneyAdapter mobileMoneyAdapter;
    String selectedMobileNumber = "";
    RecyclerView recyclerMobileMoney;
    boolean isWalletTransfer;
    MerchantPojo merchantPojo;
    ResponseParser responseParser;
    String money = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_money);
        initUI();
        getBundleDetails();
        prepareMobileMoneyList();
    }

    private void getBundleDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("isWalletTransfer"))
                isWalletTransfer = true;
            if (bundle.containsKey("merchantPojo"))
                merchantPojo = (MerchantPojo) bundle.getSerializable("merchantPojo");
            if (bundle.containsKey("money"))
                money = bundle.getString("money");
        }
    }

    private void prepareRecyclerMobileMoney() {
        mobileMoneyAdapter = new MobileMoneyAdapter(this, mobileMoneyModels, selectedMobileNumber);
        recyclerMobileMoney.setLayoutManager(new LinearLayoutManager(this));
        recyclerMobileMoney.setAdapter(mobileMoneyAdapter);
        if (!mobileMoneyModels.isEmpty()) {
            if (isWalletTransfer) {
                txtUpdate.setVisibility(View.VISIBLE);
                if (!selectedMobileNumber.isEmpty())
                    txtUpdate.performClick();
            } else {
                enableSwipe();
            }
        } else {
            txtUpdate.setVisibility(View.GONE);
        }
    }

    private void prepareMobileMoneyList() {
        ApiCall.getApiCall(MobileMoneyActivity.this,
                apiService.getMobileMoney(Support.getHeader(MobileMoneyActivity.this),
                        Support.getPref(getApplicationContext(), Vars.USER_ID)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        mobileMoneyModels = new ArrayList<>();
                        PaymentResponseModel model = responseParser.getPaymentModel(objectResponse.body());
                        if (model.getStatus() == 1) {
                            mobileMoneyModels = model.getData().getMobileOperator();
                            prepareRecyclerMobileMoney();
                        } else {
                            txtUpdate.setVisibility(View.GONE);
//                            Support.displayToastMessage(MobileMoneyActivity.this, model.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(MobileMoneyActivity.this, message);
                    }
                });
    }

    private void enableSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
                    removeMobileMoney(position);
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;
                    Paint p = new Paint();
                    if (dX > 0) {
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_white);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    } else {
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_white);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerMobileMoney);
    }

    private void removeMobileMoney(int position) {
        final MobileMoneyOperators operators = mobileMoneyModels.get(position);
        mobileMoneyModels.remove(position);
        mobileMoneyAdapter.notifyDataSetChanged();
        NewMobileMoneyModel model = new NewMobileMoneyModel();
        model.setUserId(Support.getPref(getApplicationContext(), Vars.USER_ID));
        model.setMobileOperatorId(operators.getOperatorId() + "");
        model.setMobileNo(operators.getMobileNumber() + "");
        ApiCall.getApiCall(MobileMoneyActivity.this,
                apiService.removeMobileMoney(Support.getHeader(getApplicationContext()), model), false,
                new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        PaymentResponseModel paymentResponseModel = responseParser.getPaymentModel(objectResponse.body());
                        if (paymentResponseModel.getStatus() == 0) {
                            Toast.makeText(MobileMoneyActivity.this, paymentResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                            mobileMoneyModels.add(operators);
                            mobileMoneyAdapter.notifyDataSetChanged();
                        } else
                            Toast.makeText(MobileMoneyActivity.this, paymentResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(String message) {
                        Toast.makeText(MobileMoneyActivity.this, message, Toast.LENGTH_SHORT).show();
                        mobileMoneyModels.add(operators);
                        mobileMoneyAdapter.notifyDataSetChanged();
                    }
                });

    }

    private void initUI() {
        recyclerMobileMoney = findViewById(R.id.recyclerMobileMoney);
        txtUpdate = findViewById(R.id.txtUpdate);
        txtAddNew = findViewById(R.id.txtAddNew);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Mobile Money");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        responseParser = new ResponseParser(this);
        merchantPojo = new MerchantPojo();
        mobileMoneyModels = new ArrayList<>();
        txtUpdate.setOnClickListener(this);
        txtAddNew.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtUpdate:
                if (!selectedMobileNumber.isEmpty()) {
                    makeTransaction();
                } else {
                    Toast.makeText(this, "Please select mobile money!", Toast.LENGTH_SHORT).show();
                }
//                Intent intent = new Intent(this, TransferPinActivity.class);
//                intent.putExtra("merchantPojo", merchantPojo);
//                startActivityForResult(intent, PAYMENT_TRANSFER);
                break;
            case R.id.txtAddNew:
                startActivityForResult(new Intent(this, AddMobileMoneyActivity.class), REFRESH_SCREEN);
                break;
        }
    }

    private void makeTransaction() {
        MobileMoneyOperators selectedOperator = null;
        for (int i = 0; i < mobileMoneyModels.size(); i++) {
            MobileMoneyOperators mobileMoneyOperators = mobileMoneyModels.get(i);
            if ((mobileMoneyOperators.getMobileNumber() + "").equals(selectedMobileNumber)) {
                selectedOperator = mobileMoneyOperators;
            }
        }
        if (selectedOperator != null) {
            final PayMobModel model = new PayMobModel();
            model.setAmount(money);
            model.setDesc("fleet test");
            model.setMerchant_id(merchantPojo.getMerchantId());
            model.setOperator(selectedOperator.getrSwitch());
            model.setProcessing_code("000200");
            model.setSubscriber_number("0" + selectedMobileNumber);
            model.setTransaction_id(merchantPojo.getTransactionId());

            Map<String, String> map = new HashMap<>();
            map.put("Authorization", "Basic ZW5zaGlrYTpPRFkzTXpRMk5Ea3hOMlZ1YzJocGEyRlVkV1V0U25WdUlERXpMVEl3TVRjPQ==");
            map.put("Content-Type", "application/json");

            ApiCall.getApiCall(MobileMoneyActivity.this, apiServiceTeller.makePaymentMoMo(map, model), true,
                    new ApiCall.OnApiResult() {
                        @Override
                        public void onSuccess(Response<Object> objectResponse) {
                            TransactionResultModel model1 = responseParser.getTransactionResult(objectResponse.body());
                            if (model1.getCode() == 000) {
                                saveTransaction(model1.getTransaction_id(), model1.getReason());
                            } else {
                                Intent intent = new Intent(MobileMoneyActivity.this, TransactionFailureActivity.class);
                                intent.putExtra("id", model.getTransaction_id());
                                if (model1.getReason() != null && !model1.getReason().isEmpty())
                                    intent.putExtra("reason", model1.getReason());
                                else
                                    intent.putExtra("reason", model1.getDescription());
                                intent.putExtra("payment_mode", Vars.PAYMENT_MOMO);
                                startActivityForResult(intent, PAYMENT_FAILURE);
                            }
                        }

                        @Override
                        public void onFailure(String message) {

                        }
                    });
        }
    }

    private void saveTransaction(String transaction_id, final String reason) {
        SaveTransactionModel saveTransactionModel = new SaveTransactionModel();
        saveTransactionModel.setTransactionId(transaction_id);
        saveTransactionModel.setStatus("1");
//        saveTransactionModel.setUser_id(Support.getPref(getApplicationContext(), Vars.USER_ID));
        saveTransactionModel.setPaymentMode(Vars.PAYMENT_MOMO + "");
        ApiCall.getApiCall(MobileMoneyActivity.this, ApiCall.apiService.saveTransaction(saveTransactionModel),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Toast.makeText(MobileMoneyActivity.this, reason, Toast.LENGTH_SHORT).show();
                        setResult(RESULT_OK, new Intent());
                        finish();
                    }

                    @Override
                    public void onFailure(String message) {
//                        setResult(RESULT_OK, new Intent());
//                        finish();
                    }
                });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REFRESH_SCREEN && resultCode == RESULT_OK) {
            if (isWalletTransfer)
                selectedMobileNumber = data.getStringExtra("mobile");
            prepareMobileMoneyList();
        } else if (requestCode == PAYMENT_TRANSFER && resultCode == RESULT_OK) {
            setResult(RESULT_OK, new Intent());
            finish();
        } else if (requestCode == PAYMENT_FAILURE && resultCode == RESULT_CANCELED) {
            setResult(RESULT_CANCELED, new Intent());
            finish();
        }
    }

    public void onMobileMoneyClick(int position) {
        if (isWalletTransfer) {
            selectedMobileNumber = mobileMoneyModels.get(position).getMobileNumber() + "";
            mobileMoneyAdapter.updateSelection(selectedMobileNumber);
        }
    }
}
