package com.enshika.fleet_management.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.enshika.fleet_management.R;
import com.enshika.fleet_management.adapter.MultiSelectionAdapter;
import com.enshika.fleet_management.model.CarsPojo;
import com.enshika.fleet_management.model.DriverDetailModel;
import com.enshika.fleet_management.model.DriverIdModel;
import com.enshika.fleet_management.model.NewDriverModel;
import com.enshika.fleet_management.model.RegionPojo;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.RuntimePermissionsActivity;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;
import com.hbb20.CountryCodePicker;
import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;
import com.theartofdev.edmodo.cropper.CropImage;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.Support.formatDateIntoMMddyyyy;
import static com.enshika.fleet_management.utils.Support.getHeader;
import static com.enshika.fleet_management.utils.Vars.PERISSION_READ_WRITE;

/**
 * Created by Vino on 29-03-2018.
 */

public class NewDriverActivity extends RuntimePermissionsActivity implements View.OnClickListener {
    Button btn_continue;
    MultiSelectionAdapter multi_selection_adapter;
    ArrayList<RegionPojo> regionPojos;
    ArrayList<CarsPojo> carModels;
    ArrayList<String> selectedRegionIds;
    Dialog pick_Dialog;
    Toolbar toolbar;
    boolean isEdit;
    String strDriverId = "";
    String strStartDate = "", strEndDate = "";
    Date startDate, endDate;
    String selected_car_id = "";
    String pick_image_type = "";
    CountryCodePicker ccp_country_id;
    MaterialBetterSpinner sprCar;
    EditText edtFirstName, edtLastName, edtEmail, edtDateOfBirth, edtPhoneNumber, edtId, edtRegion,
            edtStreetOne, edtStreetTwo, edtCountry, edtState, edtCity, edtBankName, edtAccountNo, edtAccountName,
            edtBankCode, edtAccountType, edtDrivingLicense, edtInsuranceDateRange, edtLicenseExpiry;
    RadioButton radioMale, radioFemale;
    ImageView imgProfile, imgPoliceClearance, imgPassport, imgLicenseBack, imgLicenseFront, imgId;
    String str_img_profile = "", str_img_police_clearance = "", str_img_passport = "", str_img_license_back = "",
            str_img_license_front = "", str_img_id = "";
    DriverDetailModel driverDetailModel;
    ResponseParser responseParser;
    ArrayList<String> carTypeList;
    private android.app.DatePickerDialog.OnDateSetListener dateSetListener =
            new android.app.DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int month, int day) {

                }
            };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_driver);
        initUI();
        getBundleDetails();
        prepareRegion();

    }

    private void pickImageWithPermission() {
        super.requestAppPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERISSION_READ_WRITE);
    }

    private void pickImage() {
        ImagePicker.with(this)
                .setToolbarColor("#fd6600")
                .setStatusBarColor("#fd6600")
                .setToolbarTextColor("#FFFFFF")
                .setToolbarIconColor("#FFFFFF")
                .setProgressBarColor("#ed1b25")
                .setBackgroundColor("#212121")
                .setCameraOnly(false)
                .setMultipleMode(false)
                .setFolderMode(true)
                .setShowCamera(true)
                .setFolderTitle("Albums")
                .setImageTitle("Galleries")
                .setDoneTitle("Done")
//                .setLimitMessage("You have reached selection limit")
//                .setMaxSize(1)
                .setSavePath("Enshika Fleet")
//                .setSelectedImages(images)
                .setAlwaysShowDoneButton(true)
                .setKeepScreenOn(true)
                .start();
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        if (requestCode == PERISSION_READ_WRITE) {
            pickImage();
        }
    }

    private void getBundleDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("id")) {
                isEdit = true;
                strDriverId = bundle.getString("id");
                driverDetailModel = (DriverDetailModel) bundle.getSerializable("detail");
                prepareDriverDetail();
            }
        }
        setToolbarTitle(isEdit);
    }

    @SuppressLint("SetTextI18n")
    private void prepareDriverDetail() {
        try {
            if (driverDetailModel.getRegionId() != null) {
                selectedRegionIds.addAll(Arrays.asList(driverDetailModel.getRegionId().split("\\s*,\\s*")));
            }
            edtFirstName.setText(driverDetailModel.getFirstName());
            edtLastName.setText(driverDetailModel.getLastName());
            edtEmail.setText(driverDetailModel.getEmail());
            edtDateOfBirth.setText(Support.getDateFromLong(driverDetailModel.getDrivingLicenseModel().getDob(), Vars.SHORT_DATE_FORMAT));
            edtPhoneNumber.setText(driverDetailModel.getPhoneNo());
            ccp_country_id.setCountryForPhoneCode(Integer.parseInt(driverDetailModel.getPhoneNoCode()
                    .replace("+", "")));
            if (driverDetailModel.getGender() != null)
                if (driverDetailModel.getGender().equalsIgnoreCase("male"))
                    radioMale.setChecked(true);
                else
                    radioFemale.setChecked(true);
            edtId.setText(driverDetailModel.getDrivingLicenseModel().getSocialSecurityNumber());
            edtRegion.setText(driverDetailModel.getRegion());
            edtStreetOne.setText(driverDetailModel.getMailAddressLineOne());
            edtStreetTwo.setText(driverDetailModel.getMailAddressLineTwo());
            edtCountry.setText(driverDetailModel.getMailCountryId());
            edtState.setText(driverDetailModel.getMailStateId());
            edtCity.setText(driverDetailModel.getMailCityId());
            edtBankName.setText(driverDetailModel.getDriverBankDetails().getBankName());
            edtAccountNo.setText(driverDetailModel.getDriverBankDetails().getAccountNumber());
            edtAccountName.setText(driverDetailModel.getDriverBankDetails().getAccountName());
            edtBankCode.setText(driverDetailModel.getDriverBankDetails().getRoutingNumber());
            edtAccountType.setText(driverDetailModel.getDriverBankDetails().getType());

            if (driverDetailModel.getCarModel() != null) {
                sprCar.setText(driverDetailModel.getCarModel().getMake() + ", " + driverDetailModel.getCarModel().getModelName()
                        + ", " + driverDetailModel.getCarModel().getCarPlateNo() + ", "
                        + driverDetailModel.getCarModel().getCarColor()
                        + ", " + driverDetailModel.getCarModel().getOwner());
                selected_car_id = driverDetailModel.getCarModel().getCarId();
            }
            edtDrivingLicense.setText(driverDetailModel.getDrivingLicenseModel().getDriverLicenseCardNumber());
            strStartDate = Support.getDateFromLong(driverDetailModel.getDrivingLicenseModel()
                    .getInsuranceEffectiveDate(), Vars.SHORT_DATE_FORMAT);
            strEndDate = Support.getDateFromLong(driverDetailModel.getDrivingLicenseModel()
                    .getInsuranceExpirationDate(), Vars.SHORT_DATE_FORMAT);
            edtInsuranceDateRange.setText(strStartDate + " - " + strEndDate);
            edtLicenseExpiry.setText(Support.getDateFromLong(driverDetailModel.getDrivingLicenseModel().getLicenceExpirationDate()
                    , Vars.SHORT_DATE_FORMAT));

            Support.loadImage(driverDetailModel.getDrivingLicenseModel().getDrivingLicensePhotoUrl(), imgLicenseFront, R.drawable.ic_placeholder_gallery);
            Support.loadImage(driverDetailModel.getDrivingLicenseModel().getDrivingLicenseBackPhotoUrl(), imgLicenseBack, R.drawable.ic_placeholder_gallery);
            Support.loadImage(driverDetailModel.getDrivingLicenseModel().getCriminalHistoryPhotoUrl(), imgPoliceClearance, R.drawable.ic_placeholder_gallery);
            Support.loadImage(driverDetailModel.getDrivingLicenseModel().getBirthAccreditationPassportPhotoUrl(), imgPassport, R.drawable.ic_placeholder_gallery);
            Support.loadImage(driverDetailModel.getDrivingLicenseModel().getSocilaSecurityPhotoUrl(), imgId, R.drawable.ic_placeholder_gallery);
            Support.loadImage(driverDetailModel.getPhotoUrl(), imgProfile, R.drawable.ic_placeholder_gallery);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepareRegion() {
        regionPojos = new ArrayList<>();

        ApiCall.getApiCall(NewDriverActivity.this,
                apiService.getRegions(Support.getHeader(this)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        regionPojos = responseParser.getRegions(objectResponse.body());
                        prepareCarList();
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(NewDriverActivity.this, message);
                    }
                });
    }

    private void prepareCarList() {
        ApiCall.getApiCall(NewDriverActivity.this,
                apiService.getCarList(Support.getHeader(this)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        carModels = responseParser.getCarList(objectResponse.body());
                        setCarListSpinnerAdapter(sprCar, carModels);
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(NewDriverActivity.this, message);
                    }
                });
    }

    private void initUI() {
        edtDrivingLicense = findViewById(R.id.edtDrivingLicense);
        edtInsuranceDateRange = findViewById(R.id.edtInsuranceDateRange);
        edtLicenseExpiry = findViewById(R.id.edtLicenseExpiry);
        sprCar = findViewById(R.id.sprCar);
        imgProfile = findViewById(R.id.imgProfile);
        imgPoliceClearance = findViewById(R.id.imgPoliceClearance);
        imgPassport = findViewById(R.id.imgPassport);
        imgLicenseBack = findViewById(R.id.imgLicenseBack);
        imgLicenseFront = findViewById(R.id.imgLicenseFront);
        imgId = findViewById(R.id.imgId);
        edtFirstName = findViewById(R.id.edtFirstName);
        edtLastName = findViewById(R.id.edtLastName);
        edtEmail = findViewById(R.id.edtEmail);
        edtDateOfBirth = findViewById(R.id.edtDateOfBirth);
        ccp_country_id = findViewById(R.id.ccp_country_id);
        edtPhoneNumber = findViewById(R.id.edtPhoneNumber);
        radioMale = findViewById(R.id.radioMale);
        radioFemale = findViewById(R.id.radioFemale);
        edtId = findViewById(R.id.edtId);
        edtRegion = findViewById(R.id.edtRegion);
        edtStreetOne = findViewById(R.id.edtStreetOne);
        edtStreetTwo = findViewById(R.id.edtStreetTwo);
        edtCountry = findViewById(R.id.edtCountry);
        edtState = findViewById(R.id.edtState);
        edtCity = findViewById(R.id.edtCity);
        edtBankName = findViewById(R.id.edtBankName);
        edtAccountNo = findViewById(R.id.edtAccountNo);
        edtAccountName = findViewById(R.id.edtAccountName);
        edtBankCode = findViewById(R.id.edtBankCode);
        edtAccountType = findViewById(R.id.edtAccountType);

        toolbar = findViewById(R.id.toolbar);
        btn_continue = findViewById(R.id.btn_continue);

        responseParser = new ResponseParser(this);
        driverDetailModel = new DriverDetailModel();
        selectedRegionIds = new ArrayList<>();
        carModels = new ArrayList<>();
        carTypeList = new ArrayList<>();
        carTypeList.add("Select");
        setSpinnerAdapter(sprCar, carTypeList);

        edtRegion.setOnClickListener(this);
        btn_continue.setOnClickListener(this);
        edtInsuranceDateRange.setOnClickListener(this);
        edtLicenseExpiry.setOnClickListener(this);
        edtDateOfBirth.setOnClickListener(this);
        imgProfile.setOnClickListener(this);
        imgPoliceClearance.setOnClickListener(this);
        imgPassport.setOnClickListener(this);
        imgLicenseBack.setOnClickListener(this);
        imgLicenseFront.setOnClickListener(this);
        imgId.setOnClickListener(this);
    }

    private void setSpinnerAdapter(MaterialBetterSpinner spinner, ArrayList<String> arrayList) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                R.layout.spinner_dropdown_item, arrayList);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
    }

    private void setCarListSpinnerAdapter(MaterialBetterSpinner spinner, final ArrayList<CarsPojo> arrayList) {
        ArrayAdapter<CarsPojo> arrayAdapter = new ArrayAdapter<>(this,
                R.layout.spinner_dropdown_item, arrayList);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selected_car_id = arrayList.get(position).getCarId();
            }
        });
    }

    private void setToolbarTitle(boolean isEdit) {
        if (!isEdit)
            toolbar.setTitle("Add New Driver");
        else
            toolbar.setTitle("Update Driver");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_continue:
                if (isValidFields()) {
                    NewDriverModel model = new NewDriverModel();
                    model.setFirstName(edtFirstName.getText().toString());
                    model.setLastName(edtLastName.getText().toString());
                    model.setEmail(edtEmail.getText().toString());
                    model.setDob(Support.convertDateToMillisecs(edtDateOfBirth.getText().toString()));
                    model.setPhoneNoCode(ccp_country_id.getSelectedCountryCodeWithPlus());
                    model.setPhoneNo(edtPhoneNumber.getText().toString());
                    model.setSocialSecurityNumber(edtId.getText().toString());
                    model.setRegion(selectedRegionIds.toString().replace("[", "").replace("]", ""));
                    model.setCar(selected_car_id);
                    model.setDrivingLicense(edtDrivingLicense.getText().toString());
                    if (!strStartDate.isEmpty())
                        model.setInsuranceEffectiveDate(Support.convertDateToMillisecs(strStartDate));
                    if (!strEndDate.isEmpty())
                        model.setInsuranceExpirationDate(Support.convertDateToMillisecs(strEndDate));
                    if (!edtLicenseExpiry.getText().toString().isEmpty())
                        model.setLicenseExpiration(Support.convertDateToMillisecs(edtLicenseExpiry.getText().toString()));
                    model.setMailAddressLineOne(edtStreetOne.getText().toString());
                    model.setMailAddressLineTwo(edtStreetTwo.getText().toString());
                    model.setMailCountryId(edtCountry.getText().toString());
                    model.setMailStateId(edtState.getText().toString());
                    model.setMailCityId(edtCity.getText().toString());
                    model.setBankName(edtBankName.getText().toString());
                    model.setAccountNumber(edtAccountNo.getText().toString());
                    model.setAccountName(edtAccountName.getText().toString());
                    model.setRoutingNumber(edtBankCode.getText().toString());
                    model.setType(edtAccountType.getText().toString());
                    if (!isEdit) {
                        ApiCall.getApiCall(this, apiService.addDriver(getHeader(getApplicationContext()), model), true,
                                new ApiCall.OnApiResult() {
                                    @Override
                                    public void onSuccess(Response<Object> objectResponse) {
                                        DriverIdModel driverIdModel = responseParser.getDriverIdModel(objectResponse.body());
                                        if (driverIdModel.getSuccess().equals("true")) {
                                            strDriverId = driverIdModel.getDriverId();
                                            isEdit = true;
                                            LinkedHashMap<String, String> images = new LinkedHashMap<>();
                                            if (!str_img_profile.isEmpty())
                                                images.put("photoUrl", str_img_profile);
                                            if (!str_img_police_clearance.isEmpty())
                                                images.put("criminalHistoryPhotoUrl", str_img_police_clearance);
                                            if (!str_img_passport.isEmpty())
                                                images.put("birthAccreditationPassportPhotoUrl", str_img_passport);
                                            if (!str_img_license_back.isEmpty())
                                                images.put("drivingLicenseBackPhotoUrl", str_img_license_back);
                                            if (!str_img_license_front.isEmpty())
                                                images.put("drivingLicensePhotoUrl", str_img_license_front);
                                            if (!str_img_id.isEmpty())
                                                images.put("socilaSecurityPhotoUrl", str_img_id);
                                            if (!images.isEmpty()) {
                                                uploadImage(false, images, 0);
                                            } else {
                                                Support.displayToastMessage(NewDriverActivity.this, "Driver Added!");
                                                setResult(RESULT_OK, new Intent());
                                                finish();
                                            }
                                        } else
                                            Support.displayToastMessage(NewDriverActivity.this, responseParser.getErrorMessage(objectResponse.body() + ""));
                                    }

                                    @Override
                                    public void onFailure(String message) {
                                        Support.displayToastMessage(NewDriverActivity.this, message);
                                    }
                                });
                    } else {
                        model.setUserId(strDriverId);
                        ApiCall.getApiCall(this, apiService.updateDriver(getHeader(getApplicationContext()), model), true,
                                new ApiCall.OnApiResult() {
                                    @Override
                                    public void onSuccess(Response<Object> objectResponse) {
                                        String status = responseParser.getStatus(objectResponse.body() + "");
                                        if (status.equals("SUCCESS")) {
                                            LinkedHashMap<String, String> images = new LinkedHashMap<>();
                                            if (!str_img_profile.isEmpty())
                                                images.put("photoUrl", str_img_profile);
                                            if (!str_img_police_clearance.isEmpty())
                                                images.put("criminalHistoryPhotoUrl", str_img_police_clearance);
                                            if (!str_img_passport.isEmpty())
                                                images.put("birthAccreditationPassportPhotoUrl", str_img_passport);
                                            if (!str_img_license_back.isEmpty())
                                                images.put("drivingLicenseBackPhotoUrl", str_img_license_back);
                                            if (!str_img_license_front.isEmpty())
                                                images.put("drivingLicensePhotoUrl", str_img_license_front);
                                            if (!str_img_id.isEmpty())
                                                images.put("socilaSecurityPhotoUrl", str_img_id);
                                            if (!images.isEmpty()) {
                                                uploadImage(false, images, 0);
                                            } else {
                                                Support.displayToastMessage(NewDriverActivity.this, "Driver Updated!");
                                                setResult(RESULT_OK, new Intent());
                                                finish();
                                            }
                                        } else
                                            Support.displayToastMessage(NewDriverActivity.this, responseParser.getErrorMessage(objectResponse.body() + ""));
                                    }

                                    @Override
                                    public void onFailure(String message) {
                                        Support.displayToastMessage(NewDriverActivity.this, message);
                                    }
                                });
                    }
                }
                break;
            case R.id.edtRegion:
                ShowRegionPopUp(regionPojos, selectedRegionIds);
                break;
            case R.id.edtInsuranceDateRange:
                showDateRangePickerDialog();
                break;
            case R.id.edtLicenseExpiry:
                showDatePicker("license_expiry");
                break;
            case R.id.edtDateOfBirth:
                showDatePicker("dob");
                break;
            case R.id.imgProfile:
                pick_image_type = "profile";
                pickImageWithPermission();
                break;
            case R.id.imgPoliceClearance:
                pick_image_type = "PoliceClearance";
                pickImageWithPermission();
                break;
            case R.id.imgPassport:
                pick_image_type = "Passport";
                pickImageWithPermission();
                break;
            case R.id.imgLicenseBack:
                pick_image_type = "LicenseBack";
                pickImageWithPermission();
                break;
            case R.id.imgLicenseFront:
                pick_image_type = "LicenseFront";
                pickImageWithPermission();
                break;
            case R.id.imgId:
                pick_image_type = "Id";
                pickImageWithPermission();
                break;

        }
    }

    private boolean isValidFields() {
        boolean isValid = true;
        if (edtFirstName.getText().toString().isEmpty()) {
            edtFirstName.setError("Please enter first name");
            edtFirstName.requestFocus();
            isValid = false;
        }
        if (edtLastName.getText().toString().isEmpty()) {
            edtLastName.setError("Please enter last name");
            edtLastName.requestFocus();
            isValid = false;
        }
        if (edtEmail.getText().toString().isEmpty()) {
            edtEmail.setError("Please enter email");
            edtEmail.requestFocus();
            isValid = false;
        }
        if (edtDateOfBirth.getText().toString().isEmpty()) {
            edtDateOfBirth.setError("Please enter date of birth");
            edtDateOfBirth.requestFocus();
            isValid = false;
        }
        if (edtPhoneNumber.getText().toString().isEmpty()) {
            edtPhoneNumber.setError("Please enter phone number");
            edtPhoneNumber.requestFocus();
            isValid = false;
        }
        if (edtId.getText().toString().isEmpty()) {
            edtId.setError("Please enter id");
            edtId.requestFocus();
            isValid = false;
        }
        if (edtRegion.getText().toString().isEmpty()) {
            edtRegion.setError("Please select region");
            edtRegion.requestFocus();
            isValid = false;
        }
        /*if (edtDrivingLicense.getText().toString().isEmpty()) {
            edtDrivingLicense.setError("Please enter driving license no");
            edtDrivingLicense.requestFocus();
            isValid = false;
        }
        if (edtInsuranceDateRange.getText().toString().isEmpty()) {
            edtInsuranceDateRange.setError("Please select insurance date range");
            edtInsuranceDateRange.requestFocus();
            isValid = false;
        }
        if (edtLicenseExpiry.getText().toString().isEmpty()) {
            edtLicenseExpiry.setError("Please enter license expiry");
            edtLicenseExpiry.requestFocus();
            isValid = false;
        }
        if (sprCar.getText().toString().isEmpty() || sprCar.getText().toString().equals("Select")) {
            sprCar.setError("Please select car");
            sprCar.requestFocus();
            isValid = false;
        }
        if (edtStreetOne.getText().toString().isEmpty()) {
            edtStreetOne.setError("Please enter street 1");
            edtStreetOne.requestFocus();
            isValid = false;
        }
        if (edtStreetTwo.getText().toString().isEmpty()) {
            edtStreetTwo.setError("Please enter street 2");
            edtStreetTwo.requestFocus();
            isValid = false;
        }
        if (edtCountry.getText().toString().isEmpty()) {
            edtCountry.setError("Please enter country");
            edtCountry.requestFocus();
            isValid = false;
        }
        if (edtState.getText().toString().isEmpty()) {
            edtState.setError("Please enter state");
            edtState.requestFocus();
            isValid = false;
        }
        if (edtCity.getText().toString().isEmpty()) {
            edtCity.setError("Please enter city");
            edtCity.requestFocus();
            isValid = false;
        }
        if (edtBankName.getText().toString().isEmpty()) {
            edtBankName.setError("Please enter bank name");
            edtBankName.requestFocus();
            isValid = false;
        }
        if (edtAccountNo.getText().toString().isEmpty()) {
            edtAccountNo.setError("Please enter account no");
            edtAccountNo.requestFocus();
            isValid = false;
        }
        if (edtAccountName.getText().toString().isEmpty()) {
            edtAccountName.setError("Please enter account name");
            edtAccountName.requestFocus();
            isValid = false;
        }
        if (edtBankCode.getText().toString().isEmpty()) {
            edtBankCode.setError("Please enter bank code");
            edtBankCode.requestFocus();
            isValid = false;
        }
        if (edtAccountType.getText().toString().isEmpty()) {
            edtAccountType.setError("Please enter account type");
            edtAccountType.requestFocus();
            isValid = false;
        }*/

        /*if (!isEdit) {
            if (str_img_profile.isEmpty()) {
                Support.displayToastMessage(this, "Please upload profile image.");
                isValid = false;
            }
            if (str_img_police_clearance.isEmpty()) {
                Support.displayToastMessage(this, "Please upload police clearance image.");
                isValid = false;
            }
            if (str_img_passport.isEmpty()) {
                Support.displayToastMessage(this, "Please upload driver accreditation image.");
                isValid = false;
            }
            if (str_img_license_back.isEmpty()) {
                Support.displayToastMessage(this, "Please upload license back image.");
                isValid = false;
            }
            if (str_img_license_front.isEmpty()) {
                Support.displayToastMessage(this, "Please upload license front image.");
                isValid = false;
            }
            if (str_img_id.isEmpty()) {
                Support.displayToastMessage(this, "Please upload id image.");
                isValid = false;
            }
        }*/
        return isValid;
    }

    private void showDateRangePickerDialog() {
        if (strStartDate.isEmpty() || strEndDate.isEmpty()) {
            try {
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
                endDate = calendar.getTime();
                strEndDate = simpleDateFormat.format(calendar.getTime());
                startDate = calendar.getTime();
                strStartDate = simpleDateFormat.format(calendar.getTime());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(simpleDateFormat.parse(strStartDate));
                startDate = calendar.getTime();
                calendar.setTime(simpleDateFormat.parse(strEndDate));
                endDate = calendar.getTime();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Calendar startDateCalendar = Calendar.getInstance();
        startDateCalendar.setTime(startDate);

        Calendar endDateCalendar = Calendar.getInstance();
        endDateCalendar.setTime(endDate);

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
                        String tempStartDay = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        String tempEndDay = dayOfMonthEnd + "-" + (monthOfYearEnd + 1) + "-" + yearEnd;
                        Date tempStartDate = Support.stringToDateinddMYYYY(tempStartDay);
                        Date tempEndDate = Support.stringToDateinddMYYYY(tempEndDay);

                        if (tempStartDate.before(tempEndDate)) {
                            strStartDate = formatDateIntoMMddyyyy(tempStartDay);
                            strEndDate = formatDateIntoMMddyyyy(tempEndDay);
                            edtInsuranceDateRange.setText(strStartDate + " - " + strEndDate);
                        } else {
                            Toast.makeText(NewDriverActivity.this, "Invalid date selection!", Toast.LENGTH_SHORT).show();
                            showDateRangePickerDialog();
                        }

                    }
                },
                startDateCalendar.get(Calendar.YEAR),
                startDateCalendar.get(Calendar.MONTH),
                startDateCalendar.get(Calendar.DAY_OF_MONTH), endDateCalendar.get(Calendar.YEAR),
                endDateCalendar.get(Calendar.MONTH),
                endDateCalendar.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");

    }

    private void showDatePicker(final String type) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        android.app.DatePickerDialog datePickerDialog = new android.app.DatePickerDialog(NewDriverActivity.this,
                new android.app.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        if (type.equals("license_expiry"))
                            edtLicenseExpiry.setText(formatDateIntoMMddyyyy(dayOfMonth + "-" + (month + 1) + "-" + year));
                        else if (type.equals("dob"))
                            edtDateOfBirth.setText(formatDateIntoMMddyyyy(dayOfMonth + "-" + (month + 1) + "-" + year));
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void ShowRegionPopUp(final ArrayList<RegionPojo> regionPojos, final ArrayList<String> selectedItemIds) {
        if (pick_Dialog == null) {
            pick_Dialog = new Dialog(NewDriverActivity.this);
            pick_Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pick_Dialog.setCancelable(true);
            pick_Dialog.setContentView(R.layout.pop_multi_selection);
            RecyclerView recycler_List_Item;

            TextView txt_Title = pick_Dialog.findViewById(R.id.txt_Title);
            Button btn_apply = pick_Dialog.findViewById(R.id.btn_apply);
            Button btn_reset = pick_Dialog.findViewById(R.id.btn_reset);
            txt_Title.setText("Select Region");

            final EditText edt_Search = pick_Dialog.findViewById(R.id.edt_Search);
            final ImageView img_Search_Clear = pick_Dialog.findViewById(R.id.img_Search_Clear);

            multi_selection_adapter = new MultiSelectionAdapter(NewDriverActivity.this, regionPojos, selectedItemIds);
            recycler_List_Item = pick_Dialog.findViewById(R.id.recycler_List_Item);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(NewDriverActivity.this);
            recycler_List_Item.setLayoutManager(mLayoutManager);
            recycler_List_Item.addItemDecoration(new DividerItemDecoration(NewDriverActivity.this, DividerItemDecoration.VERTICAL));
            recycler_List_Item.setAdapter(multi_selection_adapter);

            img_Search_Clear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    edt_Search.setText("");
                }
            });

            img_Search_Clear.setVisibility(View.GONE);
            final RecyclerView finalRecycler_List_Item = recycler_List_Item;
            edt_Search.setImeOptions(EditorInfo.IME_ACTION_DONE);
            edt_Search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    ArrayList<RegionPojo> lkpTempList = new ArrayList<>();
                    String SearchContent = editable.toString().toLowerCase();
                    for (int i = 0; i < regionPojos.size(); i++) {
                        if (regionPojos.get(i).getName().toLowerCase().contains(SearchContent)) {
                            lkpTempList.add(regionPojos.get(i));
                        }
                    }
                    multi_selection_adapter = new MultiSelectionAdapter(NewDriverActivity.this, lkpTempList, selectedItemIds);
                    finalRecycler_List_Item.setAdapter(multi_selection_adapter);
                    if (SearchContent.length() == 0)
                        img_Search_Clear.setVisibility(View.GONE);
                    else
                        img_Search_Clear.setVisibility(View.VISIBLE);
                }
            });

            btn_apply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String strRegion = "";
                    for (int i = 0; i < regionPojos.size(); i++) {
                        for (int j = 0; j < selectedItemIds.size(); j++) {
                            if (selectedItemIds.get(j).equals(regionPojos.get(i).getId())) {
                                strRegion = strRegion + "," + regionPojos.get(i).getName();
                            }
                        }
                    }
                    if (!strRegion.isEmpty()) {
                        edtRegion.setText(strRegion.substring(1, strRegion.length()));
                    } else
                        edtRegion.setText("");
                    pick_Dialog.dismiss();
                }
            });

            btn_reset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    multi_selection_adapter.resetSelection();
                }
            });

            pick_Dialog.show();
        } else if (pick_Dialog.isShowing()) {
            pick_Dialog.dismiss();
            ShowRegionPopUp(regionPojos, selectedItemIds);
        } else {
            pick_Dialog = null;
            ShowRegionPopUp(regionPojos, selectedItemIds);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
            Log.e("onActivityResult: ", images.get(0).getPath());
            CropImage.activity(Uri.fromFile(new File(images.get(0).getPath())))
                    .start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Support.refreshMediaScanner(this, resultUri);
                switch (pick_image_type) {
                    case "profile":
                        str_img_profile = resultUri.getPath();
                        Support.loadImage(resultUri, imgProfile);
                        break;
                    case "PoliceClearance":
                        str_img_police_clearance = resultUri.getPath();
                        Support.loadImage(resultUri, imgPoliceClearance);
                        break;
                    case "Passport":
                        str_img_passport = resultUri.getPath();
                        Support.loadImage(resultUri, imgPassport);
                        break;
                    case "LicenseBack":
                        str_img_license_back = resultUri.getPath();
                        Support.loadImage(resultUri, imgLicenseBack);
                        break;
                    case "LicenseFront":
                        str_img_license_front = resultUri.getPath();
                        Support.loadImage(resultUri, imgLicenseFront);
                        break;
                    case "Id":
                        str_img_id = resultUri.getPath();
                        Support.loadImage(resultUri, imgId);
                        break;
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Support.displayToastMessage(this, error.getMessage());
            }
        }
    }

    private void uploadImage(final boolean isNew, final LinkedHashMap<String, String> images, final int position) {
        String path = (new ArrayList<String>(images.values())).get(position);
        final String imageName = (new ArrayList<String>(images.keySet())).get(position);
        File file = new File(path);
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData(imageName, file.getName(), reqFile);
//        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), imageName);
        ApiCall.getApiCall(this, apiService.uploadDriverImages(Support.getHeader(getApplicationContext()), strDriverId, body/*, name*/), true, new ApiCall.OnApiResult() {
            @Override
            public void onSuccess(Response<Object> objectResponse) {
                if (position + 1 < images.size()) {
                    uploadImage(isNew, images, position + 1);
                } else {
                    if (isNew) {
                        Support.displayToastMessage(NewDriverActivity.this, "Driver Added!");
                        setResult(RESULT_OK, new Intent());
                        finish();
                    } else {
                        Support.displayToastMessage(NewDriverActivity.this, "Driver Updated!");
                        setResult(RESULT_OK, new Intent());
                        finish();
                    }
                }

            }

            @Override
            public void onFailure(String message) {
                Support.displayToastMessage(NewDriverActivity.this, message);
            }
        });
    }
}
