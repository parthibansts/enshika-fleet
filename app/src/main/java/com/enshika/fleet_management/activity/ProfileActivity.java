package com.enshika.fleet_management.activity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.ProfilePojo;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.RuntimePermissionsActivity;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;
import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.Vars.PERISSION_READ_WRITE;

/**
 * Created by Shamla Tech on 05-07-2018.
 */

public class ProfileActivity extends RuntimePermissionsActivity implements View.OnClickListener {
    FloatingActionButton fabCamera;
    CircleImageView ciFleet;
    Toolbar toolbar;
    String strImgPath = "";
    TextView txtUpdate;
    EditText edtFirstName, edtLastName, edtEmail, edtPassword, edtChangePassword, edtOldPassword;
    ProfilePojo profilePojo;
    ResponseParser responseParser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initUI();
        prepareData();
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        if (requestCode == PERISSION_READ_WRITE) {
            pickImage();
        }
    }

    private void pickImage() {
        ImagePicker.with(this)
                .setToolbarColor("#fd6600")
                .setStatusBarColor("#fd6600")
                .setToolbarTextColor("#FFFFFF")
                .setToolbarIconColor("#FFFFFF")
                .setProgressBarColor("#ed1b25")
                .setBackgroundColor("#212121")
                .setCameraOnly(false)
                .setMultipleMode(false)
                .setFolderMode(true)
                .setShowCamera(true)
                .setFolderTitle("Albums")
                .setImageTitle("Galleries")
                .setDoneTitle("Done")
                .setSavePath("Enshika Fleet")
                .setAlwaysShowDoneButton(true)
                .setKeepScreenOn(true)
                .start();
    }

    private void prepareData() {
        edtFirstName.setText(Support.getPref(getApplicationContext(), Vars.FIRST_NAME));
        edtLastName.setText(Support.getPref(getApplicationContext(), Vars.LAST_NAME));
        edtEmail.setText(Support.getPref(getApplicationContext(), Vars.EMAIL));
        Support.loadImage(Support.getPref(getApplicationContext(), Vars.PHOTO_URL), ciFleet, R.drawable.ic_user_placeholder_white);
    }

    private void initUI() {
        edtOldPassword = findViewById(R.id.edtOldPassword);
        edtFirstName = findViewById(R.id.edtFirstName);
        edtLastName = findViewById(R.id.edtLastName);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        edtChangePassword = findViewById(R.id.edtChangePassword);
        txtUpdate = findViewById(R.id.txtUpdate);
        ciFleet = findViewById(R.id.ciFleet);
        fabCamera = findViewById(R.id.fabCamera);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Profile");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        fabCamera.setOnClickListener(this);
        txtUpdate.setOnClickListener(this);

        responseParser = new ResponseParser(this);
    }

    @Override
    public void onClick(View v) {
        switch ((v.getId())) {
            case R.id.fabCamera:
                super.requestAppPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERISSION_READ_WRITE);
                break;
            case R.id.txtUpdate:
                if (isValidate()) {
                    profilePojo = new ProfilePojo();
                    profilePojo.setFirstName(edtFirstName.getText().toString());
                    profilePojo.setLastName(edtLastName.getText().toString());
                    profilePojo.setEmail(edtEmail.getText().toString());
                    profilePojo.setPassword(edtPassword.getText().toString());
                    ApiCall.getApiCall(ProfileActivity.this,
                            apiService.updateFleetProfile(Support.getHeader(ProfileActivity.this), profilePojo),
                            true, new ApiCall.OnApiResult() {
                                @Override
                                public void onSuccess(Response<Object> objectResponse) {
                                    Log.e("onSuccess: ", objectResponse.body() + "");
                                    String status = responseParser.getStatus(objectResponse.body() + "");
                                    if (status.equals("SUCCESS")) {
                                        Support.setPref(getApplicationContext(), Vars.FIRST_NAME, profilePojo.getFirstName());
                                        Support.setPref(getApplicationContext(), Vars.LAST_NAME, profilePojo.getLastName());
                                        Support.setPref(getApplicationContext(), Vars.EMAIL, profilePojo.getEmail());
                                        if (!edtPassword.getText().toString().isEmpty())
                                            Support.setPref(getApplicationContext(), Vars.PASSWORD, profilePojo.getPassword());
                                        setResult(RESULT_OK, new Intent());
                                        if (!strImgPath.isEmpty()) {
                                            File file = new File(strImgPath);
                                            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                                            MultipartBody.Part body = MultipartBody.Part.createFormData("photoUrl", file.getName(), reqFile);

                                            ApiCall.getApiCall(ProfileActivity.this,
                                                    apiService.updateProfileImage(Support.getHeader(ProfileActivity.this),
                                                            Support.getPref(getApplicationContext(), Vars.USER_ID), body), true,
                                                    new ApiCall.OnApiResult() {
                                                        @Override
                                                        public void onSuccess(Response<Object> objectResponse) {
                                                            try {
                                                                Support.setPref(getApplicationContext(), Vars.PHOTO_URL, responseParser.getImageModel(objectResponse.body()).getImageURL());
                                                                setResult(RESULT_OK, new Intent());
                                                                Toast.makeText(ProfileActivity.this, "Updated!", Toast.LENGTH_SHORT).show();
                                                                finish();
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                                Toast.makeText(ProfileActivity.this, "Image upload failed!", Toast.LENGTH_SHORT).show();
                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(String message) {

                                                        }
                                                    });
                                        } else {
                                            Toast.makeText(ProfileActivity.this, "Updated!", Toast.LENGTH_SHORT).show();
                                            finish();
                                        }

                                    } else
                                        Support.displayToastMessage(ProfileActivity.this, responseParser.getErrorMessage(objectResponse.body().toString()));
                                }

                                @Override
                                public void onFailure(String message) {
                                    Support.displayToastMessage(ProfileActivity.this, message);
                                }
                            });
                }
                break;
        }
    }

    private boolean isValidate() {
        boolean isValidate = true;
        if (edtFirstName.getText().toString().isEmpty()) {
            edtFirstName.setError("Please Enter First Name");
            isValidate = false;
        }
        if (edtLastName.getText().toString().isEmpty()) {
            edtLastName.setError("Please Enter Last Name");
            isValidate = false;
        }
        if (edtEmail.getText().toString().isEmpty()) {
            edtEmail.setError("Please Enter Email");
            isValidate = false;
        }
        if (!edtOldPassword.getText().toString().isEmpty()) {
            if (edtPassword.getText().toString().isEmpty()) {
                edtPassword.setError("Please Enter Password");
                isValidate = false;
            }
            if (edtChangePassword.getText().toString().isEmpty()) {
                edtChangePassword.setError("Please Enter Password");
                isValidate = false;
            }
            if (!edtPassword.getText().toString().isEmpty() && !edtChangePassword.getText().toString().isEmpty()
                    && !edtPassword.getText().toString().equals(edtChangePassword.getText().toString())) {
                Toast.makeText(this, "New passwords doesn't match!", Toast.LENGTH_SHORT).show();
                isValidate = false;
            } else {
                if (!Support.getPref(getApplicationContext(), Vars.PASSWORD).equals(edtOldPassword.getText().toString())) {
                    edtOldPassword.setError("Invalid Password");
                    isValidate = false;
                }
            }
        }
        /*if (edtPassword.getText().toString().isEmpty()) {
            edtPassword.setError("Please Enter Password");
            isValidate = false;
        }
        if (edtChangePassword.getText().toString().isEmpty()) {
            edtChangePassword.setError("Please Enter Password");
            isValidate = false;
        }
        if (!edtPassword.getText().toString().isEmpty() && !edtChangePassword.getText().toString().isEmpty()
                && !edtPassword.getText().toString().equals(edtChangePassword.getText().toString())) {
            Toast.makeText(this, "New passwords doesn't match!", Toast.LENGTH_SHORT).show();
            isValidate = false;
        }*/
        return isValidate;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
            Log.e("onActivityResult: ", images.get(0).getPath());
            CropImage.activity(Uri.fromFile(new File(images.get(0).getPath())))
                    .start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Support.refreshMediaScanner(this, resultUri);
                strImgPath = resultUri.getPath();
                Support.loadImage(resultUri, ciFleet);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("error: ", error.getMessage());
            }
        }
    }

}
