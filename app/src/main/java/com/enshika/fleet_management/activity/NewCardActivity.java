package com.enshika.fleet_management.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.cardform.view.CardForm;
import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.CardDetailsPojo;
import com.enshika.fleet_management.model.CardModel;
import com.enshika.fleet_management.model.CardSaveReqModel;
import com.enshika.fleet_management.model.CardValidateReqModel;
import com.enshika.fleet_management.model.CardValidateResultModel;
import com.enshika.fleet_management.model.MerchantPojo;
import com.enshika.fleet_management.model.MerchantResponseModel;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.CardTypeDetectionUtils;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;

import org.json.JSONObject;

import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.ApiCall.apiServiceTeller;
import static com.enshika.fleet_management.utils.Vars.MASTER_CARD;
import static com.enshika.fleet_management.utils.Vars.UNKNOWN;
import static com.enshika.fleet_management.utils.Vars.VISA_CARD;

/**
 * Created by Shamla Tech on 21-07-2018.
 */

public class NewCardActivity extends AppCompatActivity implements View.OnClickListener {
    CardForm card_form;
    Toolbar toolbar;
    TextView btnPay;
    EditText edtHolderName;
    boolean isWalletTransfer = false;
    ResponseParser responseParser;
    MerchantPojo merchantPojo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_card);
        initUI();
        initCardForm();
        getBundleDetails();

    }

    private void getBundleDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("isWalletTransfer"))
                isWalletTransfer = bundle.getBoolean("isWalletTransfer");
        }
        if (!isWalletTransfer) {
            btnPay.setText("Save");
        } else {
            btnPay.setText("Save Card & Proceed to Payment");
        }
    }

    private void getMerchantDetails() {
        ApiCall.getApiCall(NewCardActivity.this, apiService.getMerchantDetails(
                Support.getHeader(getApplicationContext()), Support.getPref(getApplicationContext(), Vars.USER_ID)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        MerchantResponseModel model = responseParser.getMerchantResponseModel(objectResponse.body());
                        if (model.getStatus() == 1) {
                            merchantPojo = model.getData();
                            saveCard();
                        } else {
                            Toast.makeText(NewCardActivity.this, model.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Toast.makeText(NewCardActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void saveCard() {
        final CardValidateReqModel cardValidatePojo = new CardValidateReqModel();
//                    if(!isWalletTransfer)
        cardValidatePojo.setAmount("000000000010");
        cardValidatePojo.setCvv(card_form.getCvv());
        cardValidatePojo.setMerchant_id(merchantPojo.getMerchantId());
        cardValidatePojo.setUrl_response("http://www.shamlatech.com");
        cardValidatePojo.setUser_id(merchantPojo.getUserId());
        final CardDetailsPojo cardDetailsPojo = new CardDetailsPojo();
        cardDetailsPojo.setExpiry_date(card_form.getExpirationMonth()
                + card_form.getExpirationYear().substring(Math.max(card_form.getExpirationYear().length() - 2, 0)));
        cardDetailsPojo.setHolder_name(edtHolderName.getText().toString());
        cardDetailsPojo.setWallet_number(card_form.getCardNumber());

        CardTypeDetectionUtils cardTypeDetectionUtils = CardTypeDetectionUtils.detect(card_form.getCardNumber());
        String cardType;
        if (cardTypeDetectionUtils == CardTypeDetectionUtils.VISA) {
            cardType = VISA_CARD;
        } else if (cardTypeDetectionUtils == CardTypeDetectionUtils.MASTERCARD) {
            cardType = MASTER_CARD;
        } else {
            cardType = UNKNOWN;
        }
        cardDetailsPojo.setWallet_name(cardType);

        cardValidatePojo.setDetails(cardDetailsPojo);
        ApiCall.getApiCall(NewCardActivity.this,
                apiServiceTeller.isValidCard(cardValidatePojo),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        final CardValidateResultModel model = responseParser.getCardValidateResultModel(objectResponse.body());
                        if (model.getCode() == 000) {
                            CardSaveReqModel cardSaveReqModel = new CardSaveReqModel();
                            cardSaveReqModel.setAmount(model.getAmount());
                            cardSaveReqModel.setMerchant_id(cardValidatePojo.getMerchant_id());
                            cardSaveReqModel.setPass_code(merchantPojo.getPassCode());
                            cardSaveReqModel.setReference(model.getReference());
                            cardSaveReqModel.setValidation_code(model.getValidation_code());
                            cardSaveReqModel.setUser_id(cardValidatePojo.getUser_id());
                            ApiCall.getApiCall(NewCardActivity.this,
                                    apiServiceTeller.saveCard(cardSaveReqModel),
                                    true, new ApiCall.OnApiResult() {
                                        @Override
                                        public void onSuccess(Response<Object> objectResponse) {
                                            try {
                                                JSONObject jsonObject = responseParser.getJsonObjectResult(objectResponse.body());
                                                if (jsonObject.has("status")) {
                                                    Toast.makeText(NewCardActivity.this,
                                                            jsonObject.getString("reason"), Toast.LENGTH_SHORT).show();
                                                } else {
                                                    CardValidateReqModel model1 =
                                                            responseParser.getSaveCardResponseModel(objectResponse.body());
                                                    CardModel cardModel = new CardModel();
                                                    cardModel.setWallet_id(model1.getWallet_id());
                                                    cardModel.setMerchant_id(cardValidatePojo.getMerchant_id());
                                                    cardModel.setUser_id(cardValidatePojo.getUser_id());
                                                    cardModel.setDetails(cardDetailsPojo);
                                                    if (model1.getUrl_response() != null && !model1.getUrl_response().isEmpty()) {
                                                        Intent intent = new Intent();
                                                        intent.putExtra("detail", cardModel);
                                                        intent.putExtra("cvv", cardValidatePojo.getCvv());
                                                        setResult(RESULT_OK, intent);
                                                        finish();
                                                    } else {
                                                        Intent intent = new Intent();
                                                        intent.putExtra("detail", cardModel);
                                                        intent.putExtra("cvv", cardValidatePojo.getCvv());
                                                        setResult(RESULT_OK, intent);
                                                        finish();
                                                    }
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onFailure(String message) {
                                            Support.displayToastMessage(NewCardActivity.this, message);
                                        }
                                    });
                        } else
                            Toast.makeText(NewCardActivity.this, model.getReason(),
                                    Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(NewCardActivity.this, "Invalid Card!");
                    }
                });
    }

    private void initUI() {
        edtHolderName = findViewById(R.id.edtHolderName);
        card_form = findViewById(R.id.card_form);
        btnPay = findViewById(R.id.btnPay);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("New Card");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnPay.setOnClickListener(this);

        responseParser = new ResponseParser(this);
        merchantPojo = new MerchantPojo();
    }


    private void initCardForm() {
        card_form.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(true)
                .postalCodeRequired(false)
                .actionLabel("Purchase")
                .setup(this);
        card_form.setCardNumberIcon(R.drawable.ic_user_white);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPay:
                if (isValid()) {
                    getMerchantDetails();
                }
                break;
        }
    }

    private boolean isValid() {
        boolean isValid = true;
        if (edtHolderName.getText().toString().isEmpty()) {
            edtHolderName.setError("Enter Card Holder Name");
            isValid = false;
        }
        if (!card_form.isValid())
            isValid = false;

        return isValid;
    }
}
