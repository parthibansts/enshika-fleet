package com.enshika.fleet_management.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.LinearLayout;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.activity.MenuPageActivity;
import com.enshika.fleet_management.adapter.DriversAdapter;
import com.enshika.fleet_management.model.DriversPojo;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.SearchViewInterface;
import com.enshika.fleet_management.utils.Support;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;

import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;

/**
 * Created by Vino on 20-03-2018.
 */

public class FleetFeeFragment extends Fragment implements View.OnClickListener {
    ResponseParser responseParser;
    ArrayList<DriversPojo> driversPojos, searchPojos;
    RecyclerView recyclerView;
    DriversAdapter allDriverAdapter, searchDriverAdapter;
    Activity activity;
    FloatingActionMenu fab_menu;
    FloatingActionButton fab_low_to_high, fab_high_to_low;
    LinearLayout linearPlaceholder;
    String sort_by = "low";
    String search = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fleet_fee, container, false);
        initUI(view);
        prepareDriversList(true, search, sort_by);
        ((MenuPageActivity) activity).getInsufficientFleetFeeCount();

        ((MenuPageActivity) activity).getSearchViewAction(new SearchViewInterface() {
            @Override
            public void onTextChanged(String text) {
                search = text;
//                prepareDriversList(false, search, sort_by);
            }

            @Override
            public void onSearch(String text) {
                search = text;
                Support.hideSoftKeypad(activity);
                prepareDriversList(true, search, sort_by);
            }

            @Override
            public void searchViewOpened() {
                fab_menu.close(true);
                search = "";
            }

            @Override
            public void searchViewClosed() {
                search = "";
                prepareRecyclerAllDrivers();
            }
        });
        return view;

    }

    private void prepareDriversList(boolean progress, final String search, String sort_by) {
        ApiCall.getApiCall(activity,
                apiService.getDriversFleetFee(Support.getHeader(activity), sort_by, search),
                progress, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        if (search.isEmpty()) {
                            driversPojos = responseParser.getFleetFeeDriverList(objectResponse.body());
                            prepareRecyclerAllDrivers();
                        } else {
                            searchPojos = new ArrayList<>();
                            searchPojos = responseParser.getFleetFeeDriverList(objectResponse.body());
                            prepareRecyclerSearchDrivers();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(activity, message);
                    }
                });
    }

    private void prepareRecyclerSearchDrivers() {
        if (searchPojos.isEmpty())
            linearPlaceholder.setVisibility(View.VISIBLE);
        else
            linearPlaceholder.setVisibility(View.GONE);
//        if (searchDriverAdapter == null) {
        searchDriverAdapter = new DriversAdapter(activity, FleetFeeFragment.this, searchPojos);
        recyclerView.setAdapter(searchDriverAdapter);
//        } else
//            searchDriverAdapter.notifyDataSetChanged();
    }

    private void prepareRecyclerAllDrivers() {
        if (driversPojos.isEmpty())
            linearPlaceholder.setVisibility(View.VISIBLE);
        else
            linearPlaceholder.setVisibility(View.GONE);
//        if (allDriverAdapter == null) {
        allDriverAdapter = new DriversAdapter(activity, FleetFeeFragment.this, driversPojos);
        recyclerView.setAdapter(allDriverAdapter);
//        } else
//            allDriverAdapter.notifyDataSetChanged();
    }

    private void initUI(View view) {
        linearPlaceholder = view.findViewById(R.id.linearPlaceholder);
        recyclerView = view.findViewById(R.id.recyclerView);
        fab_menu = view.findViewById(R.id.fab_menu);
        fab_low_to_high = view.findViewById(R.id.fab_low_to_high);
        fab_high_to_low = view.findViewById(R.id.fab_high_to_low);

        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        driversPojos = new ArrayList<>();
        searchPojos = new ArrayList<>();
        responseParser = new ResponseParser(activity);
        fab_low_to_high.setOnClickListener(this);
        fab_high_to_low.setOnClickListener(this);
    }

    private void createCustomAnimation() {
        AnimatorSet set = new AnimatorSet();

        ObjectAnimator scaleOutX = ObjectAnimator.ofFloat(fab_menu.getMenuIconView(), "scaleX", 1.0f, 0.2f);
        ObjectAnimator scaleOutY = ObjectAnimator.ofFloat(fab_menu.getMenuIconView(), "scaleY", 1.0f, 0.2f);

        ObjectAnimator scaleInX = ObjectAnimator.ofFloat(fab_menu.getMenuIconView(), "scaleX", 0.2f, 1.0f);
        ObjectAnimator scaleInY = ObjectAnimator.ofFloat(fab_menu.getMenuIconView(), "scaleY", 0.2f, 1.0f);

        scaleOutX.setDuration(50);
        scaleOutY.setDuration(50);

        scaleInX.setDuration(150);
        scaleInY.setDuration(150);

        scaleInX.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                fab_menu.getMenuIconView().setImageResource(fab_menu.isOpened()
                        ? R.drawable.ic_close : R.drawable.ic_menu);
            }
        });

        set.play(scaleOutX).with(scaleOutY);
        set.play(scaleInX).with(scaleInY).after(scaleOutX);
        set.setInterpolator(new AccelerateInterpolator());

        fab_menu.setIconToggleAnimatorSet(set);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_low_to_high:
                fab_menu.close(true);
                sort_by = "low";
                prepareDriversList(true, search, sort_by);
                break;
            case R.id.fab_high_to_low:
                fab_menu.close(true);
                sort_by = "high";
                prepareDriversList(true, search, sort_by);
                break;
        }
    }

    public void onClickDriverList(int position) {
        /*Intent intent = new Intent(activity, DriverDetailsActivity.class);
        if (isSearch)
            intent.putExtra("detail", searchPojos.get(position));
        else
            intent.putExtra("detail", driversPojos.get(position));
        startActivityForResult(intent, REFRESH_SCREEN);*/
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity.setTitle("Fleet Fee");
        createCustomAnimation();
    }
}
