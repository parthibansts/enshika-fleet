package com.enshika.fleet_management.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.enshika.fleet_management.R;

/**
 * Created by Vino on 29-03-2018.
 */

public class RideHistoryFragment extends Fragment {
    RecyclerView recyclerRides;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ride_history,container,false);
        initUI(view);
        return view;
    }

    private void initUI(View view) {
        recyclerRides = view.findViewById(R.id.recyclerRides);
    }
}
