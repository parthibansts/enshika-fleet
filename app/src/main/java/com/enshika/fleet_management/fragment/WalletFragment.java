package com.enshika.fleet_management.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.activity.CardListActivity;
import com.enshika.fleet_management.activity.MenuPageActivity;
import com.enshika.fleet_management.activity.MobileMoneyActivity;
import com.enshika.fleet_management.adapter.PaymentOptionsAdapter;
import com.enshika.fleet_management.adapter.TransactionHistoryAdapter;
import com.enshika.fleet_management.model.DepositReqModel;
import com.enshika.fleet_management.model.MerchantPojo;
import com.enshika.fleet_management.model.MerchantResponseModel;
import com.enshika.fleet_management.model.PaymentOptionPojo;
import com.enshika.fleet_management.model.PaymentResponseModel;
import com.enshika.fleet_management.model.TransactionHistoryModel;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;
import com.flipboard.bottomsheet.BottomSheetLayout;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.Vars.PAYMENT_CARD;
import static com.enshika.fleet_management.utils.Vars.PAYMENT_MOMO;

/**
 * Created by Shamla Tech on 04-07-2018.
 */

public class WalletFragment extends Fragment implements View.OnClickListener {
    RecyclerView recyclerTransactionHistory;
    TransactionHistoryAdapter transactionHistoryAdapter;
    ArrayList<TransactionHistoryModel> transactionHistoryModels;
    Activity activity;
    CircleImageView ciFleet;
    BottomSheetLayout bottomSheet;
    TextView txtAddMoney, txtAmountOne, txtAmountTwo, txtAmountThree, txtAmountFour, txtWalletAmount;
    EditText edtAmount;
    ResponseParser responseParser;
    double walletBalance = 0.0;
    String money = "";
    ArrayList<PaymentOptionPojo> paymentPojos;
    View bottomSheetView;
    RecyclerView recyclerPaymentOptions;
    MerchantPojo merchantPojo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wallet, container, false);
        initUI(view);
        accessWalletBalance();
        prepareTransactionHistory();
        preparePaymentOptions(false);
        Support.loadImage(Support.getPref(activity, Vars.PHOTO_URL), ciFleet, R.drawable.ic_user_placeholder_white);
        return view;
    }

    private void initUI(View view) {
        bottomSheetView = LayoutInflater.from(activity).inflate(R.layout.bottom_payment_options, bottomSheet, false);
        recyclerPaymentOptions = bottomSheetView.findViewById(R.id.recyclerPaymentOptions);
        ciFleet = view.findViewById(R.id.ciFleet);
        txtWalletAmount = view.findViewById(R.id.txtWalletAmount);
        edtAmount = view.findViewById(R.id.edtAmount);
        txtAmountOne = view.findViewById(R.id.txtAmountOne);
        txtAmountTwo = view.findViewById(R.id.txtAmountTwo);
        txtAmountThree = view.findViewById(R.id.txtAmountThree);
        txtAmountFour = view.findViewById(R.id.txtAmountFour);
        txtAddMoney = view.findViewById(R.id.txtAddMoney);
        bottomSheet = view.findViewById(R.id.bottomsheet);
        recyclerTransactionHistory = view.findViewById(R.id.recyclerTransactionHistory);

        transactionHistoryModels = new ArrayList<>();

        txtAmountOne.setOnClickListener(this);
        txtAmountTwo.setOnClickListener(this);
        txtAmountThree.setOnClickListener(this);
        txtAmountFour.setOnClickListener(this);
        txtAddMoney.setOnClickListener(this);

        paymentPojos = new ArrayList<>();
        responseParser = new ResponseParser(activity);
    }

    private void accessWalletBalance() {
        ApiCall.getApiCall(activity,
                apiService.getWalletBalance(Support.getHeader(activity)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        walletBalance = responseParser.getWalletModel(objectResponse.body()).getWalletBalance();
                        txtWalletAmount.setText(walletBalance + " " + Support.getCurrencySymbol(activity));
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(activity, message);
                    }
                });
    }

    private void prepareTransactionHistory() {
        ApiCall.getApiCall(activity,
                apiService.getTransactionHistory(Support.getHeader(activity)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        transactionHistoryModels = new ArrayList<>();
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        transactionHistoryModels = responseParser.getTransactionList(objectResponse.body());
                        prepareTransactionHistoryAdapter();
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(activity, message);
                    }
                });
    }

    private void depositAmount(final int payment_type) {
        DepositReqModel model = new DepositReqModel();
//        money = "50";
        money = Support.addLeadingZeros(money);
        model.setAmount(money);
        model.setComments("fleet");
        model.setUserId(Support.getPref(activity, Vars.USER_ID));
        ApiCall.getApiCall(activity,
                apiService.depositAmount(Support.getHeader(activity), model),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        MerchantResponseModel model1 = responseParser.getMerchantResponseModel(objectResponse.body());
                        if (model1.getStatus() == 1) {
                            merchantPojo = model1.getData();
                            if (payment_type == Vars.PAYMENT_MOMO) {
                                Intent intent = new Intent(activity, MobileMoneyActivity.class);
                                intent.putExtra("isWalletTransfer", true);
                                intent.putExtra("merchantPojo", merchantPojo);
                                intent.putExtra("money", money);
                                startActivityForResult(intent, Vars.REFRESH_SCREEN);
                            } else if (payment_type == Vars.PAYMENT_CARD) {
                                Intent intent = new Intent(activity, CardListActivity.class);
                                intent.putExtra("isWalletTransfer", true);
                                intent.putExtra("merchantPojo", merchantPojo);
                                intent.putExtra("money", money);
                                startActivityForResult(intent, Vars.REFRESH_SCREEN);
                            }
                        } else {
                            Toast.makeText(activity, model1.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(activity, message);
                    }
                });
    }

    private void prepareTransactionHistoryAdapter() {
        transactionHistoryAdapter = new TransactionHistoryAdapter(activity, this, transactionHistoryModels);
        recyclerTransactionHistory.setLayoutManager(new LinearLayoutManager(activity));
        recyclerTransactionHistory.setAdapter(transactionHistoryAdapter);
    }

    private void preparePaymentOptions(final boolean isPaymentOptionNeeded) {
        ApiCall.getApiCall(activity,
                apiService.getPaymentModes(Support.getHeader(activity)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        paymentPojos = new ArrayList<>();
                        PaymentResponseModel paymentModesResponseModel = responseParser.getPaymentModel(objectResponse.body());
                        ArrayList<PaymentOptionPojo> tempPaymentPojos = paymentModesResponseModel.getData().getPaymentmodes();
                        for (int i = 0; i < tempPaymentPojos.size(); i++) {
                            if (tempPaymentPojos.get(i).getPaymentCode() == PAYMENT_CARD
                                    || tempPaymentPojos.get(i).getPaymentCode() == PAYMENT_MOMO) {
                                paymentPojos.add(tempPaymentPojos.get(i));
                            }
                        }
                        if (isPaymentOptionNeeded) {
                            showAddMoneyOptions();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(activity, message);
                    }
                });
    }

    private void showAddMoneyOptions() {
        if (!paymentPojos.isEmpty()) {
            preparePaymentOptionsAdapter();
            bottomSheet.showWithSheetView(bottomSheetView);
        } else {
            preparePaymentOptions(true);
        }
    }

    private void preparePaymentOptionsAdapter() {
        PaymentOptionsAdapter paymentOptionsAdapter = new PaymentOptionsAdapter(activity, WalletFragment.this, paymentPojos);
        recyclerPaymentOptions.setLayoutManager(new LinearLayoutManager(activity));
        recyclerPaymentOptions.setAdapter(paymentOptionsAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtAddMoney:
                if (!edtAmount.getText().toString().isEmpty()) {
                    money = edtAmount.getText().toString();
                    showAddMoneyOptions();
                } else {
                    edtAmount.setError("enter money");
                }
                break;
            case R.id.txtAmountOne:
                money = txtAmountOne.getText().toString();
                showAddMoneyOptions();
                break;
            case R.id.txtAmountTwo:
                money = txtAmountTwo.getText().toString();
                showAddMoneyOptions();
                break;
            case R.id.txtAmountThree:
                money = txtAmountThree.getText().toString();
                showAddMoneyOptions();
                break;
            case R.id.txtAmountFour:
                money = txtAmountFour.getText().toString();
                showAddMoneyOptions();
                break;

        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity.setTitle("Wallet");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Vars.REFRESH_SCREEN && resultCode == RESULT_OK) {
            accessWalletBalance();
        }
    }

    public void onClickPayment(int position) {
        if (bottomSheet != null && bottomSheet.isSheetShowing()) {
            bottomSheet.dismissSheet();
        }
        if (paymentPojos.get(position).getPaymentCode() == PAYMENT_CARD) {
            depositAmount(PAYMENT_CARD);
        } else if (paymentPojos.get(position).getPaymentCode() == PAYMENT_MOMO) {
            depositAmount(PAYMENT_MOMO);
        }
    }
}
