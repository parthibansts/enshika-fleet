package com.enshika.fleet_management.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.adapter.DriversCarsFragmentAdapter;

/**
 * Created by Vino on 20-03-2018.
 */

public class DriversCarsFragment extends Fragment {
    TabLayout tab_drivers_cars;
    ViewPager viewpager_drivers_cars;
    DriversCarsFragmentAdapter driversCarsFragmentAdapter;
    Activity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_drivers_cars, container, false);

        initUI(view);

        initFragmentViewPager();

        return view;
    }

    private void initFragmentViewPager() {
        driversCarsFragmentAdapter = new DriversCarsFragmentAdapter(activity, getChildFragmentManager());
        viewpager_drivers_cars.setAdapter(driversCarsFragmentAdapter);
        tab_drivers_cars.setupWithViewPager(viewpager_drivers_cars);
    }

    private void initUI(View view) {
        tab_drivers_cars = view.findViewById(R.id.tab_drivers_cars);
        viewpager_drivers_cars = view.findViewById(R.id.viewpager_drivers_cars);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity.setTitle("Fleet");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }
}
