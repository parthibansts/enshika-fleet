package com.enshika.fleet_management.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.enshika.fleet_management.R;
import com.enshika.fleet_management.adapter.DashboardAdapter;
import com.enshika.fleet_management.model.DashboardSummaryModel;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.Support.formatDateIntoMMddyyyy;

/**
 * Created by Shamla Tech on 14-06-2018.
 */

public class DashboardSummaryFragment extends Fragment implements View.OnClickListener {
    FloatingActionMenu fab_menu;
    FloatingActionButton fab_all, fab_today, fab_yesterday, fab_last_seven, fab_last_thirty, fab_month, fab_last_month, fab_custom;
    RecyclerView recyclerDashboard;
    DashboardAdapter dashboardAdapter;
    ArrayList<DashboardSummaryModel> dashboardPojos;
    Activity activity;
    Dialog range_picker_dialog;
    TextView txtDateRange;
    String strStartDate = "", strEndDate = "";
    Date startDate, endDate;
    ResponseParser responseParser;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard_summary, container, false);
        initUI(view);
        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-M-yyyy");
            endDate = calendar.getTime();
            strEndDate = simpleDateFormat.format(calendar.getTime());
            calendar.add(Calendar.DATE, -5);
            startDate = calendar.getTime();
            strStartDate = simpleDateFormat.format(calendar.getTime());

            strStartDate = formatDateIntoMMddyyyy(strStartDate);
            strEndDate = formatDateIntoMMddyyyy(strEndDate);

            txtDateRange.setText(strStartDate + " to " + strEndDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        prepareDashboardItems();
        return view;
    }

    private void prepareRecyclerDashboard() {
        if (dashboardAdapter == null) {
            dashboardAdapter = new DashboardAdapter(activity, DashboardSummaryFragment.this, dashboardPojos);
            recyclerDashboard.setLayoutManager(new LinearLayoutManager(activity));
            recyclerDashboard.setAdapter(dashboardAdapter);
        } else
            dashboardAdapter.notifyDataSetChanged();
    }

    private void prepareDashboardItems() {
        ApiCall.getApiCall(activity, apiService.getDashboardSummary(Support.getHeader(activity),
                Support.convertDateToMillisecs(strStartDate) + "",
                Support.convertDateToMillisecs(strEndDate) + ""), true, new ApiCall.OnApiResult() {
            @Override
            public void onSuccess(Response<Object> objectResponse) {
                dashboardPojos = responseParser.getDashboardSummary(objectResponse.body());
                prepareRecyclerDashboard();
            }

            @Override
            public void onFailure(String message) {

            }
        });
    }

    private void initUI(View view) {
        txtDateRange = view.findViewById(R.id.txtDateRange);
        recyclerDashboard = view.findViewById(R.id.recyclerDashboard);
        fab_menu = view.findViewById(R.id.fab_menu);
        fab_all = view.findViewById(R.id.fab_all);
        fab_today = view.findViewById(R.id.fab_today);
        fab_yesterday = view.findViewById(R.id.fab_yesterday);
        fab_last_seven = view.findViewById(R.id.fab_last_seven);
        fab_last_thirty = view.findViewById(R.id.fab_last_thirty);
        fab_month = view.findViewById(R.id.fab_month);
        fab_last_month = view.findViewById(R.id.fab_last_month);
        fab_custom = view.findViewById(R.id.fab_custom);

        dashboardPojos = new ArrayList<>();
        responseParser = new ResponseParser(activity);
        fab_all.setOnClickListener(this);
        fab_today.setOnClickListener(this);
        fab_yesterday.setOnClickListener(this);
        fab_last_seven.setOnClickListener(this);
        fab_last_thirty.setOnClickListener(this);
        fab_month.setOnClickListener(this);
        fab_last_month.setOnClickListener(this);
        fab_custom.setOnClickListener(this);
        txtDateRange.setOnClickListener(this);
    }


    private void createCustomAnimation() {
        AnimatorSet set = new AnimatorSet();

        ObjectAnimator scaleOutX = ObjectAnimator.ofFloat(fab_menu.getMenuIconView(), "scaleX", 1.0f, 0.2f);
        ObjectAnimator scaleOutY = ObjectAnimator.ofFloat(fab_menu.getMenuIconView(), "scaleY", 1.0f, 0.2f);

        ObjectAnimator scaleInX = ObjectAnimator.ofFloat(fab_menu.getMenuIconView(), "scaleX", 0.2f, 1.0f);
        ObjectAnimator scaleInY = ObjectAnimator.ofFloat(fab_menu.getMenuIconView(), "scaleY", 0.2f, 1.0f);

        scaleOutX.setDuration(50);
        scaleOutY.setDuration(50);

        scaleInX.setDuration(150);
        scaleInY.setDuration(150);

        scaleInX.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                fab_menu.getMenuIconView().setImageResource(fab_menu.isOpened()
                        ? R.drawable.ic_menu : R.drawable.ic_close);
            }
        });

        set.play(scaleOutX).with(scaleOutY);
        set.play(scaleInX).with(scaleInY).after(scaleOutX);
        set.setInterpolator(new OvershootInterpolator(2));

        fab_menu.setIconToggleAnimatorSet(set);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity.setTitle("Dashboard");
        createCustomAnimation();
    }

    //    fab_today,fab_yesterday,fab_last_seven,fab_last_thirty,fab_month,fab_last_month,fab_custom;
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_all:
                fab_menu.close(true);
                break;
            case R.id.fab_today:
                fab_menu.close(true);
                break;
            case R.id.fab_yesterday:
                fab_menu.close(true);
                break;
            case R.id.fab_last_seven:
                fab_menu.close(true);
                break;
            case R.id.fab_last_thirty:
                fab_menu.close(true);
                break;
            case R.id.fab_month:
                fab_menu.close(true);
                break;
            case R.id.fab_last_month:
                fab_menu.close(true);
                break;
            case R.id.fab_custom:
                fab_menu.close(true);
                showDateRangePickerDialog();
                break;
            case R.id.txtDateRange:
                showDateRangePickerDialog();
                break;
        }
    }

    private void showDateRangePickerDialog() {
        Calendar startDateCalendar = Calendar.getInstance();
        startDateCalendar.setTime(startDate);

        Calendar endDateCalendar = Calendar.getInstance();
        endDateCalendar.setTime(endDate);

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
                        String tempStartDay = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        String tempEndDay = dayOfMonthEnd + "-" + (monthOfYearEnd + 1) + "-" + yearEnd;
                        Date tempStartDate = Support.stringToDateinddMYYYY(tempStartDay);
                        Date tempEndDate = Support.stringToDateinddMYYYY(tempEndDay);

                        if (tempStartDate.before(tempEndDate) || tempStartDate.equals(tempEndDate)) {
                            strStartDate = tempStartDay;
                            strEndDate = tempEndDay;
                            strStartDate = formatDateIntoMMddyyyy(strStartDate);
                            strEndDate = formatDateIntoMMddyyyy(strEndDate);
                            txtDateRange.setText(strStartDate + " to " + strEndDate);

                            startDate = tempStartDate;
                            endDate = tempEndDate;

                            prepareDashboardItems();
                        } else {
                            Toast.makeText(activity, "Invalid date selection!", Toast.LENGTH_SHORT).show();
                            showDateRangePickerDialog();
                        }

                    }
                },
                startDateCalendar.get(Calendar.YEAR),
                startDateCalendar.get(Calendar.MONTH),
                startDateCalendar.get(Calendar.DAY_OF_MONTH), endDateCalendar.get(Calendar.YEAR),
                endDateCalendar.get(Calendar.MONTH),
                endDateCalendar.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(activity.getFragmentManager(), "Datepickerdialog");

    }
}
