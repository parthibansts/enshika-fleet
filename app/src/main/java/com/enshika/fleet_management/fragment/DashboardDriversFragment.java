package com.enshika.fleet_management.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.DriverStatusModel;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;

import java.util.ArrayList;

import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;

/**
 * Created by Shamla Tech on 20-06-2018.
 */

public class DashboardDriversFragment extends Fragment implements OnChartValueSelectedListener {
    PieChart pieChart;
    Activity activity;
    ResponseParser responseParser;
    DriverStatusModel driverStatusModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard_drivers, container, false);
        iniUI(view);
        prepareDatas();
        return view;
    }

    private void iniUI(View view) {
        pieChart = view.findViewById(R.id.pieChart);

        responseParser = new ResponseParser(activity);
        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);

        pieChart.setDragDecelerationFrictionCoef(0.95f);

        pieChart.setCenterText(generateCenterSpannableText());

        pieChart.setDrawHoleEnabled(false);
        pieChart.setHoleColor(Color.WHITE);

        pieChart.setTransparentCircleColor(Color.WHITE);
        pieChart.setTransparentCircleAlpha(50);

        pieChart.setHoleRadius(30f);
        pieChart.setTransparentCircleRadius(45f);

        pieChart.setDrawCenterText(false);

        pieChart.setRotationAngle(45);
        // enable rotation of the chart by touch
        pieChart.setRotationEnabled(false);
        pieChart.setHighlightPerTapEnabled(true);

        // pieChart.setUnit(" €");
        // pieChart.setDrawUnitsInChart(true);

        // add a selection listener
        pieChart.setOnChartValueSelectedListener(this);

        pieChart.animateY(1400, Easing.EasingOption.EaseInSine);
        // pieChart.spin(2000, 0, 360);

        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        // entry label styling
        pieChart.setEntryLabelColor(Color.WHITE);
        pieChart.setEntryLabelTextSize(12f);
    }

    private void prepareDatas() {
        ApiCall.getApiCall(activity,
                apiService.getDriversStatus(Support.getHeader(activity)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        driverStatusModel = responseParser.getDriverStatusModel(objectResponse.body());
                        setData();
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(activity, message);
                    }
                });
    }

    private void setData() {


        ArrayList<PieEntry> entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array
        // determines their position around the center of
        // the chart.
        entries.add(new PieEntry(driverStatusModel.getDriverOnline(), "Online"));
        entries.add(new PieEntry(driverStatusModel.getDriverOffline(), "Offline"));

        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<>();

        colors.add(activity.getResources().getColor(R.color.green));
        colors.add(activity.getResources().getColor(R.color.common_positive_action_button_color));

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.WHITE);
        pieChart.setData(data);

        // undo all highlights
        pieChart.highlightValues(null);

        pieChart.invalidate();
    }


    private SpannableString generateCenterSpannableText() {

        SpannableString s = new SpannableString("Drivers\nstatus");
        s.setSpan(new RelativeSizeSpan(1.7f), 0, 7, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 7, s.length() - 7, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 7, s.length() - 7, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 7, s.length() - 7, 0);
        return s;
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (e == null)
            return;
        Log.i("VAL SELECTED",
                "Value: " + e.getY() + ", index: " + h.getX()
                        + ", DataSet index: " + h.getDataSetIndex());
        Toast.makeText(activity, (int) e.getY() + " drivers " + ((PieEntry) e).getLabel(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (pieChart != null) {
                pieChart.setVisibility(View.VISIBLE);
                pieChart.animateY(500, Easing.EasingOption.EaseInSine);
            }
        } else {
            if (pieChart != null)
                pieChart.setVisibility(View.GONE);
        }
    }
}
