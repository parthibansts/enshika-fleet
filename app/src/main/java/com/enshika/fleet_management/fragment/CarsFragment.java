package com.enshika.fleet_management.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.activity.NewCarActivity;
import com.enshika.fleet_management.adapter.CarsAdapter;
import com.enshika.fleet_management.model.CarsPojo;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;

import java.util.ArrayList;

import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.Vars.REFRESH_SCREEN;

/**
 * Created by Vino on 20-03-2018.
 */

public class CarsFragment extends Fragment implements View.OnClickListener {
    ArrayList<CarsPojo> carsPojos;
    RecyclerView recyclerView;
    CarsAdapter carsAdapter;
    Activity activity;
    FloatingActionButton fabAdd;
    ResponseParser responseParser;
    LinearLayout linearPlaceholder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cars, container, false);
        initUI(view);
        prepareCarList();
        return view;
    }

    private void prepareCarList() {
        ApiCall.getApiCall(activity,
                apiService.getCarList(Support.getHeader(activity)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        carsPojos = responseParser.getCarList(objectResponse.body());
                        prepareRecyclerCar();
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(activity, message);
                    }
                });
    }

    private void prepareRecyclerCar() {
        if(carsPojos.isEmpty())
            linearPlaceholder.setVisibility(View.VISIBLE);
        else
            linearPlaceholder.setVisibility(View.GONE);
        carsAdapter = new CarsAdapter(activity, CarsFragment.this, carsPojos);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(carsAdapter);
    }


    private void initUI(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        linearPlaceholder = view.findViewById(R.id.linearPlaceholder);
        fabAdd = view.findViewById(R.id.fabAdd);

        responseParser = new ResponseParser(activity);
        fabAdd.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabAdd:
                startActivityForResult(new Intent(activity, NewCarActivity.class), REFRESH_SCREEN);
                break;
        }
    }

    public void onClickCarListItem(int position) {
        Intent intent = new Intent(activity, NewCarActivity.class);
        intent.putExtra("id", carsPojos.get(position).getCarId());
        startActivityForResult(intent, REFRESH_SCREEN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REFRESH_SCREEN && resultCode == RESULT_OK) {
            prepareCarList();
        }
    }
}
