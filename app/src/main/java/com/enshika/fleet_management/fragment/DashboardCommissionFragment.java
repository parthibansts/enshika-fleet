package com.enshika.fleet_management.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.enshika.fleet_management.R;
import com.enshika.fleet_management.utils.Support;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Shamla Tech on 20-06-2018.
 */

public class DashboardCommissionFragment extends Fragment implements OnChartValueSelectedListener, View.OnClickListener {
    PieChart pieChart;
    Activity activity;
    String strStartDate = "", strEndDate = "";
    TextView txtDateRange;
    Date startDate, endDate;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard_commission, container, false);
        iniUI(view);

        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-M-yyyy");
            endDate = calendar.getTime();
            strEndDate = simpleDateFormat.format(calendar.getTime());
            calendar.add(Calendar.DATE, -5);
            startDate = calendar.getTime();
            strStartDate = simpleDateFormat.format(calendar.getTime());

            txtDateRange.setText(strStartDate + " to " + strEndDate);

            setData(false);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private void iniUI(View view) {
        pieChart = view.findViewById(R.id.pieChart);
        txtDateRange = view.findViewById(R.id.txtDateRange);

        txtDateRange.setOnClickListener(this);
        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);

        pieChart.setDragDecelerationFrictionCoef(0.95f);

        pieChart.setCenterText(generateCenterSpannableText());

        pieChart.setDrawHoleEnabled(false);
        pieChart.setHoleColor(Color.WHITE);

        pieChart.setTransparentCircleColor(Color.WHITE);
        pieChart.setTransparentCircleAlpha(110);

        pieChart.setHoleRadius(30f);
        pieChart.setTransparentCircleRadius(45f);

        pieChart.setDrawCenterText(false);

        pieChart.setRotationAngle(45);
        // enable rotation of the chart by touch
        pieChart.setRotationEnabled(false);
        pieChart.setHighlightPerTapEnabled(true);

        // pieChart.setUnit(" €");
        // pieChart.setDrawUnitsInChart(true);

        // add a selection listener
        pieChart.setOnChartValueSelectedListener(this);

//        pieChart.animateY(1400, Easing.EasingOption.EaseInSine);
        // pieChart.spin(2000, 0, 360);

        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        // entry label styling
        pieChart.setEntryLabelColor(Color.WHITE);
        pieChart.setEntryLabelTextSize(12f);
    }


    private void setData(boolean reset) {


        ArrayList<PieEntry> entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array
        // determines their position around the center of
        // the chart.

        if (reset) {
            entries.add(new PieEntry(60, "Fleet Manager"));
            entries.add(new PieEntry(20, "Driver"));

        } else {
            entries.add(new PieEntry(70, "Fleet Manager"));
            entries.add(new PieEntry(120, "Driver"));
        }
        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<>();

        colors.add(activity.getResources().getColor(R.color.navy));
        colors.add(activity.getResources().getColor(R.color.purple));

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.WHITE);
        pieChart.setData(data);

        // undo all highlights
        pieChart.highlightValues(null);

        pieChart.invalidate();
    }


    private SpannableString generateCenterSpannableText() {

        SpannableString s = new SpannableString("Enshika\ndrivers");
        s.setSpan(new RelativeSizeSpan(1.7f), 0, 7, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 7, s.length() - 8, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 7, s.length() - 8, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 7, s.length() - 8, 0);
        return s;
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (e == null)
            return;
        Log.i("VAL SELECTED",
                "Value: " + e.getY() + ", index: " + h.getX()
                        + ", DataSet index: " + h.getDataSetIndex());
        Toast.makeText(activity, e.getY() + " Ghs for " + ((PieEntry) e).getLabel(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtDateRange:
                showDateRangePickerDialog();
                break;
        }
    }

    private void showDateRangePickerDialog() {
        Calendar startDateCalendar = Calendar.getInstance();
        startDateCalendar.setTime(startDate);

        Calendar endDateCalendar = Calendar.getInstance();
        endDateCalendar.setTime(endDate);

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
                        String tempStartDay = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        String tempEndDay = dayOfMonthEnd + "-" + (monthOfYearEnd + 1) + "-" + yearEnd;
                        Date tempStartDate = Support.stringToDateinddMYYYY(tempStartDay);
                        Date tempEndDate = Support.stringToDateinddMYYYY(tempEndDay);

                        if (tempStartDate.before(tempEndDate) || tempStartDate.equals(tempEndDate)) {
                            strStartDate = tempStartDay;
                            strEndDate = tempEndDay;
                            txtDateRange.setText(strStartDate + " to " + strEndDate);
                            startDate = tempStartDate;
                            endDate = tempEndDate;
                            setData(true);
                            pieChart.invalidate();
                        } else {
                            Toast.makeText(activity, "Invalid date selection!", Toast.LENGTH_SHORT).show();
                            showDateRangePickerDialog();
                        }

                    }
                },
                startDateCalendar.get(Calendar.YEAR),
                startDateCalendar.get(Calendar.MONTH),
                startDateCalendar.get(Calendar.DAY_OF_MONTH), endDateCalendar.get(Calendar.YEAR),
                endDateCalendar.get(Calendar.MONTH),
                endDateCalendar.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(activity.getFragmentManager(), "Datepickerdialog");

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (pieChart != null) {
                pieChart.setVisibility(View.VISIBLE);
                pieChart.animateY(500, Easing.EasingOption.EaseInSine);
            }
        } else {
            if (pieChart != null)
                pieChart.setVisibility(View.GONE);
        }
    }
}
