package com.enshika.fleet_management.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.adapter.DashboardFragmentAdapter;

/**
 * Created by Vino on 20-03-2018.
 */

public class DashboardFragment extends Fragment {
    TabLayout tab_dashboard;
    ViewPager viewpager_dashboard;
    DashboardFragmentAdapter dashboardFragmentAdapter;
    Activity activity;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard,container,false);

        initUI(view);
        initFragmentViewPager();

        return view;
    }

    private void initFragmentViewPager() {
        dashboardFragmentAdapter = new DashboardFragmentAdapter(activity,getChildFragmentManager());
        viewpager_dashboard.setAdapter(dashboardFragmentAdapter);
        viewpager_dashboard.setOffscreenPageLimit(2);
        tab_dashboard.setupWithViewPager(viewpager_dashboard);
    }

    private void initUI(View view) {
        tab_dashboard = view.findViewById(R.id.tab_dashboard);
        viewpager_dashboard = view.findViewById(R.id.viewpager_dashboard);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity.setTitle("Dashboard");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity =(Activity) context;
    }
}
