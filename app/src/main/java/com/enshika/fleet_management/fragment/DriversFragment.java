package com.enshika.fleet_management.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.activity.DriverDetailsActivity;
import com.enshika.fleet_management.activity.NewDriverActivity;
import com.enshika.fleet_management.adapter.DriversAdapter;
import com.enshika.fleet_management.model.DriversPojo;
import com.enshika.fleet_management.model.DriversResponsePojo;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;

import java.util.ArrayList;

import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.Vars.REFRESH_SCREEN;

/**
 * Created by Vino on 20-03-2018.
 */

public class DriversFragment extends Fragment implements View.OnClickListener {
    ResponseParser responseParser;
    DriversResponsePojo driversResponsePojo;
    ArrayList<DriversPojo> driversPojos;
    RecyclerView recyclerView;
    DriversAdapter driverAdapter;
    Activity activity;
    FloatingActionButton fabAdd;
    LinearLayout linearPlaceholder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_drivers, container, false);
        initUI(view);
        prepareDriversList();

        return view;

    }

    private void prepareDriversList() {
        ApiCall.getApiCall(activity,
                apiService.getDriverList(Support.getHeader(activity)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        try {
                            Log.e("onSuccess: ", objectResponse.body() + "");
                            driversResponsePojo = responseParser.getDriverList(objectResponse.body());
                            if (driversResponsePojo.getIsDriverAddable().equals("true")) {
                                fabAdd.setVisibility(View.VISIBLE);
                            } else
                                fabAdd.setVisibility(View.GONE);
                            driversPojos = driversResponsePojo.getDriverLists();
                            prepareRecyclerDriver();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(activity, message);
                    }
                });
    }

    private void prepareRecyclerDriver() {
        if (driversPojos.isEmpty())
            linearPlaceholder.setVisibility(View.VISIBLE);
        else
            linearPlaceholder.setVisibility(View.GONE);
        driverAdapter = new DriversAdapter(activity, DriversFragment.this, driversPojos);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(driverAdapter);
    }

    private void initUI(View view) {
        linearPlaceholder = view.findViewById(R.id.linearPlaceholder);
        recyclerView = view.findViewById(R.id.recyclerView);
        fabAdd = view.findViewById(R.id.fabAdd);

        driversPojos = new ArrayList<>();
        responseParser = new ResponseParser(activity);
        fabAdd.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabAdd:
                startActivityForResult(new Intent(activity, NewDriverActivity.class), REFRESH_SCREEN);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REFRESH_SCREEN && resultCode == RESULT_OK) {
            prepareDriversList();
        }
    }

    public void onClickDriverList(int position) {
        Intent intent = new Intent(activity, DriverDetailsActivity.class);
        intent.putExtra("detail", driversPojos.get(position));
        startActivityForResult(intent, REFRESH_SCREEN);
    }
}
