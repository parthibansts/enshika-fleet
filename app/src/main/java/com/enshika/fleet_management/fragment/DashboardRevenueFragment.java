package com.enshika.fleet_management.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.DashboardRevenueModel;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.DayAxisValueFormatter;
import com.enshika.fleet_management.utils.MyAxisValueFormatter;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.Support.formatDateIntoMMddyyyy;

/**
 * Created by Shamla Tech on 20-06-2018.
 */

public class DashboardRevenueFragment extends Fragment implements OnChartValueSelectedListener, View.OnClickListener {
    protected BarChart barChart;
    protected RectF mOnValueSelectedRectF = new RectF();
    String strStartDate = "", strEndDate = "";
    TextView txtDateRange;
    Activity activity;
    Date startDate, endDate;
    ArrayList<DashboardRevenueModel> dashboardRevenueModels;
    ResponseParser responseParser;

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard_revenue, container, false);
        initUI(view);
        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-M-yyyy", Locale.US);
            endDate = calendar.getTime();
            strEndDate = simpleDateFormat.format(calendar.getTime());
            calendar.add(Calendar.DATE, -5);
            startDate = calendar.getTime();
            strStartDate = simpleDateFormat.format(calendar.getTime());

            strStartDate = formatDateIntoMMddyyyy(strStartDate);
            strEndDate = formatDateIntoMMddyyyy(strEndDate);

            txtDateRange.setText(strStartDate + " to " + strEndDate);

            prepareRevenueItems();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void prepareRevenueItems() {
        ApiCall.getApiCall(activity, apiService.getDashboardRevenue(Support.getHeader(activity),
                Support.convertDateToMillisecs(strStartDate) + "",
                Support.convertDateToMillisecs(strEndDate) + ""), true, new ApiCall.OnApiResult() {
            @Override
            public void onSuccess(Response<Object> objectResponse) {
                dashboardRevenueModels = responseParser.getDashboardRevenue(objectResponse.body());
//                setData(getdays(strStartDate, strEndDate));
                for (int i = 0; i < dashboardRevenueModels.size(); i++) {
                    DashboardRevenueModel model = dashboardRevenueModels.get(i);
                    model.setDay_of_year(getDayOfYear(model.getDate()));
                    dashboardRevenueModels.set(i, model);
                }
                if (!dashboardRevenueModels.isEmpty()) {
                    setData(dashboardRevenueModels);
                    barChart.invalidate();
                } else {
                    barChart.setData(null);
                    barChart.invalidate();
                }
            }

            @Override
            public void onFailure(String message) {

            }
        });
    }

    private void setData(ArrayList<DashboardRevenueModel> dashboardRevenueModels) {


        ArrayList<BarEntry> yVals1 = new ArrayList<>();

        for (int i = 0; i < dashboardRevenueModels.size(); i++) {
            yVals1.add(new BarEntry(dashboardRevenueModels.get(i).getDay_of_year(), dashboardRevenueModels.get(i).getAmount()));
        }

        BarDataSet set1;

        if (barChart.getData() != null &&
                barChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals1, "Ghs");

            set1.setDrawIcons(false);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setBarWidth(0.9f);

            barChart.setData(data);
        }

    }

    @Override
    public void onNothingSelected() {
    }

    private void initUI(View view) {
        barChart = view.findViewById(R.id.barChart);
        txtDateRange = view.findViewById(R.id.txtDateRange);

        responseParser = new ResponseParser(activity);
        txtDateRange.setOnClickListener(this);
        barChart.setOnChartValueSelectedListener(this);
        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);


        barChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        barChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(true);

        barChart.setDrawGridBackground(false);
        // barChart.setDrawYLabels(false);

        IAxisValueFormatter xAxisFormatter = new DayAxisValueFormatter(barChart);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setLabelCount(4, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(4, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

        if (e == null)
            return;

        RectF bounds = mOnValueSelectedRectF;
        barChart.getBarBounds((BarEntry) e, bounds);
        MPPointF position = barChart.getPosition(e, YAxis.AxisDependency.LEFT);

        Log.i("bounds", bounds.toString());
        Log.i("position", position.toString());

        Log.i("x-index",
                "low: " + barChart.getLowestVisibleX() + ", high: "
                        + barChart.getHighestVisibleX());

        MPPointF.recycleInstance(position);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtDateRange:
                showDateRangePickerDialog();
                break;
        }
    }

    private void showDateRangePickerDialog() {
        Calendar startDateCalendar = Calendar.getInstance();
        startDateCalendar.setTime(startDate);

        Calendar endDateCalendar = Calendar.getInstance();
        endDateCalendar.setTime(endDate);

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
                        String tempStartDay = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        String tempEndDay = dayOfMonthEnd + "-" + (monthOfYearEnd + 1) + "-" + yearEnd;
                        Date tempStartDate = Support.stringToDateinddMYYYY(tempStartDay);
                        Date tempEndDate = Support.stringToDateinddMYYYY(tempEndDay);

                        if (tempStartDate.before(tempEndDate) || tempStartDate.equals(tempEndDate)) {
                            strStartDate = tempStartDay;
                            strEndDate = tempEndDay;
                            strStartDate = formatDateIntoMMddyyyy(strStartDate);
                            strEndDate = formatDateIntoMMddyyyy(strEndDate);
                            txtDateRange.setText(strStartDate + " to " + strEndDate);
                            prepareRevenueItems();

                            startDate = tempStartDate;
                            endDate = tempEndDate;

                        } else {
                            Toast.makeText(activity, "Invalid date selection!", Toast.LENGTH_SHORT).show();
                            showDateRangePickerDialog();
                        }

                    }
                },
                startDateCalendar.get(Calendar.YEAR),
                startDateCalendar.get(Calendar.MONTH),
                startDateCalendar.get(Calendar.DAY_OF_MONTH),
                endDateCalendar.get(Calendar.YEAR),
                endDateCalendar.get(Calendar.MONTH),
                endDateCalendar.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(activity.getFragmentManager(), "Datepickerdialog");

    }

    private float getDayOfYear(long milliseconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliseconds);
        return calendar.get(Calendar.DAY_OF_YEAR);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (barChart != null) {
                barChart.setVisibility(View.VISIBLE);
                barChart.animateY(500, Easing.EasingOption.EaseInSine);
            }
        } else {
            if (barChart != null)
                barChart.setVisibility(View.GONE);
        }

    }
}
