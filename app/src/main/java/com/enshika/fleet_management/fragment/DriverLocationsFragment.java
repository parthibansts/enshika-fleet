package com.enshika.fleet_management.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.DriverLocationsPojo;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.InfoWindowCustom;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;

/**
 * Created by Shamla Tech on 14-06-2018.
 */

public class DriverLocationsFragment extends Fragment implements OnMapReadyCallback, View.OnClickListener {
    ArrayList<DriverLocationsPojo> driverLocationsPojos;
    ArrayList<Marker> markers;
    GoogleMap googleMap;
    SupportMapFragment supportMapFragment;
    InfoWindowCustom infoWindowCustom;
    Activity activity;
    TextView txtMap, txtSatellite;
    ResponseParser responseParser;
    private int defaultZoom = 1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_locations, container, false);
        initViews(view);
        initializeMap();
        return view;
    }

    private void prepareDatas() {
        ApiCall.getApiCall(activity,
                apiService.getDriverLocation(Support.getHeader(activity)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        driverLocationsPojos = responseParser.getDriversLocationList(objectResponse.body());
                        prepareMarkers();
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(activity, message);
                    }
                });
    }

    private void initViews(View view) {
        txtMap = view.findViewById(R.id.txtMap);
        txtSatellite = view.findViewById(R.id.txtSatellite);

        responseParser = new ResponseParser(activity);
        txtMap.setOnClickListener(this);
        txtSatellite.setOnClickListener(this);
    }

    private void initializeMap() {
        if (googleMap == null) {
            supportMapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            supportMapFragment.getMapAsync(this);
        }
        infoWindowCustom = new InfoWindowCustom(activity);
    }

    @Override
    public void onMapReady(GoogleMap mGoogleMap) {
        try {
            googleMap = mGoogleMap;
            googleMap.setInfoWindowAdapter(infoWindowCustom);
            prepareDatas();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepareMarkers() {
        markers = new ArrayList<>();
        Marker marker;
        for (int i = 0; i < driverLocationsPojos.size(); i++) {
            MarkerOptions markerOptions = new MarkerOptions();
            int pin;
            if (driverLocationsPojos.get(i).getIsOnDuty().equalsIgnoreCase("true")) {
                pin = R.drawable.pin_green;
            } else
                pin = R.drawable.pin_red;
            markerOptions = markerOptions.position(new LatLng(driverLocationsPojos.get(i).getLatitude(),
                    driverLocationsPojos.get(i).getLongitude())).snippet("").title("")
                    .icon(Support.getPinIcon(activity, pin));
            marker = googleMap.addMarker(markerOptions);
            marker.setTag(driverLocationsPojos.get(i));
            markers.add(marker);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtMap:
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.txtSatellite:
                googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity.setTitle("Drivers Location");
    }
}
