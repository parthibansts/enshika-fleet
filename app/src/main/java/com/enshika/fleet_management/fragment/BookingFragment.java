package com.enshika.fleet_management.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.activity.BookingDetailActivity;
import com.enshika.fleet_management.adapter.BookingsAdapter;
import com.enshika.fleet_management.model.BookingsModel;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;

import java.util.ArrayList;

import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;

/**
 * Created by Shamla Tech on 11-06-2018.
 */

@SuppressLint("ValidFragment")
public class BookingFragment extends Fragment {
    RecyclerView recyclerView;
    BookingsAdapter bookingsAdapter;
    ArrayList<BookingsModel> bookingsModels;
    Activity activity;
    boolean isCurrentBooking = false;
    ResponseParser responseParser;
    LinearLayout linearPlaceholder;

    @SuppressLint("ValidFragment")
    public BookingFragment(boolean isCurrentBooking) {
        this.isCurrentBooking = isCurrentBooking;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookings, container, false);
        initUI(view);
        if (isCurrentBooking) {
            prepareCurrentBookingsList();
        } else {
            prepareBookingsList();
        }
        return view;

    }

    private void prepareBookingsList() {
        ApiCall.getApiCall(activity,
                apiService.getBookings(Support.getHeader(activity)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        bookingsModels = responseParser.getBookingsList(objectResponse.body());
                        prepareBookingsAdapter();
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(activity, message);
                    }
                });
    }

    private void prepareCurrentBookingsList() {
        ApiCall.getApiCall(activity,
                apiService.getCurrentBookings(Support.getHeader(activity)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        bookingsModels = responseParser.getBookingsList(objectResponse.body());
                        prepareBookingsAdapter();
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(activity, message);
                    }
                });
    }

    private void prepareBookingsAdapter() {
        if (bookingsModels.isEmpty()) {
            linearPlaceholder.setVisibility(View.VISIBLE);
        } else
            linearPlaceholder.setVisibility(View.GONE);
        bookingsAdapter = new BookingsAdapter(activity, BookingFragment.this, bookingsModels);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(bookingsAdapter);
    }

    private void initUI(View view) {
        linearPlaceholder = view.findViewById(R.id.linearPlaceholder);
        recyclerView = view.findViewById(R.id.recyclerView);
        responseParser = new ResponseParser(activity);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    public void onListClick(int position) {
        Intent intent = new Intent(activity, BookingDetailActivity.class);
        intent.putExtra("tourId", bookingsModels.get(position).getTourId());
        startActivity(intent);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (isCurrentBooking)
            activity.setTitle("Current Bookings");
        else
            activity.setTitle("Bookings");
    }
}
