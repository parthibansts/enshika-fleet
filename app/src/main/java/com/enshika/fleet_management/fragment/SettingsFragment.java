package com.enshika.fleet_management.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.activity.CardListActivity;
import com.enshika.fleet_management.activity.ManagePinActivity;
import com.enshika.fleet_management.activity.MenuPageActivity;
import com.enshika.fleet_management.activity.MobileMoneyActivity;
import com.enshika.fleet_management.activity.ProfileActivity;
import com.enshika.fleet_management.adapter.PaymentOptionsAdapter;
import com.enshika.fleet_management.model.PaymentOptionPojo;
import com.enshika.fleet_management.model.PaymentResponseModel;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;
import com.flipboard.bottomsheet.BottomSheetLayout;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.Vars.PAYMENT_CARD;
import static com.enshika.fleet_management.utils.Vars.PAYMENT_MOMO;
import static com.enshika.fleet_management.utils.Vars.REFRESH_SCREEN;

/**
 * Created by Shamla Tech on 04-07-2018.
 */

public class SettingsFragment extends Fragment implements View.OnClickListener {
    Activity activity;
    RelativeLayout rlProfile, rlPaymentOptions, rlLogout, rlPin;
    BottomSheetLayout bottomSheet;
    CircleImageView ciFleet;
    ResponseParser responseParser;
    ArrayList<PaymentOptionPojo> paymentPojos;
    View bottomSheetView;
    RecyclerView recyclerPaymentOptions;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        initUI(view);
        Support.loadImage(Support.getPref(activity, Vars.PHOTO_URL), ciFleet, R.drawable.ic_user_placeholder_white);
        return view;
    }

    private void initUI(View view) {
        ciFleet = view.findViewById(R.id.ciFleet);
        rlProfile = view.findViewById(R.id.rlProfile);
        rlPaymentOptions = view.findViewById(R.id.rlPaymentOptions);
        rlLogout = view.findViewById(R.id.rlLogout);
        bottomSheet = view.findViewById(R.id.bottomsheet);
        rlPin = view.findViewById(R.id.rlPin);
        bottomSheetView = LayoutInflater.from(activity).inflate(R.layout.bottom_payment_options, bottomSheet, false);
        recyclerPaymentOptions = bottomSheetView.findViewById(R.id.recyclerPaymentOptions);

        rlPin.setOnClickListener(this);
        rlProfile.setOnClickListener(this);
        rlPaymentOptions.setOnClickListener(this);
        rlLogout.setOnClickListener(this);

        paymentPojos = new ArrayList<>();
        responseParser = new ResponseParser(activity);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity.setTitle("Settings");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlProfile:
                startActivityForResult(new Intent(activity, ProfileActivity.class), REFRESH_SCREEN);
                break;
            case R.id.rlPaymentOptions:
                showAddMoneyOptions();
                break;
            case R.id.rlPin:
                startActivity(new Intent(activity, ManagePinActivity.class));
                break;
            case R.id.rlLogout:
                Support.showAlertWithNegativeButton(activity, "Are you sure do you want to logout?", true, "Logout");
                break;

        }
    }

    private void preparePaymentOptions() {
        ApiCall.getApiCall(activity,
                apiService.getPaymentModes(Support.getHeader(activity)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        paymentPojos = new ArrayList<>();
                        PaymentResponseModel paymentModesResponseModel = responseParser.getPaymentModel(objectResponse.body());
                        ArrayList<PaymentOptionPojo> tempPaymentPojos = paymentModesResponseModel.getData().getPaymentmodes();
                        for (int i = 0; i < tempPaymentPojos.size(); i++) {
                            if (tempPaymentPojos.get(i).getPaymentCode() == PAYMENT_CARD
                                    || tempPaymentPojos.get(i).getPaymentCode() == PAYMENT_MOMO) {
                                paymentPojos.add(tempPaymentPojos.get(i));
                            }
                        }
                        showAddMoneyOptions();
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(activity, message);
                    }
                });
    }

    private void showAddMoneyOptions() {
        if (!paymentPojos.isEmpty()) {
            preparePaymentOptionsAdapter();
            bottomSheet.showWithSheetView(bottomSheetView);
        } else {
            preparePaymentOptions();
        }
    }

    private void preparePaymentOptionsAdapter() {
        PaymentOptionsAdapter paymentOptionsAdapter = new PaymentOptionsAdapter(activity, SettingsFragment.this, paymentPojos);
        recyclerPaymentOptions.setLayoutManager(new LinearLayoutManager(activity));
        recyclerPaymentOptions.setAdapter(paymentOptionsAdapter);
    }

    public void onClickPayment(int position) {
        if (bottomSheet != null && bottomSheet.isSheetShowing()) {
            bottomSheet.dismissSheet();
        }
        if (paymentPojos.get(position).getPaymentCode() == PAYMENT_CARD) {
            Intent intent = new Intent(activity, CardListActivity.class);
            startActivity(intent);
        } else if (paymentPojos.get(position).getPaymentCode() == PAYMENT_MOMO) {
            Intent intent = new Intent(activity, MobileMoneyActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REFRESH_SCREEN && resultCode == RESULT_OK) {
            ((MenuPageActivity) activity).prepareNavigationData();
            Support.loadImage(Support.getPref(activity, Vars.PHOTO_URL), ciFleet, R.drawable.ic_user_placeholder_white);
        }
    }
}
