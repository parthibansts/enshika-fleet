package com.enshika.fleet_management.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.activity.BookingDetailActivity;
import com.enshika.fleet_management.adapter.DriverRidesAdapter;
import com.enshika.fleet_management.model.DriverRidePojo;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;

import java.util.ArrayList;

import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;

/**
 * Created by ADMIN on 20-Apr-18.
 */

public class DriverRidesFragment extends Fragment {
    Activity activity;
    RecyclerView rvRides;
    DriverRidesAdapter driverRidesAdapter;
    ArrayList<DriverRidePojo> driverRidePojos;
    String strDriverId = "";
    ResponseParser responseParser;
    LinearLayout linearPlaceholder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_rides, container, false);
        initUI(view);
        getBundleDetails();
        prepareRideList();

        return view;
    }

    private void getBundleDetails() {
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey("driverId")) {
            strDriverId = bundle.getString("driverId");
        }
    }

    private void prepareRidesAdapter() {
        if (driverRidePojos.isEmpty())
            linearPlaceholder.setVisibility(View.VISIBLE);
        else
            linearPlaceholder.setVisibility(View.GONE);
        driverRidesAdapter = new DriverRidesAdapter(activity, DriverRidesFragment.this, driverRidePojos);
        rvRides.setLayoutManager(new LinearLayoutManager(activity));
        rvRides.setAdapter(driverRidesAdapter);
    }

    private void prepareRideList() {
        ApiCall.getApiCall(activity,
                apiService.getDriverRideList(strDriverId, Support.getHeader(activity)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");
                        driverRidePojos = responseParser.getDriverRidesList(objectResponse.body());
                        prepareRidesAdapter();
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(activity, message);
                    }
                });
    }

    private void initUI(View view) {
        linearPlaceholder = view.findViewById(R.id.linearPlaceholder);
        rvRides = view.findViewById(R.id.rvRides);
        driverRidePojos = new ArrayList<>();
        responseParser = new ResponseParser(activity);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity.setTitle("Ride History");
    }

    public void onClickDriverRideList(int position) {
        Intent intent = new Intent(activity, BookingDetailActivity.class);
        intent.putExtra("tourId", driverRidePojos.get(position).getTourId());
        startActivity(intent);
    }
}
