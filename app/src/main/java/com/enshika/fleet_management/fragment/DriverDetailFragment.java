package com.enshika.fleet_management.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.activity.DriverDetailsActivity;
import com.enshika.fleet_management.activity.NewDriverActivity;
import com.enshika.fleet_management.adapter.DriverDocumentsAdapter;
import com.enshika.fleet_management.model.DriverDetailModel;
import com.enshika.fleet_management.utils.ApiCall;
import com.enshika.fleet_management.utils.ResponseParser;
import com.enshika.fleet_management.utils.Support;
import com.enshika.fleet_management.utils.Vars;
import com.hbb20.CountryCodePicker;

import java.util.LinkedHashMap;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.Vars.REFRESH_SCREEN;

/**
 * Created by ADMIN on 20-Apr-18.
 */

public class DriverDetailFragment extends Fragment implements View.OnClickListener {
    LinearLayout linearPersonalDetails;
    FloatingActionButton fabEdit;
    Activity activity;
    String strDriverId = "";
    DriverDetailModel driverDetailModel;
    ResponseParser responseParser;
    CountryCodePicker ccp_country_id;
    EditText edtFirstName, edtLastName, edtEmail, edtDateOfBirth, edtPhoneNumber, edtId, edtRegion,
            edtStreetOne, edtStreetTwo, edtCountry, edtState, edtCity, edtBankName, edtAccountNo, edtAccountName,
            edtBankCode, edtAccountType, edtCar, edtDrivingLicense, edtInsuranceDateRange, edtLicenseExpiry;
    RadioButton radioMale, radioFemale;
    //    ImageView imgPoliceClearance, imgPassport, imgLicenseBack, imgLicenseFront, imgId, imgProfile;
    ViewPager pagerDocuments;
    CircleIndicator indicator;
    RelativeLayout relativeDocumentsHead;
    LinearLayout linearDocuments;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_detail, container, false);
        initUI(view);
//        disableEnableControls(false, linearPersonalDetails);
        getBundleDetails();
        return view;
    }

    private void getBundleDetails() {
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey("driverId")) {
            strDriverId = bundle.getString("driverId");
            prepareDriverDetails();
        }
    }

    private void prepareDriverDetails() {

        ApiCall.getApiCall(activity,
                apiService.getDriverDetail(strDriverId, Support.getHeader(activity)),
                true, new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        Log.e("onSuccess: ", objectResponse.body() + "");

                        driverDetailModel = responseParser.getDriverDetail(objectResponse.body());
                        edtFirstName.setText(driverDetailModel.getFirstName());
                        edtLastName.setText(driverDetailModel.getLastName());
                        edtEmail.setText(driverDetailModel.getEmail());
                        edtDateOfBirth.setText(Support.getDateFromLong(driverDetailModel.getDrivingLicenseModel().getDob(), Vars.SHORT_DATE_FORMAT));
                        edtPhoneNumber.setText(driverDetailModel.getPhoneNo());
                        ccp_country_id.setCountryForPhoneCode(Integer.parseInt(driverDetailModel.getPhoneNoCode()
                                .replace("+", "")));
                        if (driverDetailModel.getGender() != null)
                            if (driverDetailModel.getGender().equalsIgnoreCase("male"))
                                radioMale.setChecked(true);
                            else
                                radioFemale.setChecked(true);
                        edtId.setText(driverDetailModel.getDrivingLicenseModel().getSocialSecurityNumber());
                        edtRegion.setText(driverDetailModel.getRegion());
                        edtStreetOne.setText(driverDetailModel.getMailAddressLineOne());
                        edtStreetTwo.setText(driverDetailModel.getMailAddressLineTwo());
                        edtCountry.setText(driverDetailModel.getMailCountryId());
                        edtState.setText(driverDetailModel.getMailStateId());
                        edtCity.setText(driverDetailModel.getMailCityId());
                        edtBankName.setText(driverDetailModel.getDriverBankDetails().getBankName());
                        edtAccountNo.setText(driverDetailModel.getDriverBankDetails().getAccountNumber());
                        edtAccountName.setText(driverDetailModel.getDriverBankDetails().getAccountName());
                        edtBankCode.setText(driverDetailModel.getDriverBankDetails().getRoutingNumber());
                        edtAccountType.setText(driverDetailModel.getDriverBankDetails().getType());
                        if (driverDetailModel.getCarModel() != null)
                            edtCar.setText(driverDetailModel.getCarModel().getMake() + ", " + driverDetailModel.getCarModel().getModelName()
                                    + ", " + driverDetailModel.getCarModel().getCarPlateNo() + ", "
                                    + driverDetailModel.getCarModel().getCarColor()
                                    + ", " + driverDetailModel.getCarModel().getOwner());
                        edtDrivingLicense.setText(driverDetailModel.getDrivingLicenseModel().getDriverLicenseCardNumber());
                        if (driverDetailModel.getDrivingLicenseModel()
                                .getInsuranceEffectiveDate() != 0 && driverDetailModel.getDrivingLicenseModel()
                                .getInsuranceExpirationDate() != 0)
                            edtInsuranceDateRange.setText(Support.getDateFromLong(driverDetailModel.getDrivingLicenseModel()
                                    .getInsuranceEffectiveDate(), Vars.SHORT_DATE_FORMAT) + " - " +
                                    Support.getDateFromLong(driverDetailModel.getDrivingLicenseModel()
                                            .getInsuranceExpirationDate(), Vars.SHORT_DATE_FORMAT));
                        if (driverDetailModel.getDrivingLicenseModel().getLicenceExpirationDate() != 0)
                            edtLicenseExpiry.setText(Support.getDateFromLong(driverDetailModel.getDrivingLicenseModel().getLicenceExpirationDate()
                                    , Vars.SHORT_DATE_FORMAT));
//                        Support.loadImage(driverDetailModel.getDrivingLicenseModel().getDrivingLicensePhotoUrl(), imgLicenseFront, R.drawable.ic_placeholder_gallery);
//                        Support.loadImage(driverDetailModel.getDrivingLicenseModel().getDrivingLicenseBackPhotoUrl(), imgLicenseBack, R.drawable.ic_placeholder_gallery);
//                        Support.loadImage(driverDetailModel.getDrivingLicenseModel().getCriminalHistoryPhotoUrl(), imgPoliceClearance, R.drawable.ic_placeholder_gallery);
//                        Support.loadImage(driverDetailModel.getDrivingLicenseModel().getBirthAccreditationPassportPhotoUrl(), imgPassport, R.drawable.ic_placeholder_gallery);
//                        Support.loadImage(driverDetailModel.getDrivingLicenseModel().getSocilaSecurityPhotoUrl(), imgId, R.drawable.ic_placeholder_gallery);
//                        Support.loadImage(driverDetailModel.getPhotoUrl(), imgProfile, R.drawable.ic_placeholder_gallery);
                        ((DriverDetailsActivity) activity).loadDriverDatas(driverDetailModel);

                        LinkedHashMap<String, String> images = new LinkedHashMap<>();
                        if (driverDetailModel.getDrivingLicenseModel().getSocilaSecurityPhotoUrl() != null)
                            images.put("Id", driverDetailModel.getDrivingLicenseModel().getSocilaSecurityPhotoUrl());
                        if (driverDetailModel.getDrivingLicenseModel().getDrivingLicensePhotoUrl() != null)
                            images.put("Driving License Front", driverDetailModel.getDrivingLicenseModel().getDrivingLicensePhotoUrl());
                        if (driverDetailModel.getDrivingLicenseModel().getDrivingLicenseBackPhotoUrl() != null)
                            images.put("Driving License Back", driverDetailModel.getDrivingLicenseModel().getDrivingLicenseBackPhotoUrl());
                        if (driverDetailModel.getDrivingLicenseModel().getBirthAccreditationPassportPhotoUrl() != null)
                            images.put("Driver Accreditation / Passport / Birth Certification",
                                    driverDetailModel.getDrivingLicenseModel().getBirthAccreditationPassportPhotoUrl());
                        if (driverDetailModel.getDrivingLicenseModel().getCriminalHistoryPhotoUrl() != null)
                            images.put("Police Clearance Certificate", driverDetailModel.getDrivingLicenseModel().getCriminalHistoryPhotoUrl());
                        if (!images.isEmpty()) {
                            relativeDocumentsHead.setVisibility(View.VISIBLE);
                            linearDocuments.setVisibility(View.VISIBLE);
                            DriverDocumentsAdapter driverDocumentsAdapter = new DriverDocumentsAdapter(activity, images);
                            pagerDocuments.setAdapter(driverDocumentsAdapter);
                            indicator.setViewPager(pagerDocuments);
                        } else {
                            relativeDocumentsHead.setVisibility(View.GONE);
                            linearDocuments.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(activity, message);
                    }
                });
    }

    private void initUI(View view) {
        relativeDocumentsHead = view.findViewById(R.id.relativeDocumentsHead);
        linearDocuments = view.findViewById(R.id.linearDocuments);
        pagerDocuments = view.findViewById(R.id.pagerDocuments);
        indicator = view.findViewById(R.id.indicator);
//        imgProfile = view.findViewById(R.id.imgProfile);
        edtCar = view.findViewById(R.id.edtCar);
        edtDrivingLicense = view.findViewById(R.id.edtDrivingLicense);
        edtInsuranceDateRange = view.findViewById(R.id.edtInsuranceDateRange);
        edtLicenseExpiry = view.findViewById(R.id.edtLicenseExpiry);
//        imgPoliceClearance = view.findViewById(R.id.imgPoliceClearance);
//        imgPassport = view.findViewById(R.id.imgPassport);
//        imgLicenseBack = view.findViewById(R.id.imgLicenseBack);
//        imgLicenseFront = view.findViewById(R.id.imgLicenseFront);
//        imgId = view.findViewById(R.id.imgId);
        edtFirstName = view.findViewById(R.id.edtFirstName);
        edtLastName = view.findViewById(R.id.edtLastName);
        edtEmail = view.findViewById(R.id.edtEmail);
        edtDateOfBirth = view.findViewById(R.id.edtDateOfBirth);
        ccp_country_id = view.findViewById(R.id.ccp_country_id);
        edtPhoneNumber = view.findViewById(R.id.edtPhoneNumber);
        radioMale = view.findViewById(R.id.radioMale);
        radioFemale = view.findViewById(R.id.radioFemale);
        edtId = view.findViewById(R.id.edtId);
        edtRegion = view.findViewById(R.id.edtRegion);
        edtStreetOne = view.findViewById(R.id.edtStreetOne);
        edtStreetTwo = view.findViewById(R.id.edtStreetTwo);
        edtCountry = view.findViewById(R.id.edtCountry);
        edtState = view.findViewById(R.id.edtState);
        edtCity = view.findViewById(R.id.edtCity);
        edtBankName = view.findViewById(R.id.edtBankName);
        edtAccountNo = view.findViewById(R.id.edtAccountNo);
        edtAccountName = view.findViewById(R.id.edtAccountName);
        edtBankCode = view.findViewById(R.id.edtBankCode);
        edtAccountType = view.findViewById(R.id.edtAccountType);


        fabEdit = view.findViewById(R.id.fabEdit);
        linearPersonalDetails = view.findViewById(R.id.linearPersonalDetails);
        responseParser = new ResponseParser(activity);
        fabEdit.setOnClickListener(this);
    }

    private void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabEdit:
                Intent intent = new Intent(activity, NewDriverActivity.class);
                intent.putExtra("id", strDriverId);
                intent.putExtra("detail", driverDetailModel);
                startActivityForResult(intent, REFRESH_SCREEN);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REFRESH_SCREEN && resultCode == RESULT_OK) {
            prepareDriverDetails();
        }
    }
}
