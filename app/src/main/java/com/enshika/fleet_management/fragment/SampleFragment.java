package com.enshika.fleet_management.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.enshika.fleet_management.R;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.truizlop.fabreveallayout.FABRevealLayout;
import com.truizlop.fabreveallayout.OnRevealChangeListener;

/**
 * Created by Shamla Tech on 04-07-2018.
 */

public class SampleFragment extends Fragment implements View.OnClickListener {
    Activity activity;
    BottomSheetLayout bottomSheet;
    FloatingActionButton fabAdd;
    FABRevealLayout fab_reveal_layout;
    FloatingActionButton fabAddMoney, fab_close;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sample, container, false);
        initUI(view);

        fab_reveal_layout = view.findViewById(R.id.fab_reveal_layout);
        configureFABReveal(fab_reveal_layout);
        return view;
    }

    private void initUI(View view) {
        fabAdd = view.findViewById(R.id.fabAdd);
        fabAddMoney = view.findViewById(R.id.fabAddMoney);
        fab_close = view.findViewById(R.id.fab_close);
        bottomSheet = view.findViewById(R.id.bottomsheet);

        fab_close.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_close:
//                fab_reveal_layout.revealMainView();
                bottomSheet.showWithSheetView(LayoutInflater.from(activity).inflate(R.layout.bottom_payment_options, bottomSheet, false));
                break;
        }
    }

    private void configureFABReveal(FABRevealLayout fabRevealLayout) {
        fabRevealLayout.setOnRevealChangeListener(new OnRevealChangeListener() {
            @Override
            public void onMainViewAppeared(FABRevealLayout fabRevealLayout, View mainView) {
                fab_close.setVisibility(View.GONE);
            }

            @Override
            public void onSecondaryViewAppeared(final FABRevealLayout fabRevealLayout, View secondaryView) {
                fab_close.setVisibility(View.VISIBLE);
            }
        });
    }
}
