package com.enshika.fleet_management.utils;

import com.enshika.fleet_management.model.CarDetailPojo;
import com.enshika.fleet_management.model.CardModel;
import com.enshika.fleet_management.model.CardSaveReqModel;
import com.enshika.fleet_management.model.CardValidateReqModel;
import com.enshika.fleet_management.model.DepositReqModel;
import com.enshika.fleet_management.model.NewDriverModel;
import com.enshika.fleet_management.model.NewMobileMoneyModel;
import com.enshika.fleet_management.model.PayCardModel;
import com.enshika.fleet_management.model.PayMobModel;
import com.enshika.fleet_management.model.ProfilePojo;
import com.enshika.fleet_management.model.SaveTransactionModel;
import com.enshika.fleet_management.model.UserModel;
import com.enshika.fleet_management.model.WalletSetPinPojo;
import com.enshika.fleet_management.model.WalletUpdatePinPojo;
import java.util.Map;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.HTTP;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Shamla Tech on 26-06-2018.
 */

public interface ApiList {

    @retrofit2.http.GET("api/app-settings.json")
    retrofit2.Call<Object> getAppSettings();

    @retrofit2.http.POST("api/login.json")
    retrofit2.Call<Object> makeLogin(@retrofit2.http.Body UserModel req);

    @retrofit2.http.POST("api/user/fleet-forgot-password.json")
    retrofit2.Call<Object> forgotPassword(@retrofit2.http.Body UserModel req);

    @retrofit2.http.GET("api/fleet-manager/car-list.json")
    retrofit2.Call<Object> getCarList(@HeaderMap Map<String, String> headers);

    @retrofit2.http.GET("api/fleet-manager/driver-list.json")
    retrofit2.Call<Object> getDriverList(@HeaderMap Map<String, String> headers);

    @retrofit2.http.GET("api/car-type.json")
    retrofit2.Call<Object> getCarType(@HeaderMap Map<String, String> headers);

    @retrofit2.http.GET("api/fleet-manager/car-details/{carId}.json")
    retrofit2.Call<Object> getCarDetail(@Path("carId") String carId, @HeaderMap Map<String, String> headers);

    @retrofit2.http.GET("api/fleet-manager/driver-details/{driverId}.json")
    retrofit2.Call<Object> getDriverDetail(@Path("driverId") String driverId, @HeaderMap Map<String, String> headers);

    @retrofit2.http.GET("api/fleet-manager/driver-ride-list/{driverId}.json")
    retrofit2.Call<Object> getDriverRideList(@Path("driverId") String driverId, @HeaderMap Map<String, String> headers);

    @retrofit2.http.GET("api/fleet-manager/drivers-location.json")
    retrofit2.Call<Object> getDriverLocation(@HeaderMap Map<String, String> headers);

    @retrofit2.http.GET("api/fleet-manager/current-booking-list.json")
    retrofit2.Call<Object> getCurrentBookings(@HeaderMap Map<String, String> headers);

    @retrofit2.http.GET("api/fleet-manager/previous-booking-list.json")
    retrofit2.Call<Object> getBookings(@HeaderMap Map<String, String> headers);

    @retrofit2.http.POST("api/fleet-manager/add-car.json")
    retrofit2.Call<Object> addCar(@HeaderMap Map<String, String> headers, @Body CarDetailPojo carDetailPojo);

    @retrofit2.http.PUT("api/fleet-manager/update-car.json")
    retrofit2.Call<Object> updateCar(@HeaderMap Map<String, String> headers, @Body CarDetailPojo carDetailPojo);

    @retrofit2.http.GET("api/fleet-manager/region.json")
    retrofit2.Call<Object> getRegions(@HeaderMap Map<String, String> headers);

    @retrofit2.http.GET("api/fleet-manager/driverOnOff.json")
    retrofit2.Call<Object> getDriversStatus(@HeaderMap Map<String, String> headers);

    @retrofit2.http.POST("api/fleet-manager/add-driver.json")
    retrofit2.Call<Object> addDriver(@HeaderMap Map<String, String> headers, @retrofit2.http.Body NewDriverModel req);

    @retrofit2.http.PUT("api/fleet-manager/update-driver.json")
    retrofit2.Call<Object> updateDriver(@HeaderMap Map<String, String> headers, @retrofit2.http.Body NewDriverModel req);

    @retrofit2.http.GET("api/fleet-manager/individual-booking-list/{tourId}.json")
    retrofit2.Call<Object> getBookingDetails(@HeaderMap Map<String, String> headers, @Path("tourId") String tourId);

    @retrofit2.http.GET("api/fleet-manager/dashboard-summary/{startDate}/{endDate}.json")
    retrofit2.Call<Object> getDashboardSummary(@HeaderMap Map<String, String> headers, @Path("startDate") String startDate,
                                               @Path("endDate") String endDate);

    @retrofit2.http.GET("api/fleet-manager/revenue-reports/{startDate}/{endDate}.json")
    retrofit2.Call<Object> getDashboardRevenue(@HeaderMap Map<String, String> headers, @Path("startDate") String startDate,
                                               @Path("endDate") String endDate);

    @Multipart
    @retrofit2.http.POST("api/fleet-manager/upload-car-images/{carId}.json")
    retrofit2.Call<Object> uploadCarImages(@HeaderMap Map<String, String> headers, @Path("carId") String carId,
                                           @Part MultipartBody.Part image);

    @Multipart
    @retrofit2.http.POST("api/fleet-manager/upload-driver-images/{userId}.json")
    retrofit2.Call<Object> uploadDriverImages(@HeaderMap Map<String, String> headers, @Path("userId") String userId,
                                              @Part MultipartBody.Part image);

    @retrofit2.http.GET("api/fleet-manager/wallet-balance.json")
    retrofit2.Call<Object> getWalletBalance(@HeaderMap Map<String, String> headers);

    @retrofit2.http.GET("api/fleet-manager/transaction-history.json")
    retrofit2.Call<Object> getTransactionHistory(@HeaderMap Map<String, String> headers);

    @retrofit2.http.PUT("api/fleet-manager/update-fleet-profile.json")
    retrofit2.Call<Object> updateFleetProfile(@HeaderMap Map<String, String> headers, @Body ProfilePojo profilePojo);

    @Multipart
    @retrofit2.http.POST("api/fleet-manager/update-profile-image/{userId}.json")
    retrofit2.Call<Object> updateProfileImage(@HeaderMap Map<String, String> headers, @Path("userId") String userId,
                                              @Part MultipartBody.Part image);

    @retrofit2.http.POST("api/wallets/resetpin.json")
    retrofit2.Call<Object> setPin(@HeaderMap Map<String, String> headers, @retrofit2.http.Body WalletSetPinPojo walletPinPojo);

    @retrofit2.http.POST("api/wallets/modifypin.json")
    retrofit2.Call<Object> updatePin(@HeaderMap Map<String, String> headers, @retrofit2.http.Body WalletUpdatePinPojo walletPinPojo);

    @retrofit2.http.POST("api/wallets/validatepin.json")
    retrofit2.Call<Object> validatePin(@HeaderMap Map<String, String> headers, @retrofit2.http.Body WalletSetPinPojo walletPinPojo);

    @retrofit2.http.GET("api/payment-mode/paymentmode.do")
    retrofit2.Call<Object> getPaymentModes(@HeaderMap Map<String, String> headers);

    @retrofit2.http.GET("api/operator.do")
    retrofit2.Call<Object> getMobileMoney(@HeaderMap Map<String, String> headers, @Query("userId") String userId);

    @retrofit2.http.GET("api/mobile-money-operator/operators.do")
    retrofit2.Call<Object> getMobileOperatorList(@HeaderMap Map<String, String> headers);

    @retrofit2.http.POST("api/operator.do")
    retrofit2.Call<Object> addMobileMoney(@HeaderMap Map<String, String> headers,
                                          @retrofit2.http.Body NewMobileMoneyModel newMobileMoneyModel);

    @retrofit2.http.POST("api/operator/delete.do")
    retrofit2.Call<Object> removeMobileMoney(@HeaderMap Map<String, String> headers,
                                             @retrofit2.http.Body NewMobileMoneyModel newMobileMoneyModel);

    @retrofit2.http.POST("api/operator/resendotp.do")
    retrofit2.Call<Object> sendOtp(@HeaderMap Map<String, String> headers,
                                   @retrofit2.http.Body NewMobileMoneyModel newMobileMoneyModel);

    @retrofit2.http.POST("api/operator/verifyotp.do")
    retrofit2.Call<Object> verifyOtp(@HeaderMap Map<String, String> headers,
                                     @retrofit2.http.Body NewMobileMoneyModel newMobileMoneyModel);

    @retrofit2.http.GET("api/merchants.do")
    retrofit2.Call<Object> getMerchantDetails(@HeaderMap Map<String, String> headers,
                                              @Query("userId") String userId);

    @retrofit2.http.POST("api/wallettxn/deposit.do")
    retrofit2.Call<Object> depositAmount(@HeaderMap Map<String, String> headers, @retrofit2.http.Body DepositReqModel depositReqModel);

    @retrofit2.http.POST("api/wallets/validate.do")
    retrofit2.Call<Object> isValidCard(@retrofit2.http.Body CardValidateReqModel cardValidatePojo);

    @retrofit2.http.POST("api/transaction/process")
    retrofit2.Call<Object> makePaymentMoMo(@HeaderMap Map<String, String> headers, @retrofit2.http.Body PayMobModel makeTransactionModel);

    @retrofit2.http.POST("api/wallets/save.do")
    retrofit2.Call<Object> saveCard(@retrofit2.http.Body CardSaveReqModel cardSaveReqModel);

    @retrofit2.http.POST("api/wallets/payment.do")
    retrofit2.Call<Object> makePaymentCard(@HeaderMap Map<String, String> headers, @retrofit2.http.Body PayCardModel payCardModel);

    @HTTP(method = "DELETE", path = "api/wallets/remove.do", hasBody = true)
    retrofit2.Call<Object> removeCard(@retrofit2.http.Body CardModel cardModel);

    @retrofit2.http.POST("api/fleet-manager/wallet-txn-status.json")
    retrofit2.Call<Object> saveTransaction(@retrofit2.http.Body SaveTransactionModel cardSaveReqModel);

    @retrofit2.http.GET("api/wallets/{merchant_id}/{user_id}")
    retrofit2.Call<Object> getCardList(@Path("merchant_id") String merchant_id, @Path("user_id") String user_id);

    @retrofit2.http.PUT("api/user/verification.json")
    retrofit2.Call<Object> updateVerification(@HeaderMap Map<String, String> headers,
                                              @Query("verificationCode") String verificationCode);

    @retrofit2.http.POST("api/user/code-resent.json")
    retrofit2.Call<Object> resendVerification(@HeaderMap Map<String, String> headers);

    @retrofit2.http.PUT("api/user/phone-update.json")
    retrofit2.Call<Object> updatePhone(@HeaderMap Map<String, String> headers,
                                       @Query("phoneNoCode") String phoneNoCode,
                                       @Query("phoneNo") String phoneNo);

    @retrofit2.http.GET("api/fleet-manager/getInsufficientFleetFeeCount.json")
    retrofit2.Call<Object> getInsufficientFleetFeeCount(@HeaderMap Map<String, String> headers);

    @retrofit2.http.GET("api/fleet-manager/getFleetFee/{sortby}.json")
    retrofit2.Call<Object> getDriversFleetFee(@HeaderMap Map<String, String> headers, @Path("sortby") String sort_by,
                                              @Query("searchText") String searchText);

}
