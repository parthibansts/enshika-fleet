package com.enshika.fleet_management.utils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Shamla Tech on 28-06-2018.
 */

public class RetroHeaderInterceptor implements Interceptor  {
    String sessionKey = "";

    public void RetroHeaderInterceptor(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request originalRequest = chain.request();
        if (!"/posts".contains(originalRequest.url() + "")) {
            return chain.proceed(originalRequest);
        }

        Request newRequest = originalRequest.newBuilder()
                .header("x-api-key", sessionKey)
                .build();

        return chain.proceed(newRequest);
    }
}