package com.enshika.fleet_management.utils;

/**
 * Created by Shamla Tech on 10-08-2018.
 */

public interface SearchViewInterface {
    void onTextChanged(String text);

    void onSearch(String text);

    void searchViewOpened();

    void searchViewClosed();
}
