package com.enshika.fleet_management.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.activity.LoginActivity;
import com.enshika.fleet_management.model.UserModel;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Response;

import static com.enshika.fleet_management.utils.ApiCall.apiService;
import static com.enshika.fleet_management.utils.Vars.DEVELOPMENT_URL;

/**
 * Created by Vino on 20-03-2018.
 */

public class Support {

    public static View viewAviIndicator;
    static String numbers = "[0-9]+";
    static SharedPreferences sharedpreferences;

    public static void showLoader(Activity activity) {
        viewAviIndicator = activity.findViewById(R.id.viewAviIndicator);
        viewAviIndicator.setVisibility(View.VISIBLE);
        AVLoadingIndicatorView aviIndicator = activity.findViewById(R.id.aviIndicator);
        aviIndicator.show();

    }

    public static void hideLoader(Activity activity) {
        viewAviIndicator = activity.findViewById(R.id.viewAviIndicator);
        viewAviIndicator.setVisibility(View.GONE);
        AVLoadingIndicatorView aviIndicator = activity.findViewById(R.id.aviIndicator);
        aviIndicator.hide();
    }

    public static void setPref(Context con, String Key, String Value) {
        try {
            sharedpreferences = con.getSharedPreferences(Vars.MyPref, Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = sharedpreferences.edit();
            edit.putString(Key, Value);
            edit.commit();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public static void setPref(Context con, String Key, int Value) {
        try {
            sharedpreferences = con.getSharedPreferences(Vars.MyPref, Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = sharedpreferences.edit();
            edit.putInt(Key, Value).apply();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public static void setPref(Context con, String Key, boolean Value) {
        sharedpreferences = con.getSharedPreferences(Vars.MyPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putBoolean(Key, Value);
        edit.apply();
    }

    public static void clearAllPref(Context con) {
        sharedpreferences = con.getSharedPreferences(Vars.MyPref, Context.MODE_PRIVATE);
        sharedpreferences.edit().clear().apply();
    }

    public static String getPref(Context con, String Key) {
        String Value = "";
        try {
            sharedpreferences = con.getSharedPreferences(Vars.MyPref, Context.MODE_PRIVATE);
            if (sharedpreferences.contains(Key)) {
                Value = sharedpreferences.getString(Key, "");
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return Value;
    }

    public static int getPrefInt(Context con, String Key) {
        int Value = 0;
        try {
            sharedpreferences = con.getSharedPreferences(Vars.MyPref, Context.MODE_PRIVATE);
            if (sharedpreferences.contains(Key)) {
                Value = sharedpreferences.getInt(Key, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return Value;
    }

    public static boolean getPrefBoolean(Context con, String Key) {
        sharedpreferences = con.getSharedPreferences(Vars.MyPref, Context.MODE_PRIVATE);
        boolean Value = false;
        if (sharedpreferences.contains(Key)) {
            Value = sharedpreferences.getBoolean(Key, false);
        }
        return Value;
    }

    public static void setSessionKey(Context con, String Value) {
        sharedpreferences = con.getSharedPreferences(Vars.MyPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(Vars.SESSION_KEY, Value);
        edit.apply();
    }

    public static String getSessionKey(Context con) {
        String Value = "";
        try {
            sharedpreferences = con.getSharedPreferences(Vars.MyPref, Context.MODE_PRIVATE);
            if (sharedpreferences.contains(Vars.SESSION_KEY)) {
                Value = sharedpreferences.getString(Vars.SESSION_KEY, "");
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return Value;
    }


    public static void setCurrencySymbol(Context con, String Value) {
        sharedpreferences = con.getSharedPreferences(Vars.MyPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(Vars.CURRENCY_SYMBOL, Value);
        edit.apply();
    }

    public static String getCurrencySymbol(Context con) {
        String Value = "";
        try {
            sharedpreferences = con.getSharedPreferences(Vars.MyPref, Context.MODE_PRIVATE);
            if (sharedpreferences.contains(Vars.CURRENCY_SYMBOL)) {
                Value = sharedpreferences.getString(Vars.CURRENCY_SYMBOL, "");
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return Value;
    }

    public static Map<String, String> getHeader(Context context) {
        Map<String, String> map = new HashMap<>();
        map.put("x-api-key", getSessionKey(context));
        return map;
    }


    public static boolean isPhoneNumber(String str) {
        return Pattern.matches(numbers, str);
    }

    public static float stringToFloat(String str) {
        float value = 0;
        if (str != null && !str.isEmpty())
            value = Float.parseFloat(str);
        return value;
    }

    public static void loadImage(String url, final ImageView imageView) {
        if (url != null && !url.isEmpty())
            Picasso.get().load(DEVELOPMENT_URL + url).fit().into(imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    Picasso.get().load(R.drawable.ic_placeholder_gallery).into(imageView);
                }
            });
    }

    public static void loadImage(String url, final ImageView imageView, final int placeholder) {
        if (url != null && !url.isEmpty()) {
            Picasso.get().load(DEVELOPMENT_URL + url).fit().into(imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    Picasso.get().load(placeholder).into(imageView);
                }
            });
        } else {
            Picasso.get().load(placeholder).into(imageView);
        }
    }

    public static void loadImageWithFullUrl(String url, final ImageView imageView, final int placeholder) {
        if (url != null && !url.isEmpty()) {
            Picasso.get().load(url).fit().into(imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    Picasso.get().load(placeholder).into(imageView);
                }
            });
        } else {
            Picasso.get().load(placeholder).into(imageView);
        }
    }

    public static void loadProfileImage(String url, final ImageView imageView, final int placeholder) {
        if (url != null && !url.isEmpty()) {
            Picasso.get().load(DEVELOPMENT_URL + url).networkPolicy(NetworkPolicy.NO_CACHE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).networkPolicy(NetworkPolicy.NO_CACHE)
                    .fit().into(imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    Picasso.get().load(placeholder).into(imageView);
                }
            });
        } else {
            Picasso.get().load(placeholder).into(imageView);
        }
    }

    public static void loadImage(Uri url, ImageView imageView) {
        if (url != null && url.getPath() != null) {
            Picasso.get().load(url).fit().into(imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {

                }
            });
        }
    }

    public static void refreshMediaScanner(Activity activity, Uri uri) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            scanIntent.setData(uri);
            activity.sendBroadcast(scanIntent);
        } else {
            final Intent intent = new Intent(Intent.ACTION_MEDIA_MOUNTED, uri);
            activity.sendBroadcast(intent);
        }
    }

    private static void MakeImageDir() {
        File dir = new File(Environment.getExternalStorageDirectory() + File.separator + Vars.Storage_Directory);
        try {
            if (!dir.exists())
                if (!dir.isDirectory()) {
                    if (dir.mkdir()) {
                        System.out.println("AndroidDriver created");
                    } else {
                        System.out.println("AndroidDriver is not created");
                    }
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String SaveImage(Activity activity, Uri uri) {
        MakeImageDir();
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + File.separator + Vars.Storage_Directory);
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);
        try {
            Bitmap finalBitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.getAbsolutePath();
    }


    public static Date stringToDateinddMYYYY(String strDate) {
        Date date = Calendar.getInstance().getTime();
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-M-yyyy");
            date = simpleDateFormat.parse(strDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String formatDateIntoMMddyyyy(String strDate) {

        String inputPattern = "dd-M-yyyy";
        String outputPattern = "MM/dd/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.US);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(strDate);
            str = outputFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    public static long convertDateToMillisecs(String strDate) {
        if (strDate != null && !strDate.isEmpty()) {
            Date date = Calendar.getInstance().getTime();
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
                date = simpleDateFormat.parse(strDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return date.getTime();
        } else
            return 0;
    }

    public static boolean isNetworkNotConnected(Context activity) {
        if (activity != null) {
            ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm.getActiveNetworkInfo() == null) {
//                ShowAlertWithTitle(activity);
            }
            return cm.getActiveNetworkInfo() == null;
        }
        return false;
    }

    public static void displayToastMessage(Activity activity, String message) {
        if (activity != null && message != null && !message.isEmpty()) {
            try {
                Toast toast = Toast.makeText(activity, message, Toast.LENGTH_SHORT);
                LinearLayout toastLayout = (LinearLayout) toast.getView();
                TextView toastTV = (TextView) toastLayout.getChildAt(0);
//                toastTV.setTypeface(typefaceRegularRound);
                toast.show();
            } catch (Exception e) {
                try {
                    Toast toast = Toast.makeText(activity, message, Toast.LENGTH_SHORT);
                    RelativeLayout toastLayout = (RelativeLayout) toast.getView();
                    TextView toastTV = (TextView) toastLayout.getChildAt(0);
//                    toastTV.setTypeface(typefaceRegularRound);
                    toast.show();
                } catch (Exception e1) {

                }
            }
        }
    }

    public static Boolean isValidEmail(String inputEmail) {
        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        Pattern patternObj = Pattern.compile(regExpn);

        Matcher matcherObj = patternObj.matcher(inputEmail);
        if (matcherObj.matches()) {
            return true;
        } else {

            return false;
        }
    }

    public static Boolean isPasswordLengthValid(String password) {

        if (password.length() >= Vars.PASSWORD_LENGHT) {
            return true;
        } else {
            return false;
        }
    }

    public static void showAlert(final Activity activity, String message, boolean isCancellable, final String type) {
        try {
            AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
            alertDialog.setTitle("Alert");
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);
//        alertBuilder.setTitle("Alert");
            alertBuilder.setMessage(message);
            alertBuilder.setCancelable(isCancellable);
            alertBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    arg0.dismiss();
                    if (type.equals("Session_Expired")) {
                        makeLogout(activity);
//                        makeDevelopmentLogin(activity);
                    } else if (type.equals("Server_connect_error")) {
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("exit", true);
                        activity.startActivity(intent);
                    } else if (type.equals("Logout")) {
                        makeLogout(activity);
                    }
                }
            });
            alertDialog = alertBuilder.create();
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            if (activity != null)
                Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
        }
    }

    public static void showAlertWithNegativeButton(final Activity activity, String message, boolean isCancellable, final String type) {
        try {
            AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
            alertDialog.setTitle("Alert");
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);
//        alertBuilder.setTitle("Alert");
            alertBuilder.setMessage(message);
            alertBuilder.setCancelable(isCancellable);
            alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    arg0.dismiss();
                    if (type.equals("Logout")) {
                        makeLogout(activity);
                    }
                }
            });
            alertBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog = alertBuilder.create();
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            if (activity != null)
                Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
        }
    }

    public static void makeLogout(Activity activity) {
        clearAllPref(activity);
        Intent intent = new Intent(activity, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    public static void makeDevelopmentLogin(final Activity activity) {
        UserModel model = new UserModel();
        model.setEmail("navamsc.ss@gmail.com");
        model.setPassword("37J375");
        model.setRoleId(Vars.USER_ROLE);
        model.setPhoneNoCode("");
        final ResponseParser responseParser = new ResponseParser(activity);
        ApiCall.getApiCall(activity, apiService.makeLogin(model), false,
                new ApiCall.OnApiResult() {
                    @Override
                    public void onSuccess(Response<Object> objectResponse) {
                        UserModel userModel = responseParser.getUserModel(objectResponse.body());
                        if (userModel == null) {
                            return;
                        }

                        Support.setPref(activity, Vars.FIRST_NAME, userModel.getFirstName());
                        Support.setPref(activity, Vars.LAST_NAME, userModel.getLastName());
                        Support.setPref(activity, Vars.EMAIL, userModel.getEmail());
                        Support.setPref(activity, Vars.PHONE_NO, userModel.getPhoneNo());
                        Support.setPref(activity, Vars.PHONE_CODE, userModel.getPhoneNoCode());
                        Support.setPref(activity, Vars.PASSWORD, "37J375");
                        Support.setPref(activity, Vars.PHOTO_URL, userModel.getPhotoUrl());
                        Support.setPref(activity, Vars.USER_ID, userModel.getUserId());
                        Support.setSessionKey(activity, userModel.getApiSessionKey());
                    }

                    @Override
                    public void onFailure(String message) {
                        Support.displayToastMessage(activity, message);
                    }
                });
    }

    public static String getDateFromLong(long milliseconds, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.US);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliseconds);
        return formatter.format(calendar.getTime());
    }

    public static String getTimeFromLong(long millis) {
        String hms = "00:00:00";
        try {
            hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hms;
    }

    public static String convertIntoUnitTypeValue(Context context, String value) {
        String roundOffValue = "0.0" + " " + Support.getPref(context, Vars.DISTANCE_TYPE);
        ;
        try {
            String units = Support.getPref(context, Vars.DISTANCE_UNITS);
            double converted = Double.parseDouble(value) / Double.parseDouble(units);
            roundOffValue = String.format("%.2f", converted) + " " + Support.getPref(context, Vars.DISTANCE_TYPE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return roundOffValue;
    }

    public static void loadFragment(AppCompatActivity appCompatActivity, Fragment fragment) {
        FragmentTransaction referNEarnFragmentTransaction1 = appCompatActivity.getSupportFragmentManager().beginTransaction();
        referNEarnFragmentTransaction1.replace(R.id.frame, fragment);
        referNEarnFragmentTransaction1.commit();
    }

    public static String addLeadingZeros(String money) {
        String zeros = "000000000000";
        String result = zeros + money + "";
        result = result.substring(money.length(), result.length());
        return result;
    }

    public static BitmapDescriptor getPinIcon(Context context, int icon) {
        int height = 100;
        int width = 100;
        BitmapDrawable bitmapdraw = (BitmapDrawable) context.getResources().getDrawable(icon);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        return BitmapDescriptorFactory.fromBitmap(smallMarker);
    }

    public static void hideSoftKeypad(Activity activity) {
        View focusedView = activity.getCurrentFocus();
        if (focusedView != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
        }
    }

    public static void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }

}
