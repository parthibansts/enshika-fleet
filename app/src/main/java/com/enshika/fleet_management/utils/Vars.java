package com.enshika.fleet_management.utils;

/**
 * Created by Shamla Tech on 26-06-2018.
 */

public interface Vars {
    //API
    //live
//    String URL_BASE = "http://34.226.236.48:8080/";
    //local ip
    String URL_BASE = "http://192.168.0.113:8080/";
//    String URL_BASE = "http://18.216.0.202:8080/";


    String URL_MAIN = "enshika_qa/";
//    String URL_MAIN = "enshika/";

//    String DEVELOPMENT_URL = URL_BASE + URL_MAIN + API;

    //live
//    String DEVELOPMENT_URL = URL_BASE + URL_MAIN;
    //local ip
    String DEVELOPMENT_URL = URL_BASE /*+ URL_MAIN*/;
    String TELLER_URL = "https://api.theteller.net/v1.1/";

    //PREF
    String MyPref = "Enshika_Fleet";
    String ERROR_MSG = "ERROR-BUSSINESS";
    String FIRST_NAME = "FIRST_NAME";
    String LAST_NAME = "LAST_NAME";
    String EMAIL = "EMAIL";
    String PASSWORD = "PASSWORD";
    String PHONE_CODE = "PHONE_CODE";
    String PHONE_NO = "PHONE_NO";
    String PHOTO_URL = "PHOTO_URL";
    String USER_ID = "USER_ID";
    String PHONE_VERIFIED = "PHONE_VERIFIED";
    String PIN = "PIN";
    String SESSION_KEY = "SESSION_KEY";
    String USER_EMAIL = "USER_EMAIL";
    String USER_MOB = "USER_MOB";
    String USER_MOB_CODE = "USER_MOB_CODE";
    String USER_PASSWORD = "USER_PASSWORD";
    String INSUFFICIENT_FLEET_FEE = "INSUFFICIENT_FLEET_FEE";

    //VALIDATION
    int PASSWORD_LENGHT = 5;

    //CONSTANT
    String USER_ROLE = "7";

    //date
    String DATE_FORMAT = "MM/dd/yyyy, hh:mm aa";
    String SHORT_DATE_FORMAT = "MM/dd/yyyy";
    String SHORT_TIME_FORMAT = "hh:mm aa";


    String Storage_Directory = "Enshika Fleet";
    int PERISSION_READ_WRITE = 12;
    int REFRESH_SCREEN = 13;
    int PAYMENT_TRANSFER = 14;
    int PAYMENT_FAILURE = 15;
    int PAYMENT_WEBVIEW = 16;

    //common
    String CURRENCY_SYMBOL = "CURRENCY_SYMBOL";
    String DISTANCE_TYPE = "DISTANCE_TYPE";
    String DISTANCE_UNITS = "DISTANCE_UNITS";


    //enshika passenger coroporate category type
    String CATEGORY_OTHER = "1";
    String CATEGORY_BANK = "2";
    String CATEGORY_MOBILE_OPERATOR = "3";
    String CATEGORY_UNIVERSITY = "4";

    //payment code
    int PAYMENT_CARD = 1;
    int PAYMENT_MOMO = 2;

    //cards
    String VISA_CARD = "VIS";
    String MASTER_CARD = "MAS";
    //    String AMERICAN_EXPRESS = "AME";
    String UNKNOWN = "UNK";

    //callback url
    String TELLER_CALLBACK_URL = "https://api.theteller.net";
    String TELLER_DESCRIPTION = "fleet wallet payment";


}
