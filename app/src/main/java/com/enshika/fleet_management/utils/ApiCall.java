package com.enshika.fleet_management.utils;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;

import com.enshika.fleet_management.R;
import com.google.gson.Gson;

import java.net.ConnectException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.enshika.fleet_management.utils.Support.isNetworkNotConnected;
import static com.enshika.fleet_management.utils.Support.showAlert;


/**
 * Created by SUN on 30-01-2018.
 */

public class ApiCall {

    private static OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES).build();

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Vars.DEVELOPMENT_URL).client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    public static ApiList apiService = retrofit.create(ApiList.class);

    private static Retrofit retrofit_teller = new Retrofit.Builder()
            .baseUrl(Vars.TELLER_URL).client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    public static ApiList apiServiceTeller = retrofit_teller.create(ApiList.class);

    public static void getApiCall(final Activity activity, Call<Object> call, final boolean progress, final OnApiResult onApiResult) {
        final ResponseParser responseParser = new ResponseParser(activity);
        if (!isNetworkNotConnected(activity)) {
            if (progress)
                Support.showLoader(activity);
            try {
                Log.e("api: ", call.request().url().toString());
                Log.e("parms: ", new Gson().toJson(call.request().body()) + "");
                Log.e("header: ", call.request().headers() + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(@NonNull Call<Object> call, @NonNull Response<Object> response) {
                    try {
                        if (progress)
                            Support.hideLoader(activity);
                        if (response.isSuccessful()) {
                            onApiResult.onSuccess(response);
                        } else {
                            if (response.code() == 401) {
                                showAlert(activity, activity.getString(R.string.session_expired), false, "Session_Expired");
                                onApiResult.onFailure("");
                            } else
                                onApiResult.onFailure(responseParser.getErrorMessage(response.errorBody().string()));
                        }
                        Log.e("Response code: ", response.code() + "");
                        Log.e("onResponse: ", new Gson().toJson(response.body()) + "");
                        Log.e("onErrorResponse: ", response.errorBody() + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.w("API Exception : ", e.getLocalizedMessage());
                        onApiResult.onFailure(e.getLocalizedMessage() + "");
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    Log.e("onErrorResponse: ", t.getMessage() + " " + t.getCause());
                    if (progress)
                        Support.hideLoader(activity);
                    if (t instanceof ConnectException) {
                        showAlert(activity, activity.getString(R.string.server_connection_error), false, "Server_connect_error");
                        onApiResult.onFailure("");
                    } else {
                        onApiResult.onFailure(t.getMessage());
                    }

                }


            });
        }
    }

    public static void getRetro_Service_Call(Call<Object> call, final boolean progress, final OnApiResult onApiResult) {
//        if (progress)
//            showProgress(Vars.Custom_Progress);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(@NonNull Call<Object> call, @NonNull Response<Object> response) {
                try {
//                    if (progress)
//                        hideProgress(Vars.Custom_Progress);
                    if (response.isSuccessful()) {
                        onApiResult.onSuccess(response);
                    } else {
                        onApiResult.onFailure(response.raw().message());
                        Log.w("API Response Error", " " +
                                "\nResponse Code : " + response.raw().code() +
                                "\nError  Msg : " + response.raw().message() +
                                "\nUrl : " + response.raw().request().url() +
                                "\nMethod : " + response.raw().request().method());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.w("API Exception : ", e.getLocalizedMessage());
                    onApiResult.onFailure(e.getLocalizedMessage() + "");
                }
            }

            @Override
            public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
//                if (progress)
//                    hideProgress(Vars.Custom_Progress);
                t.printStackTrace();
                onApiResult.onFailure(t.getMessage());
                Log.w("API onFailure :  ", "API call failed! " + t.getLocalizedMessage());
            }


        });
    }

    public interface OnApiResult {
        void onSuccess(Response<Object> objectResponse);

        void onFailure(String message);
    }
}
