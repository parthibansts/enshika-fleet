package com.enshika.fleet_management.utils;

import android.app.Activity;
import android.widget.Toast;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.AppSettingsModel;
import com.enshika.fleet_management.model.BookingDetailModel;
import com.enshika.fleet_management.model.BookingsModel;
import com.enshika.fleet_management.model.CarDetailPojo;
import com.enshika.fleet_management.model.CarIdModel;
import com.enshika.fleet_management.model.CarTypePojo;
import com.enshika.fleet_management.model.CardModel;
import com.enshika.fleet_management.model.CardValidateReqModel;
import com.enshika.fleet_management.model.CardValidateResultModel;
import com.enshika.fleet_management.model.CarsPojo;
import com.enshika.fleet_management.model.CommonResponseModel;
import com.enshika.fleet_management.model.DashboardRevenueModel;
import com.enshika.fleet_management.model.DashboardSummaryModel;
import com.enshika.fleet_management.model.DriverDetailModel;
import com.enshika.fleet_management.model.DriverIdModel;
import com.enshika.fleet_management.model.DriverLocationsPojo;
import com.enshika.fleet_management.model.DriverRidePojo;
import com.enshika.fleet_management.model.DriverStatusModel;
import com.enshika.fleet_management.model.DriversPojo;
import com.enshika.fleet_management.model.DriversResponsePojo;
import com.enshika.fleet_management.model.ImagePojo;
import com.enshika.fleet_management.model.MerchantResponseModel;
import com.enshika.fleet_management.model.MessageModel;
import com.enshika.fleet_management.model.PaymentResponseModel;
import com.enshika.fleet_management.model.RegionPojo;
import com.enshika.fleet_management.model.StatusMessageModel;
import com.enshika.fleet_management.model.TellerErrorModel;
import com.enshika.fleet_management.model.TransactionHistoryModel;
import com.enshika.fleet_management.model.TransactionResultModel;
import com.enshika.fleet_management.model.UserModel;
import com.enshika.fleet_management.model.WalletBalanceModel;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.ArrayList;

import static com.enshika.fleet_management.utils.Support.showAlert;

/**
 * Created by Shamla Tech on 28-06-2018.
 */

public class ResponseParser {
    private Gson gson;
    private Activity activity;

    public ResponseParser(Activity activity) {

        this.activity = activity;

        gson = new Gson();

    }


    public UserModel getUserModel(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        UserModel userModel = null;

//        if (!parseIntoError(json)) {

        userModel = gson.fromJson(json, UserModel.class);

//        }

        return userModel;
    }

    public DriverStatusModel getDriverStatusModel(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        DriverStatusModel userModel = new DriverStatusModel();
        try {
            userModel = gson.fromJson(json, DriverStatusModel.class);
        } catch (Exception e) {

        }
        return userModel;
    }

    public WalletBalanceModel getWalletModel(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        WalletBalanceModel userModel = new WalletBalanceModel();
        try {
            userModel = gson.fromJson(json, WalletBalanceModel.class);
        } catch (Exception e) {

        }
        return userModel;
    }

    public PaymentResponseModel getPaymentModel(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        PaymentResponseModel userModel = new PaymentResponseModel();
        try {
            userModel = gson.fromJson(json, PaymentResponseModel.class);
        } catch (Exception e) {

        }
        return userModel;
    }

    public JSONObject getJsonObjectResult(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(new Gson().toJson(objectResponse));
        } catch (Exception e) {

        }
        return jsonObject;
    }

    public CardValidateResultModel getCardValidateResultModel(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        CardValidateResultModel userModel = new CardValidateResultModel();
        try {
            userModel = gson.fromJson(json, CardValidateResultModel.class);
        } catch (Exception e) {

        }
        return userModel;
    }

    public CardValidateReqModel getSaveCardResponseModel(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        CardValidateReqModel userModel = new CardValidateReqModel();
        try {
            userModel = gson.fromJson(json, CardValidateReqModel.class);
        } catch (Exception e) {

        }
        return userModel;
    }

    public TransactionResultModel getTransactionResult(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        TransactionResultModel userModel = new TransactionResultModel();
        try {
            userModel = gson.fromJson(json, TransactionResultModel.class);
        } catch (Exception e) {

        }
        return userModel;
    }

    public MerchantResponseModel getMerchantResponseModel(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        MerchantResponseModel userModel = new MerchantResponseModel();
        try {
            userModel = gson.fromJson(json, MerchantResponseModel.class);
        } catch (Exception e) {

        }
        return userModel;
    }

    public ArrayList<TransactionHistoryModel> getTransactionList(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        TypeToken<ArrayList<TransactionHistoryModel>> token = new TypeToken<ArrayList<TransactionHistoryModel>>() {
        };

        ArrayList<TransactionHistoryModel> carsPojo = new ArrayList<>();
        try {
            carsPojo = gson.fromJson(json, token.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return carsPojo;
    }

    public ArrayList<CarsPojo> getCarList(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        TypeToken<ArrayList<CarsPojo>> token = new TypeToken<ArrayList<CarsPojo>>() {
        };

        ArrayList<CarsPojo> carsPojo = new ArrayList<>();
        try {
            carsPojo = gson.fromJson(json, token.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return carsPojo;
    }

    public ArrayList<RegionPojo> getRegions(Object objectResponse) {

        ArrayList<RegionPojo> carsPojo = new ArrayList<>();
        try {
            String json = new Gson().toJson(objectResponse);

            TypeToken<ArrayList<RegionPojo>> token = new TypeToken<ArrayList<RegionPojo>>() {
            };
            carsPojo = gson.fromJson(json, token.getType());
        } catch (Exception e) {

        }
        return carsPojo;
    }

    public ArrayList<BookingsModel> getBookingsList(Object objectResponse) {

        ArrayList<BookingsModel> carsPojo = new ArrayList<>();

        String json = new Gson().toJson(objectResponse);

        try {
            TypeToken<ArrayList<BookingsModel>> token = new TypeToken<ArrayList<BookingsModel>>() {
            };

//        if (!parseIntoError(json)) {
            carsPojo = gson.fromJson(json, token.getType());
//        }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return carsPojo;
    }

    public ArrayList<DriverLocationsPojo> getDriversLocationList(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        TypeToken<ArrayList<DriverLocationsPojo>> token = new TypeToken<ArrayList<DriverLocationsPojo>>() {
        };

        ArrayList<DriverLocationsPojo> carsPojo = null;

//        if (!parseIntoError(json)) {
        carsPojo = gson.fromJson(json, token.getType());
//        }
        return carsPojo;
    }

    public ArrayList<DriverRidePojo> getDriverRidesList(Object objectResponse) {

        ArrayList<DriverRidePojo> carsPojo = new ArrayList<>();
        try {
            String json = new Gson().toJson(objectResponse);

            TypeToken<ArrayList<DriverRidePojo>> token = new TypeToken<ArrayList<DriverRidePojo>>() {
            };
//            if (!parseIntoError(json)) {
            carsPojo = gson.fromJson(json, token.getType());
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return carsPojo;
    }

    public DriversResponsePojo getDriverList(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        DriversResponsePojo driversResponsePojo = new DriversResponsePojo();
        try {
            driversResponsePojo = gson.fromJson(json, DriversResponsePojo.class);
        } catch (Exception e) {

        }
        return driversResponsePojo;
    }

    public ArrayList<DriversPojo> getFleetFeeDriverList(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        TypeToken<ArrayList<DriversPojo>> token = new TypeToken<ArrayList<DriversPojo>>() {
        };

        ArrayList<DriversPojo> carsPojo = new ArrayList<>();
        try {
            carsPojo = gson.fromJson(json, token.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return carsPojo;
    }

    public ArrayList<CarTypePojo> getCarType(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        TypeToken<ArrayList<CarTypePojo>> token = new TypeToken<ArrayList<CarTypePojo>>() {
        };

        ArrayList<CarTypePojo> carsPojo = null;

//        if (!parseIntoError(json)) {
        carsPojo = gson.fromJson(json, token.getType());
//        }
        return carsPojo;
    }

    public ArrayList<DashboardSummaryModel> getDashboardSummary(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        TypeToken<ArrayList<DashboardSummaryModel>> token = new TypeToken<ArrayList<DashboardSummaryModel>>() {
        };

        ArrayList<DashboardSummaryModel> carsPojo = new ArrayList<>();
        try {
            carsPojo = gson.fromJson(json, token.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return carsPojo;
    }

    public ArrayList<DashboardRevenueModel> getDashboardRevenue(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        TypeToken<ArrayList<DashboardRevenueModel>> token = new TypeToken<ArrayList<DashboardRevenueModel>>() {
        };

        ArrayList<DashboardRevenueModel> carsPojo = new ArrayList<>();
        try {
            carsPojo = gson.fromJson(json, token.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return carsPojo;
    }

    public CarDetailPojo getCarDetail(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        CarDetailPojo carsPojo = null;

//        if (!parseIntoError(json)) {
        carsPojo = gson.fromJson(json, CarDetailPojo.class);
//        }
        return carsPojo;
    }

    public BookingDetailModel getBookingDetail(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);
        BookingDetailModel carsPojo = new BookingDetailModel();
        try {
            carsPojo = gson.fromJson(json, BookingDetailModel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return carsPojo;
    }

    public DriverDetailModel getDriverDetail(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        DriverDetailModel carsPojo = null;

//        if (!parseIntoError(json)) {
        carsPojo = gson.fromJson(json, DriverDetailModel.class);
//        }
        return carsPojo;
    }

    public DriverIdModel getDriverIdModel(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        DriverIdModel carsPojo = new DriverIdModel();
        try {
            carsPojo = gson.fromJson(json, DriverIdModel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return carsPojo;
    }

    public CarIdModel getCarIdModel(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        CarIdModel carsPojo = new CarIdModel();
        try {
            carsPojo = gson.fromJson(json, CarIdModel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return carsPojo;
    }

    public ImagePojo getImageModel(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        ImagePojo carsPojo = new ImagePojo();
        try {
            carsPojo = gson.fromJson(json, ImagePojo.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return carsPojo;
    }

    public String getErrorMessage(String json) {
        String errorMessage = "Connection Error";
        try {
            MessageModel messageModel = gson.fromJson(json, MessageModel.class);
            if (messageModel != null && messageModel.getMessages() != null && messageModel.getMessages().length > 0) {
                errorMessage = messageModel.getMessages()[0];
            } else {
                getTellerError(json);
            }
        } catch (Exception e) {
            e.printStackTrace();
            getTellerError(json);
        }
        return errorMessage;
    }

    public String getTellerError(String json) {
        String errorMessage = "Connection Error";
        try {
            TellerErrorModel messageModel = gson.fromJson(json, TellerErrorModel.class);
            errorMessage = messageModel.getDescription();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return errorMessage;
    }

    public TellerErrorModel getSaveTransactionModel(Object objectResponse) {
        String json = new Gson().toJson(objectResponse);

        TellerErrorModel carsPojo = new TellerErrorModel();
        try {
            carsPojo = gson.fromJson(json, TellerErrorModel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return carsPojo;
    }


    public AppSettingsModel getAppSettingsModel(Object objectResponse) {
        String json = new Gson().toJson(objectResponse);
        AppSettingsModel messageModel = new AppSettingsModel();
        try {
            messageModel = gson.fromJson(json, AppSettingsModel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return messageModel;
    }

    public StatusMessageModel getStatusMessage(Object objectResponse) {
        String json = new Gson().toJson(objectResponse);
        StatusMessageModel messageModel = new StatusMessageModel();
        try {
            messageModel = gson.fromJson(json, StatusMessageModel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return messageModel;
    }

    public String getStatus(String json) {
        String errorMessage = "";

        MessageModel messageModel = gson.fromJson(json, MessageModel.class);
        if (messageModel != null && messageModel.getMessages().length > 0) {
            errorMessage = messageModel.getType();
        }
        return errorMessage;
    }

    public ArrayList<CardModel> getCardList(Object objectResponse) {

        String json = new Gson().toJson(objectResponse);

        TypeToken<ArrayList<CardModel>> token = new TypeToken<ArrayList<CardModel>>() {
        };

        ArrayList<CardModel> carsPojo = new ArrayList<>();
        try {
            carsPojo = gson.fromJson(json, token.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return carsPojo;
    }

    public CardModel getCardModel(Object objectResponse) {
        String json = new Gson().toJson(objectResponse);
        CardModel messageModel = new CardModel();
        try {
            messageModel = gson.fromJson(json, CardModel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return messageModel;
    }

    public CommonResponseModel getCommonResponseModel(Object objectResponse) {
        String json = new Gson().toJson(objectResponse);
        CommonResponseModel messageModel = new CommonResponseModel();
        try {
            messageModel = gson.fromJson(json, CommonResponseModel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return messageModel;
    }

    private boolean parseIntoError(String json) {

        try {
            if (json.contains("<!DOCTYPE html><html><head><title>Apache Tomcat/8.0.26 - Error report</title><style type=\"text/css\">H1 {font-family:Tahoma,Arial,sans")) {

                Toast.makeText(activity, activity.getResources().getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                return false;
            }

            MessageModel messageModel = null;
            try {
                messageModel = gson.fromJson(json, MessageModel.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                return false;

            }

            if (messageModel != null
                    && messageModel.getType() != null
                    && (messageModel.getType().equalsIgnoreCase("error") || messageModel
                    .getType().equalsIgnoreCase(Vars.ERROR_MSG))) {

                String message = "";

                if (messageModel.getMessages() != null) {

                    for (String msg : messageModel.getMessages()) {

                        message = message + msg + ",";

                    }

                }

                if (message.lastIndexOf(',') != -1) {

                    message = message.substring(0, message.lastIndexOf(','));

                }

                if (messageModel.getStatusCode() == 401) {
                    if (activity != null) {
                        showAlert(activity, activity.getString(R.string.session_expired), false, "Session_Expired");
                    }

                    return true;

                }

                if (activity != null) {
                    showAlert(activity, message, true, "");
                }

                return true;

            }
        } catch (Exception e) {

        }
        return false;

    }
}
