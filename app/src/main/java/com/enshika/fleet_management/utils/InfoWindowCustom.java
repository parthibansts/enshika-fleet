package com.enshika.fleet_management.utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.enshika.fleet_management.R;
import com.enshika.fleet_management.model.DriverLocationsPojo;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by Shamla Tech on 14-06-2018.
 */

public class InfoWindowCustom implements GoogleMap.InfoWindowAdapter {
    Context context;
    LayoutInflater inflater;

    public InfoWindowCustom(Context context) {
        this.context = context;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.list_marker_info_window, null);

        TextView txtName = view.findViewById(R.id.txtName);
        TextView txtEmail = view.findViewById(R.id.txtEmail);
        TextView txtPhone = view.findViewById(R.id.txtPhone);
        TextView txtId = view.findViewById(R.id.txtId);
        TextView txtDOB = view.findViewById(R.id.txtDOB);
        TextView txtStar = view.findViewById(R.id.txtStar);

        DriverLocationsPojo pojo = (DriverLocationsPojo) marker.getTag();
        txtName.setText(pojo.getName());
        txtEmail.setText(pojo.getEmail());
        txtPhone.setText(pojo.getPhone());
        txtDOB.setText(pojo.getDob());
        txtStar.setText(pojo.getRate());
        txtId.setText(pojo.getDriverId());
        return view;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }
}